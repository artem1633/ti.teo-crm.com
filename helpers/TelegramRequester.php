<?php

namespace app\helpers;

/**
 * Class TelegramRequester
 * @package app\helpers
 */
class TelegramRequester
{
    public static function send($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        \Yii::warning('TELEGRAMM REQUEST');

        $url = "https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        return $result; //Или просто возращаем ответ в виде строки
    }
}