<?php

namespace app\helpers;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Class ChatAPI
 * @package app\helpers
 */
class ChatAPI
{
    /**
     * @param string $phone
     * @param string $body
     */
    public static function sendMessage($phone, $body)
    {
        \Yii::warning('WHATSAPP METHOD');
        self::formatPhone($phone);
        $url = "https://api.chat-api.com/instance170513/sendMessage?token=8q3imn0h1y0n2obi";


        $data = json_encode([
            'phone' => $phone,
            'body' => $body
        ], JSON_UNESCAPED_UNICODE);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $out = curl_exec($curl);
        \Yii::warning($out, 'WhatsApp API Response');
        curl_close($curl);
    }


    /**
     * @param string $phone
     * @param string $filename
     * @param string $caption
     */
    public static function sendFile($phone, $filename, $caption = null)
    {
        \Yii::warning('WHATSAPP METHOD');
        self::formatPhone($phone);
        $url = "https://api.chat-api.com/instance170513/sendFile?token=8q3imn0h1y0n2obi";

        $fileNameParts = explode('.', $filename);

        $extension = null;

        if(count($fileNameParts) > 0){
            $extension = $fileNameParts[count($fileNameParts) - 1];
        }


        $data = json_encode([
            'phone' => $phone,
            'body' => Url::base(true).'/'.$filename,
            'filename' => "file.{$extension}",
            'caption' => $caption,
        ], JSON_UNESCAPED_UNICODE);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $out = curl_exec($curl);
        \Yii::warning($out, 'WhatsApp API Response');
        curl_close($curl);
    }

    /**
     * @param string $phone
     */
    private static function formatPhone(&$phone)
    {
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('+', '', $phone);
    }
}