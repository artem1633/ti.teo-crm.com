<?php

use yii\db\Migration;

/**
 * Handles adding date_cr to table `available`.
 */
class m180616_040351_add_date_cr_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('resource', 'date_cr', $this->datetime());
        $this->addColumn('available', 'date_cr', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('resource', 'date_cr');
        $this->dropColumn('available', 'date_cr');
    }
}
