<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_list_product`.
 */
class m210221_091944_create_job_list_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_list_product', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'job_list_id' => $this->integer()->comment('Услуга'),
            'product_id' => $this->integer()->comment('Расходник'),
            'count' => $this->double()->comment('Кол-во'),
        ]);

        $this->createIndex(
            'idx-job_list_product-order_id',
            'job_list_product',
            'order_id'
        );

        $this->addForeignKey(
            'fk-job_list_product-order_id',
            'job_list_product',
            'order_id',
            'orders',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-job_list_product-product_id',
            'job_list_product',
            'product_id'
        );

        $this->addForeignKey(
            'fk-job_list_product-product_id',
            'job_list_product',
            'product_id',
            'product',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-job_list_product-job_list_id',
            'job_list_product',
            'job_list_id'
        );

        $this->addForeignKey(
            'fk-job_list_product-job_list_id',
            'job_list_product',
            'job_list_id',
            'job_list',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-job_list_product-job_list_id',
            'job_list_product'
        );

        $this->dropIndex(
            'idx-job_list_product-job_list_id',
            'job_list_product'
        );

        $this->dropForeignKey(
            'fk-job_list_product-product_id',
            'job_list_product'
        );

        $this->dropIndex(
            'idx-job_list_product-product_id',
            'job_list_product'
        );

        $this->dropForeignKey(
            'fk-job_list_product-order_id',
            'job_list_product'
        );

        $this->dropIndex(
            'idx-job_list_product-order_id',
            'job_list_product'
        );

        $this->dropTable('job_list_product');
    }
}
