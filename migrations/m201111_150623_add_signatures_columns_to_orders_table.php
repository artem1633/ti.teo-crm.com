<?php

use yii\db\Migration;

/**
 * Handles adding signatures to table `orders`.
 */
class m201111_150623_add_signatures_columns_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'signature_admin', $this->binary()->comment('Роспись админа'));
        $this->addColumn('orders', 'signature_client', $this->binary()->comment('Роспись клиента'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'signature_admin');
        $this->dropColumn('orders', 'signature_client');
    }
}
