<?php

use yii\db\Migration;

/**
 * Handles adding privilage to table `users`.
 */
class m210213_071213_add_privilage_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'can_view', $this->boolean()->defaultValue(false)->comment('Просмотр'));
        $this->addColumn('users', 'can_update', $this->boolean()->defaultValue(false)->comment('Редактирование'));
        $this->addColumn('users', 'can_delete', $this->boolean()->defaultValue(false)->comment('Удаление'));
        $this->addColumn('users', 'accept_notifications', $this->boolean()->defaultValue(false)->comment('Получать уведомления'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'can_view');
        $this->dropColumn('users', 'can_update');
        $this->dropColumn('users', 'can_delete');
        $this->dropColumn('users', 'accept_notifications');
    }
}
