<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders_color`.
 */
class m180607_172624_create_orders_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('orders_color', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(10),
            'day' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('orders_color');
    }
}
