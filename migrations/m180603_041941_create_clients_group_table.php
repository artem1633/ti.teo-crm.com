<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_group`.
 */
class m180603_041941_create_clients_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients_group');
    }
}
