<?php

use yii\db\Migration;

/**
 * Handles the creation of table `atelier`.
 */
class m180601_121046_create_atelier_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('atelier', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название юр. лица'),
            'director' => $this->string(255)->comment('Директор'),
            'inn' => $this->string(255)->comment('ИНН'),
            'kpp' => $this->string(255)->comment('КПП'),
            'ogrn' => $this->string(255)->comment('ОГРН'),
            'bank' => $this->string(255)->comment('Название банка'),
            'kor_schot' => $this->string(255)->comment('Корр. счет'),
            'checking_accaunt' => $this->string(255)->comment('Расчетный счет'),
            'okpo' => $this->string(255)->comment('ОКПО'),
            'bik' => $this->string(255)->comment('БИК'),
            'address' => $this->string(255)->comment('Адрес'),
            'telephone' => $this->string(255)->comment('Телефон'),
            'email' => $this->string(255)->comment('Email'),
        ]);

        $this->insert('atelier', [
                'name' => 'Главный магазин',
            ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('atelier');
    }
}
