<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_company`.
 */
class m180610_135844_create_about_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('about_company', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название юр. лица '),
            'director' => $this->string(255)->comment('Директор'),
            'address' => $this->text()->comment('Адрес '),
            'bank' => $this->string(255)->comment('Банк'),
            'inn' => $this->string(255)->comment('ИНН'),
            'kpp' => $this->string(255)->comment('КПП'),
            'okpo' => $this->string(255)->comment('ОКПО'),
            'r_s' => $this->string(255)->comment('Расчетный счет'),
            'kor_schot' => $this->string(255)->comment('Корреспондентский счет'),
            'ogrn' => $this->string(255)->comment('ОГРН'),
            'bik' => $this->string(255)->comment('БИК'),
            'telephone' => $this->string(255)->comment('Телефон'),
            'site' => $this->string(255)->comment('Сайт'),
            'email' => $this->string(255)->comment('Email'),
            'service_login' => $this->string(255)->comment('Логин смс-сервиса'),
            'service_parol' => $this->string(255)->comment('Пароль смс-сервиса'),
        ]);

        $this->insert('about_company',array(
            'name' => 'Компания "Цифровые Технологии"',
            'director' => 'Боровиков Андрей Васильевич',
            'address' => 'г. Глазов, ул. Молодой гвардии, д. 9',
            'bank' => 'Ижкомбанк',
            'email' => 'tv-glazov@mail.ru',
            'telephone' => '(34141) 66-001, 8-912-022-82-22',
            'site' => 'shop-crm.ru',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about_company');
    }
}
