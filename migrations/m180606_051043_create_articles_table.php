<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m180606_051043_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->insert('articles',array(
            'name' => 'Без названия',
        ));

        $this->insert('articles',array(
            'name' => 'Выдача ЗП',
        ));

        $this->insert('articles',array(
            'name' => 'Продажа товара',
        ));

        $this->insert('articles',array(
            'name' => 'Оказание услуг',
        ));

        $this->insert('articles',array(
            'name' => 'Химчистка',
        ));

        $this->insert('articles',array(
            'name' => 'Продажа товаров/носки',
        ));

        $this->insert('articles',array(
            'name' => 'Продажа товаров/ швейная фурнитура',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('articles');
    }
}
