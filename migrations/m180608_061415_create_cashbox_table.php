<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashbox`.
 */
class m180608_061415_create_cashbox_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cashbox', [
            'id' => $this->primaryKey(),
            'atelier_id' => $this->integer()->comment('Ателье'),
            'name' => $this->string(255)->comment('Название кассы'),
            'online_cash' => $this->integer()->comment('Подключение онлайн-кассы'),
        ]);

        $this->createIndex('idx-cashbox-atelier_id', 'cashbox', 'atelier_id', false);
        $this->addForeignKey("fk-cashbox-atelier_id", "cashbox", "atelier_id", "atelier", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-cashbox-atelier_id','cashbox');
        $this->dropIndex('idx-cashbox-atelier_id','cashbox');  

        $this->dropTable('cashbox');
    }
}
