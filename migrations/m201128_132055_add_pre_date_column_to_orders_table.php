<?php

use yii\db\Migration;

/**
 * Handles adding pre_date to table `orders`.
 */
class m201128_132055_add_pre_date_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'pre_date', $this->date()->comment('Предварительная дата'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'pre_date');
    }
}
