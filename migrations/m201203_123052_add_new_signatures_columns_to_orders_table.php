<?php

use yii\db\Migration;

/**
 * Handles adding new_signatures to table `orders`.
 */
class m201203_123052_add_new_signatures_columns_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'signature_admin_second', $this->binary()->comment('Роспись админа'));
        $this->addColumn('orders', 'signature_client_second', $this->binary()->comment('Роспись клиента'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'signature_admin_second');
        $this->dropColumn('orders', 'signature_client_second');
    }
}
