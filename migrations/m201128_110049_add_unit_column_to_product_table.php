<?php

use yii\db\Migration;

/**
 * Handles adding unit to table `product`.
 */
class m201128_110049_add_unit_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'unit', $this->integer()->comment('Ед. измерения'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'unit');
    }
}
