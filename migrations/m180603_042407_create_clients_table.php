<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m180603_042407_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment('ФИО'),
            'group_id' => $this->integer()->comment('Группа клиента'),
            'address' => $this->text()->comment('Адрес'),
            'telephone' => $this->string(255)->comment('Телефон'),
            'email' => $this->string(255)->comment('E-mail'),
            'sale_for_work' => $this->float()->defaultValue(0)->comment('Скидка-кэшбэк на работу'),
            'sale_for_product' => $this->float()->defaultValue(0)->comment('Скидка-кэшбэк на товар'),
            'usd_ball' => $this->float()->comment('Количество скидочных баллов из сервиса  UDS'),
            'company_name' => $this->string()->comment('Компания'),
            'company_full_name' => $this->string()->comment('Полное название компании'),
            'bank_name' => $this->string()->comment('Банк'),
            'bin' => $this->string()->comment('БИН'),
            'iban' => $this->string()->comment('IBAN'),
            'bik' => $this->string()->comment('БИК'),
            'description' => $this->text()->comment('Примечание'),
            'atelier_id' => $this->integer()->comment('Ателье'),
        ]);

        $this->createIndex('idx-clients-group_id', 'clients', 'group_id', false);
        $this->addForeignKey("fk-clients-group_id", "clients", "group_id", "clients_group", "id");

        $this->createIndex('idx-clients-atelier_id', 'clients', 'atelier_id', false);
        $this->addForeignKey("fk-clients-atelier_id", "clients", "atelier_id", "atelier", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-clients-group_id','clients');
        $this->dropIndex('idx-clients-group_id','clients');  

        $this->dropForeignKey('fk-clients-atelier_id','clients');
        $this->dropIndex('idx-clients-atelier_id','clients');  

        $this->dropTable('clients');
    }
}
