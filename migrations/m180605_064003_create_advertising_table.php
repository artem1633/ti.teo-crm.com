<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advertising`.
 */
class m180605_064003_create_advertising_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('advertising', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('advertising');
    }
}
