<?php

use yii\db\Migration;

/**
 * Handles adding can to table `users`.
 */
class m201201_145842_add_can_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'can_storage', $this->integer()->comment('Склад'));
        $this->addColumn('users', 'can_book', $this->integer()->comment('Справочники'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'can_storage');
        $this->dropColumn('users', 'can_book');
    }
}
