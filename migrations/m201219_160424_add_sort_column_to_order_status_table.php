<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `order_status`.
 */
class m201219_160424_add_sort_column_to_order_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order_status', 'sort', $this->integer()->comment('Сортировка'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order_status', 'sort');
    }
}
