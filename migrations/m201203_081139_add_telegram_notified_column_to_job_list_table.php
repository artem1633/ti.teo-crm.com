<?php

use yii\db\Migration;

/**
 * Handles adding telegram_notified to table `job_list`.
 */
class m201203_081139_add_telegram_notified_column_to_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('job_list', 'telegram_notified', $this->boolean()->defaultValue(false)->comment('Уведомлен'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('job_list', 'telegram_notified');
    }
}
