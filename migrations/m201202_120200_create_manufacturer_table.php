<?php

use yii\db\Migration;

/**
 * Handles the creation of table `manufacturer`.
 */
class m201202_120200_create_manufacturer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('manufacturer', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('manufacturer');
    }
}
