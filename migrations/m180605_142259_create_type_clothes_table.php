<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_clothes`.
 */
class m180605_142259_create_type_clothes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('type_clothes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('type_clothes');
    }
}
