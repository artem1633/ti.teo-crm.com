<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `application`.
 */
class m180917_064516_drop_application_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk-application-atelier_id','application');
        $this->dropIndex('idx-application-atelier_id','application');

        $this->dropForeignKey('fk-application-creator','application');
        $this->dropIndex('idx-application-creator','application');

        $this->dropForeignKey('fk-application-product_id','application');
        $this->dropIndex('idx-application-product_id','application');

        $this->dropForeignKey('fk-application-status_id','application');
        $this->dropIndex('idx-application-status_id','application');

        $this->dropTable('application');
        $this->dropTable('claim_statuses');
        $this->dropTable('articles');
        $this->dropTable('orders_color');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
       $this->createTable('application', [
            'id' => $this->primaryKey(),
            'date_cr' => $this->datetime()->comment('Дата создания'),
            'atelier_id' => $this->integer()->comment('Запрашивающее ателье'),
            'creator' => $this->integer()->comment('Создатель'),
            'product_id' => $this->integer()->comment('Товар'),
            'count' => $this->integer()->comment('Количество'),
            'status_id' => $this->integer()->comment('Статус'),
        ]);

        $this->createTable('claim_statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(10),
        ]);

        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->createTable('orders_color', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(10),
            'day' => $this->integer(),
        ]);

        $this->createIndex('idx-application-atelier_id', 'application', 'atelier_id', false);
        $this->addForeignKey("fk-application-atelier_id", "application", "atelier_id", "atelier", "id");

        $this->createIndex('idx-application-creator', 'application', 'creator', false);
        $this->addForeignKey("fk-application-creator", "application", "creator", "users", "id");

        $this->createIndex('idx-application-product_id', 'application', 'product_id', false);
        $this->addForeignKey("fk-application-product_id", "application", "product_id", "product", "id");

        $this->createIndex('idx-application-status_id', 'application', 'status_id', false);
        $this->addForeignKey("fk-application-status_id", "application", "status_id", "claim_statuses", "id");
    }
}
