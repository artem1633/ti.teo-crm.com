<?php

use yii\db\Migration;

/**
 * Class m201219_093115_add_can_order_to_users_table
 */
class m201219_092015_add_can_order_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'can_order', $this->integer()->comment('Заявки'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'can_order');
    }
}
