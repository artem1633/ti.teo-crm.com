<?php

use yii\db\Migration;

/**
 * Handles adding new to table `job_list`.
 */
class m201117_112653_add_new_columns_to_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
//        $this->addColumn('job_list', 'prepaid', $this->float()->comment('Аванс'));
        $this->addColumn('job_list', 'master_price', $this->float()->comment('Сумма мастера'));
        $this->addColumn('job_list', 'master_prepaid', $this->float()->comment('Аванс мастера'));
        $this->addColumn('job_list', 'cost_credit', $this->float()->comment('Оплата расходник'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
//        $this->dropColumn('job_list', 'prepaid');
        $this->dropColumn('job_list', 'master_price');
        $this->dropColumn('job_list', 'master_prepaid');
        $this->dropColumn('job_list', 'cost_credit');
    }
}
