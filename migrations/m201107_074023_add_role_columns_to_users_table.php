<?php

use yii\db\Migration;

/**
 * Handles adding role to table `users`.
 */
class m201107_074023_add_role_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'can_dashboard', $this->boolean()->defaultValue(false)->comment('Dashboard'));
        $this->addColumn('users', 'can_orders', $this->boolean()->defaultValue(false)->comment('Заказы'));
        $this->addColumn('users', 'can_clients', $this->boolean()->defaultValue(false)->comment('Клиенты'));
        $this->addColumn('users', 'can_masters', $this->boolean()->defaultValue(false)->comment('Мастера'));
        $this->addColumn('users', 'can_reports', $this->boolean()->defaultValue(false)->comment('Отчеты'));
        $this->addColumn('users', 'can_users', $this->boolean()->defaultValue(false)->comment('Пользователи'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'can_dashboard');
        $this->dropColumn('users', 'can_orders');
        $this->dropColumn('users', 'can_clients');
        $this->dropColumn('users', 'can_masters');
        $this->dropColumn('users', 'can_reports');
        $this->dropColumn('users', 'can_users');
    }
}
