<?php

use yii\db\Migration;

/**
 * Handles adding status_id to table `job_list`.
 */
class m201121_092859_add_status_id_column_to_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('job_list', 'status_id', $this->integer()->comment('Статус'));

        $this->createIndex(
            'idx-job_list-status_id',
            'job_list',
            'status_id'
        );

        $this->addForeignKey(
            'fk-job_list-status_id',
            'job_list',
            'status_id',
            'order_status',
            'id',
            'SET NULL'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-job_list-status_id',
            'job_list'
        );

        $this->dropIndex(
            'idx-job_list-status_id',
            'job_list'
        );
    }
}
