<?php

use yii\db\Migration;

/**
 * Handles adding atelier_id to table `available`.
 */
class m210519_115205_add_atelier_id_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'atelier_id', $this->integer()->after('storage_id')->comment('Филиал'));

        $this->createIndex(
            'idx-available-atelier_id',
            'available',
            'atelier_id'
        );

        $this->addForeignKey(
            'fk-available-atelier_id',
            'available',
            'atelier_id',
            'atelier',
            'id',
            'SET NULL'
        );


        $this->addColumn('resource', 'atelier_id', $this->integer()->after('storage_id')->comment('Филиал'));

        $this->createIndex(
            'idx-resource-atelier_id',
            'resource',
            'atelier_id'
        );

        $this->addForeignKey(
            'fk-resource-atelier_id',
            'resource',
            'atelier_id',
            'atelier',
            'id',
            'SET NULL'
        );


        $this->addColumn('move', 'atelier_id', $this->integer()->after('storage_to')->comment('Филиал'));

        $this->createIndex(
            'idx-move-atelier_id',
            'move',
            'atelier_id'
        );

        $this->addForeignKey(
            'fk-move-atelier_id',
            'move',
            'atelier_id',
            'atelier',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-resource-atelier_id',
            'resource'
        );

        $this->dropIndex(
            'idx-resource-atelier_id',
            'resource'
        );

        $this->dropColumn('resource', 'atelier_id');

        $this->dropForeignKey(
            'fk-available-atelier_id',
            'available'
        );

        $this->dropIndex(
            'idx-available-atelier_id',
            'available'
        );

        $this->dropColumn('available', 'atelier_id');


        $this->dropForeignKey(
            'fk-move-atelier_id',
            'move'
        );

        $this->dropIndex(
            'idx-move-atelier_id',
            'move'
        );

        $this->dropColumn('move', 'atelier_id');
    }
}
