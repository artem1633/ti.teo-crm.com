<?php

use yii\db\Migration;

/**
 * Handles the creation of table `quick_order`.
 */
class m180606_143833_create_quick_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('quick_order', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->insert('quick_order',array(
            'name' => '25%',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('quick_order');
    }
}
