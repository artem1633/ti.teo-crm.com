<?php

use yii\db\Migration;

/**
 * Handles adding date to table `job_list`.
 */
class m201128_132522_add_date_column_to_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('job_list', 'date', $this->date()->comment('Предварительная дата'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('job_list', 'date');
    }
}
