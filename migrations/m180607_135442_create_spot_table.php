<?php

use yii\db\Migration;

/**
 * Handles the creation of table `spot`.
 */
class m180607_135442_create_spot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('spot', [
            'id' => $this->primaryKey(),
            'marking_id' => $this->integer()->comment('Марка'),
            'name' => $this->string(255),
        ]);

        $this->createIndex(
            'idx-spot-marking_id',
            'spot',
            'marking_id'
        );

        $this->addForeignKey(
            'fk-spot-marking_id',
            'spot',
            'marking_id',
            'marking',
            'id',
            'SET NULl'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-spot-marking_id',
            'spot'
        );

        $this->dropIndex(
            'idx-spot-marking_id',
            'spot'
        );

        $this->dropTable('spot');
    }
}
