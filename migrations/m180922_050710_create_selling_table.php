<?php

use yii\db\Migration;

/**
 * Handles the creation of table `selling`.
 */
class m180922_050710_create_selling_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('selling', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'storage_id' => $this->integer(),
            'client_id' => $this->integer(),
            'summa' => $this->float(),
            'comment' => $this->text(),
            'creater_id' => $this->integer(),
            'changed_id' => $this->integer(),
            'date_cr' => $this->datetime(),
            'date_up' => $this->datetime(),
        ]);

        $this->createIndex('idx-selling-company_id', 'selling', 'company_id', false);
        $this->addForeignKey("fk-selling-company_id", "selling", "company_id", "companies", "id");

        $this->createIndex('idx-selling-storage_id', 'selling', 'storage_id', false);
        $this->addForeignKey("fk-selling-storage_id", "selling", "storage_id", "storage", "id");

        $this->createIndex('idx-selling-client_id', 'selling', 'client_id', false);
        $this->addForeignKey("fk-selling-client_id", "selling", "client_id", "clients", "id");

        $this->createIndex('idx-selling-creater_id', 'selling', 'creater_id', false);
        $this->addForeignKey("fk-selling-creater_id", "selling", "creater_id", "users", "id");

        $this->createIndex('idx-selling-changed_id', 'selling', 'changed_id', false);
        $this->addForeignKey("fk-selling-changed_id", "selling", "changed_id", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-selling-company_id','selling');
        $this->dropIndex('idx-selling-company_id','selling');

        $this->dropForeignKey('fk-selling-storage_id','selling');
        $this->dropIndex('idx-selling-storage_id','selling');

        $this->dropForeignKey('fk-selling-client_id','selling');
        $this->dropIndex('idx-selling-client_id','selling');

        $this->dropForeignKey('fk-selling-creater_id','selling');
        $this->dropIndex('idx-selling-creater_id','selling');

        $this->dropForeignKey('fk-selling-changed_id','selling');
        $this->dropIndex('idx-selling-changed_id','selling');


        $this->dropTable('selling');
    }
}
