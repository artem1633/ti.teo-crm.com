<?php

use yii\db\Migration;

/**
 * Handles the creation of table `list_service_product`.
 */
class m201111_081515_create_list_service_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('list_service_product', [
            'id' => $this->primaryKey(),
            'list_service_id' => $this->integer()->comment('Услуга'),
            'product_id' => $this->integer()->comment('Товар'),
            'count' => $this->integer()->comment('Кол-во'),
        ]);

        $this->createIndex(
            'idx-list_service_product-list_service_id',
            'list_service_product',
            'list_service_id'
        );

        $this->addForeignKey(
            'fk-list_service_product-list_service_id',
            'list_service_product',
            'list_service_id',
            'list_services',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-list_service_product-product_id',
            'list_service_product',
            'product_id'
        );

        $this->addForeignKey(
            'fk-list_service_product-product_id',
            'list_service_product',
            'product_id',
            'product',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-list_service_product-product_id',
            'list_service_product'
        );

        $this->dropIndex(
            'idx-list_service_product-product_id',
            'list_service_product'
        );

        $this->dropForeignKey(
            'fk-list_service_product-list_service_id',
            'list_service_product'
        );

        $this->dropIndex(
            'idx-list_service_product-list_service_id',
            'list_service_product'
        );

        $this->dropTable('list_service_product');
    }
}
