<?php

use yii\db\Migration;

/**
 * Handles the creation of table `claim_statuses`.
 */
class m180606_095456_create_claim_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('claim_statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(10),
        ]);
         $this->insert('claim_statuses',array(
            'name' => 'Cоздано',
            'color' => '#ff0000',
        ));

        $this->insert('claim_statuses',array(
            'name' => 'Принята',
            'color' => '#339933',
        ));

        $this->insert('claim_statuses',array(
            'name' => 'Товар отправлен',
            'color' => '#ffff80',
        ));

        $this->insert('claim_statuses',array(
            'name' => 'Товар принят',
            'color' => '#80ffff',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('claim_statuses');
    }
}
