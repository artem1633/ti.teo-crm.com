<?php

use yii\db\Migration;

/**
 * Handles adding manufacturer_id to table `available`.
 */
class m201202_124015_add_manufacturer_id_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'manufacturer_id', $this->integer()->comment('Проивзодитель'));

        $this->createIndex(
            'idx-available-manufacturer_id',
            'available',
            'manufacturer_id'
        );

        $this->addForeignKey(
            'fk-available-manufacturer_id',
            'available',
            'manufacturer_id',
            'manufacturer',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-available-manufacturer_id',
            'available'
        );

        $this->dropIndex(
            'idx-available-manufacturer_id',
            'available'
        );

        $this->dropColumn('available', 'manufacturer_id');
    }
}
