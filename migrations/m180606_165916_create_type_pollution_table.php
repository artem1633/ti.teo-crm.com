<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_pollution`.
 */
class m180606_165916_create_type_pollution_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('type_pollution', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->text(),
        ]);

        $this->insert('type_pollution',array(
            'name' => 'Засаленность',
            'description' => '',
        ));

        $this->insert('type_pollution',array(
            'name' => 'Выгорание/побурение',
            'description' => '',
        ));

        $this->insert('type_pollution',array(
            'name' => 'Повреждения',
            'description' => '',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('type_pollution');
    }
}
