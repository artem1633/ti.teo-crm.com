<?php

use yii\db\Migration;

/**
 * Class m180620_081923_add_company_id_all_tables
 */
class m180620_081923_add_company_id_all_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx-users-company_id', 'users', 'company_id', false);
        $this->addForeignKey("fk-users-company_id", "users", "company_id", "companies", "id");

        $this->addCompanyIdField('atelier');//
        $this->addCompanyIdField('suppliers');//
        $this->addCompanyIdField('list_services');//
        $this->addCompanyIdField('product');//
        $this->addCompanyIdField('cashbox');//
        $this->addCompanyIdField('clients');//
        $this->addCompanyIdField('storage');//
        $this->addCompanyIdField('available');//
        $this->addCompanyIdField('resource');//
        $this->addCompanyIdField('orders');//
        $this->addCompanyIdField('move');//
        $this->addCompanyIdField('about_company');
        $this->addCompanyIdField('advertising');
        //$this->addCompanyIdField('articles');
        //$this->addCompanyIdField('claim_statuses');
        $this->addCompanyIdField('clients_group');
        $this->addCompanyIdField('goods');
        $this->addCompanyIdField('marking');
        $this->addCompanyIdField('orders_color');
        $this->addCompanyIdField('order_status');
        //$this->addCompanyIdField('product_status');
        $this->addCompanyIdField('quick_order');
        $this->addCompanyIdField('spot');
        $this->addCompanyIdField('type_clothes');
        $this->addCompanyIdField('type_pollution');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users-company_id','users');
        $this->dropIndex('idx-users-company_id','users');  
        
        $this->dropCompanyIdField('atelier');
        $this->dropCompanyIdField('suppliers');
        $this->dropCompanyIdField('list_services');
        $this->dropCompanyIdField('product');
        $this->dropCompanyIdField('cashbox');
        $this->dropCompanyIdField('clients');
        $this->dropCompanyIdField('storage');
        $this->dropCompanyIdField('available');
        $this->dropCompanyIdField('resource');
        $this->dropCompanyIdField('orders');
        $this->dropCompanyIdField('move');
        $this->dropCompanyIdField('about_company');
        $this->dropCompanyIdField('advertising');
        //$this->dropCompanyIdField('articles');
        //$this->dropCompanyIdField('claim_statuses');
        $this->dropCompanyIdField('clients_group');
        $this->dropCompanyIdField('goods');
        $this->dropCompanyIdField('marking');
        $this->dropCompanyIdField('orders_color');
        $this->dropCompanyIdField('order_status');
        //$this->dropCompanyIdField('product_status');
        $this->dropCompanyIdField('quick_order');
        $this->dropCompanyIdField('spot');
        $this->dropCompanyIdField('type_clothes');
        $this->dropCompanyIdField('type_pollution');
    }


    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "CASCADE"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
