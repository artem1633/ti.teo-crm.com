<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_order_status`.
 */
class m201121_154151_create_job_order_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_order_status', [
            'id' => $this->primaryKey(),
            'job_list_id' => $this->integer()->comment('Услуга'),
            'order_status_id' => $this->integer()->comment('Статус'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-job_order_status-job_list_id',
            'job_order_status',
            'job_list_id'
        );

        $this->addForeignKey(
            'fk-job_order_status-job_list_id',
            'job_order_status',
            'job_list_id',
            'job_list',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-job_order_status-order_status_id',
            'job_order_status',
            'order_status_id'
        );

        $this->addForeignKey(
            'fk-job_order_status-order_status_id',
            'job_order_status',
            'order_status_id',
            'order_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-job_order_status-user_id',
            'job_order_status',
            'user_id'
        );

        $this->addForeignKey(
            'fk-job_order_status-user_id',
            'job_order_status',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-job_order_status-user_id',
            'job_order_status'
        );

        $this->dropIndex(
            'idx-job_order_status-user_id',
            'job_order_status'
        );

        $this->dropForeignKey(
            'fk-job_order_status-order_status_id',
            'job_order_status'
        );

        $this->dropIndex(
            'idx-job_order_status-order_status_id',
            'job_order_status'
        );

        $this->dropForeignKey(
            'fk-job_order_status-job_list_id',
            'job_order_status'
        );

        $this->dropIndex(
            'idx-job_order_status-job_list_id',
            'job_order_status'
        );

        $this->dropTable('job_order_status');
    }
}
