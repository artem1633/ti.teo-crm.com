<?php

use yii\db\Migration;

/**
 * Handles adding job_id to table `move`.
 */
class m201224_133127_add_job_id_column_to_move_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('move', 'job_id', $this->integer()->comment('Услуга'));

        $this->createIndex(
            'idx-move-job_id',
            'move',
            'job_id'
        );

        $this->addForeignKey(
            'fk-move-job_id',
            'move',
            'job_id',
            'list_services',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-move-job_id',
            'move'
        );

        $this->dropIndex(
            'idx-move-job_id',
            'move'
        );

        $this->dropColumn('move', 'job_id');
    }
}
