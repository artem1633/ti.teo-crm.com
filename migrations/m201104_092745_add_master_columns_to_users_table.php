<?php

use yii\db\Migration;

/**
 * Handles adding master to table `users`.
 */
class m201104_092745_add_master_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'address', $this->string()->comment('Адрес'));
        $this->addColumn('users', 'state', $this->integer()->comment('Положение'));
        $this->addColumn('users', 'documents', $this->text()->comment('Документы'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'address');
        $this->dropColumn('users', 'state');
        $this->dropColumn('users', 'documents');
    }
}
