<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_list`.
 */
class m180613_064740_create_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_list', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'job_id' => $this->integer(),
            'type' => $this->string(255),
            'count' => $this->integer(),
            'price' => $this->float(),
            'material_pay' => $this->float()->comment('Оплата за материал'),
            'master_id' => $this->integer()->comment('Мастер'),
            'prepaid' => $this->float()->comment('Предоплата'),
            'description' => $this->text()->comment('Примечание'),
        ]);

        $this->createIndex(
            'idx-job_list-master_id',
            'job_list',
            'master_id'
        );

        $this->addForeignKey(
            'fk-job_list-master_id',
            'job_list',
            'master_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex('idx-job_list-order_id', 'job_list', 'order_id', false);
        $this->addForeignKey("fk-job_list-order_id", "job_list", "order_id", "orders", "id");

        $this->createIndex(
            'idx-job_list-job_id',
            'job_list',
            'job_id'
        );

        $this->addForeignKey(
            'fk-job_list-job_id',
            'job_list',
            'job_id',
            'list_services',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-job_list-order_id','job_list');
        $this->dropIndex('idx-job_list-order_id','job_list');

        $this->dropTable('job_list');
    }
}
