<?php

use yii\db\Migration;

/**
 * Handles adding type to table `clients`.
 */
class m201103_132650_add_type_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'type', $this->integer()->defaultValue(0)->comment('Тип'));
        $this->addColumn('clients', 'contact_person', $this->string()->comment('Контактное лицо'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'type');
        $this->dropColumn('clients', 'contact_person');
    }
}
