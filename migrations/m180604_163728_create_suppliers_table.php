<?php

use yii\db\Migration;

/**
 * Handles the creation of table `suppliers`.
 */
class m180604_163728_create_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('suppliers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('suppliers');
    }
}
