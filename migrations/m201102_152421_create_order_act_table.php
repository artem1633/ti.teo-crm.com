<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_act`.
 */
class m201102_152421_create_order_act_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_act', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'name' => $this->string()->comment('Наименование'),
            'count' => $this->integer()->comment('Кол-во'),
        ]);

        $this->createIndex(
            'idx-order_act-order_id',
            'order_act',
            'order_id'
        );

        $this->addForeignKey(
            'fk-order_act-order_id',
            'order_act',
            'order_id',
            'orders',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order_act-order_id',
            'order_act'
        );

        $this->dropIndex(
            'idx-order_act-order_id',
            'order_act'
        );

        $this->dropTable('order_act');
    }
}
