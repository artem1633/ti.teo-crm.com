<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_group`.
 * Has foreign keys to the tables:
 *
 * - `users`
 * - `group`
 */
class m201104_101635_create_junction_table_for_users_and_group_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_group', [
            'id' => $this->primaryKey(),
            'users_id' => $this->integer(),
            'group_id' => $this->integer(),
        ]);

        // creates index for column `users_id`
        $this->createIndex(
            'idx-users_group-users_id',
            'users_group',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-users_group-users_id',
            'users_group',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `group_id`
        $this->createIndex(
            'idx-users_group-group_id',
            'users_group',
            'group_id'
        );

        // add foreign key for table `group`
        $this->addForeignKey(
            'fk-users_group-group_id',
            'users_group',
            'group_id',
            'group',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-users_group-users_id',
            'users_group'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-users_group-users_id',
            'users_group'
        );

        // drops foreign key for table `group`
        $this->dropForeignKey(
            'fk-users_group-group_id',
            'users_group'
        );

        // drops index for column `group_id`
        $this->dropIndex(
            'idx-users_group-group_id',
            'users_group'
        );

        $this->dropTable('users_group');
    }
}
