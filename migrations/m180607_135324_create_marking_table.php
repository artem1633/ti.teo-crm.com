<?php

use yii\db\Migration;

/**
 * Handles the creation of table `marking`.
 */
class m180607_135324_create_marking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('marking', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('marking');
    }
}
