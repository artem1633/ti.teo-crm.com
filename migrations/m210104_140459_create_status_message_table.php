<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_message`.
 */
class m210104_140459_create_status_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_message', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'content' => $this->text()->comment('Текст'),
            'status_id' => $this->integer()->comment('Статус'),
            'files' => $this->text()->comment('Файлы'),
            'is_active' => $this->boolean()->defaultValue(false)->comment('Активный'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-status_message-status_id',
            'status_message',
            'status_id'
        );

        $this->addForeignKey(
            'fk-status_message-status_id',
            'status_message',
            'status_id',
            'order_status',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-status_message-status_id',
            'status_message'
        );

        $this->dropIndex(
            'idx-status_message-status_id',
            'status_message'
        );

        $this->dropTable('status_message');
    }
}
