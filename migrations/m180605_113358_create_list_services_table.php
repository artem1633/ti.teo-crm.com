<?php

use yii\db\Migration;

/**
 * Handles the creation of table `list_services`.
 */
class m180605_113358_create_list_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('list_services', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('list_services');
    }
}
