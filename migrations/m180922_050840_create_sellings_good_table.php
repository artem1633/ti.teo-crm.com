<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sellings_good`.
 */
class m180922_050840_create_sellings_good_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sellings_good', [
            'id' => $this->primaryKey(),
            'selling_id' => $this->integer(),
            'product_id' => $this->integer(),
            'price' => $this->float(),
            'count' => $this->integer(),
        ]);
        $this->createIndex('idx-sellings_good-selling_id', 'sellings_good', 'selling_id', false);
        $this->addForeignKey("fk-sellings_good-selling_id", "sellings_good", "selling_id", "selling", "id");

        $this->createIndex('idx-sellings_good-product_id', 'sellings_good', 'product_id', false);
        $this->addForeignKey("fk-sellings_good-product_id", "sellings_good", "product_id", "product", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-sellings_good-product_id','sellings_good');
        $this->dropIndex('idx-sellings_good-product_id','sellings_good');

        $this->dropForeignKey('fk-sellings_good-product_id','sellings_good');
        $this->dropIndex('idx-sellings_good-product_id','sellings_good');

        $this->dropTable('sellings_good');
    }
}
