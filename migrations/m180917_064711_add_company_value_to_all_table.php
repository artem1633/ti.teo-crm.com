<?php

use yii\db\Migration;
use app\models\Companies;
use app\models\manual\AboutCompany;
use app\models\manual\Articles;
use app\models\manual\ClaimStatuses;
use app\models\manual\Goods;
use app\models\manual\Marking;
use app\models\manual\ProductStatus;
use app\models\manual\QuickOrder;
use app\models\manual\OrderStatus;
use app\models\manual\Spot;
use app\models\Storage;
use app\models\manual\TypePollution;

/**
 * Class m180917_064711_add_company_value_to_all_table
 */
class m180917_064711_add_company_value_to_all_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $company = Companies::find()->one();
        if($company != null)
        {
            $aboutCompany = AboutCompany::find()->all();
            foreach ($aboutCompany as $value) 
            {
                Yii::$app->db->createCommand()->update('about_company', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            /*$articles = Articles::find()->all();
            foreach ($articles as $value) 
            {
                Yii::$app->db->createCommand()->update('articles', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }*/

            /*$claim = ClaimStatuses::find()->all();
            foreach ($claim as $value) 
            {
                Yii::$app->db->createCommand()->update('claim_statuses', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }*/

            $goods = Goods::find()->all();
            foreach ($goods as $value) 
            {
                Yii::$app->db->createCommand()->update('goods', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            $marking = Marking::find()->all();
            foreach ($marking as $value) 
            {
                Yii::$app->db->createCommand()->update('marking', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            $order_status = OrderStatus::find()->all();
            foreach ($order_status as $value) 
            {
                Yii::$app->db->createCommand()->update('order_status', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            /*$product_status = ProductStatus::find()->all();
            foreach ($product_status as $value) 
            {
                Yii::$app->db->createCommand()->update('product_status', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }*/

            $quick_order = QuickOrder::find()->all();
            foreach ($quick_order as $value) 
            {
                Yii::$app->db->createCommand()->update('quick_order', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            $spot = Spot::find()->all();
            foreach ($spot as $value) 
            {
                Yii::$app->db->createCommand()->update('spot', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            $storage = Storage::find()->all();
            foreach ($storage as $value) 
            {
                Yii::$app->db->createCommand()->update('storage', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }

            $type_pollution = TypePollution::find()->all();
            foreach ($type_pollution as $value) 
            {
                Yii::$app->db->createCommand()->update('type_pollution', ['company_id' => $company->id], [ 'id' => $value->id ])->execute();
            }
        }
    }

    public function down()
    {

    }
}
