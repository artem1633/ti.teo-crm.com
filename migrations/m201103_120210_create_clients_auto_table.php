<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_auto`.
 */
class m201103_120210_create_clients_auto_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients_auto', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'type_id' => $this->integer()->comment('Тип транспорта'),
            'marks_id' => $this->integer()->comment('Марка'),
            'spot_id' => $this->integer()->comment('Модель'),
            'number' => $this->string()->comment('Номер'),
            'passport' => $this->string()->comment('Тех. паспорт'),
            'color' => $this->string()->comment('Цвет'),
            'year' => $this->string()->comment('Год выпуска'),
        ]);

        $this->createIndex(
            'idx-clients_auto-client_id',
            'clients_auto',
            'client_id'
        );

        $this->addForeignKey(
            'fk-clients_auto-client_id',
            'clients_auto',
            'client_id',
            'clients',
            'id',
            'SET NULL'
        );

        $this->createIndex('idx-clients_auto-clothes_type', 'clients_auto', 'type_id', false);
        $this->addForeignKey("fk-clients_auto-clothes_type", "clients_auto", "type_id", "type_clothes", "id");

        $this->createIndex('idx-clients_auto-spot_id', 'clients_auto', 'spot_id', false);
        $this->addForeignKey("fk-clients_auto-spot_id", "clients_auto", "spot_id", "spot", "id");

        $this->createIndex('idx-clients_auto-marks_id', 'clients_auto', 'marks_id', false);
        $this->addForeignKey("fk-clients_auto-marks_id", "clients_auto", "marks_id", "marking", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-clients_auto-client_id','clients_auto');
        $this->dropIndex('idx-clients_auto-client_id','clients_auto');

        $this->dropForeignKey('fk-clients_auto-clothes_type','clients_auto');
        $this->dropIndex('idx-clients_auto-clothes_type','clients_auto');

        $this->dropForeignKey('fk-clients_auto-spot_id','clients_auto');
        $this->dropIndex('idx-clients_auto-spot_id','clients_auto');

        $this->dropForeignKey('fk-clients_auto-marks_id','clients_auto');
        $this->dropIndex('idx-clients_auto-marks_id','clients_auto');

        $this->dropTable('clients_auto');
    }
}
