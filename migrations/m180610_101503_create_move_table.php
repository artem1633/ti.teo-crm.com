<?php

use yii\db\Migration;

/**
 * Handles the creation of table `move`.
 */
class m180610_101503_create_move_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('move', [
            'id' => $this->primaryKey(),
            'table' => $this->string(255)->comment('Тип перемещения'),
            'storage_form' => $this->integer()->comment('От склада'),
            'storage_to' => $this->integer()->comment('На склад'),
            'old_count' => $this->integer()->comment('Старое значение'),
            'sending_count' => $this->integer()->comment('Новое значение '),
            'data' => $this->datetime()->comment('Дата/время'),
            'part_id' => $this->integer()->comment('Товар '),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('move');
    }
}
