<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180611_060408_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->comment('Номер'),
            'atelier_id' => $this->integer()->comment('Ателье'),
            'creator_id' => $this->integer()->comment('Создатель'),
            'acceptor_id' => $this->integer()->comment('Принял'),
            'fact_date' => $this->datetime()->comment('Фактическая дата'),
            'status_id' => $this->integer()->comment('Статус'),
            'issue_date' => $this->datetime()->comment('Выдать '),
            'order_type' => $this->integer()->comment('Тип заказа '),
            'client_id' => $this->integer()->comment('Клиент '),
            'telephone' => $this->string(255)->comment('Телефон'),
            'user_id' => $this->integer()->comment('Мастер'),
            'cost' => $this->float()->comment('Цена'),
            'clothes_type' => $this->integer()->comment('Тип одежды '),
            'prepay' => $this->float()->comment('Предоплата'),
            'sale' => $this->float()->comment('Скидка'),
            'comment' => $this->text()->comment('Комментарий'),
            'marketing_id' => $this->integer()->comment('Реклама'),
            'quick' => $this->integer()->comment('Срочность'),
            'color' => $this->string(255)->comment('Цвет изделия'),
            'fabric_structure' => $this->string(255)->comment('Состав ткани'),
            'material' => $this->string(255)->comment('Детали из искусственных материалов'),
            'symbol' => $this->string(255)->comment('Символика'),
            'product_warranty' => $this->string(255)->comment('Фурнитура без гарантии'),
            'complekt' => $this->string(255)->comment('Комплектность'),
            'defect' => $this->string(255)->comment('Особые свойства (дефекты)'),
            'pollution_id' => $this->integer()->comment('Степень и тип загрязнения'),
            'damage' => $this->string(255)->comment('Повреждения'),
            'spot_id' => $this->integer()->comment('Наличие пятен'),
            'spots_location' => $this->string(255)->comment('Местоположение пятен'),
            'marks_id' => $this->integer()->comment('Маркировка'),
            'wear' => $this->integer()->comment('Износ'),
            'lost_product' => $this->integer()->comment('Потеря товарного вида'),
            'hidden_defect' => $this->integer()->comment('Проявление скрытых дефектов'),
        ]);

        $this->createIndex('idx-orders-atelier_id', 'orders', 'atelier_id', false);
        $this->addForeignKey("fk-orders-atelier_id", "orders", "atelier_id", "atelier", "id");

        $this->createIndex('idx-orders-status_id', 'orders', 'status_id', false);
        $this->addForeignKey("fk-orders-status_id", "orders", "status_id", "order_status", "id");

        $this->createIndex('idx-orders-client_id', 'orders', 'client_id', false);
        $this->addForeignKey("fk-orders-client_id", "orders", "client_id", "clients", "id");

        $this->createIndex('idx-orders-user_id', 'orders', 'user_id', false);
        $this->addForeignKey("fk-orders-user_id", "orders", "user_id", "users", "id");

        $this->createIndex('idx-orders-acceptor_id', 'orders', 'acceptor_id', false);
        $this->addForeignKey("fk-orders-acceptor_id", "orders", "acceptor_id", "users", "id");

        $this->createIndex('idx-orders-creator_id', 'orders', 'creator_id', false);
        $this->addForeignKey("fk-orders-creator_id", "orders", "creator_id", "users", "id");

        $this->createIndex('idx-orders-clothes_type', 'orders', 'clothes_type', false);
        $this->addForeignKey("fk-orders-clothes_type", "orders", "clothes_type", "type_clothes", "id");

        $this->createIndex('idx-orders-marketing_id', 'orders', 'marketing_id', false);
        $this->addForeignKey("fk-orders-marketing_id", "orders", "marketing_id", "advertising", "id");

        $this->createIndex('idx-orders-quick', 'orders', 'quick', false);
        $this->addForeignKey("fk-orders-quick", "orders", "quick", "quick_order", "id");

        $this->createIndex('idx-orders-pollution_id', 'orders', 'pollution_id', false);
        $this->addForeignKey("fk-orders-pollution_id", "orders", "pollution_id", "type_pollution", "id");

        $this->createIndex('idx-orders-spot_id', 'orders', 'spot_id', false);
        $this->addForeignKey("fk-orders-spot_id", "orders", "spot_id", "spot", "id");

        $this->createIndex('idx-orders-marks_id', 'orders', 'marks_id', false);
        $this->addForeignKey("fk-orders-marks_id", "orders", "marks_id", "marking", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-orders-atelier_id','orders');
        $this->dropIndex('idx-orders-atelier_id','orders');
        
        $this->dropForeignKey('fk-orders-status_id','orders');
        $this->dropIndex('idx-orders-status_id','orders');

        $this->dropForeignKey('fk-orders-client_id','orders');
        $this->dropIndex('idx-orders-client_id','orders');

        $this->dropForeignKey('fk-orders-user_id','orders');
        $this->dropIndex('idx-orders-user_id','orders');

        $this->dropForeignKey('fk-orders-acceptor_id','orders');
        $this->dropIndex('idx-orders-acceptor_id','orders');
        
        $this->dropForeignKey('fk-orders-creator_id','orders');
        $this->dropIndex('idx-orders-creator_id','orders');

        $this->dropForeignKey('fk-orders-clothes_type','orders');
        $this->dropIndex('idx-orders-clothes_type','orders');

        $this->dropForeignKey('fk-orders-marketing_id','orders');
        $this->dropIndex('idx-orders-marketing_id','orders');

        $this->dropForeignKey('fk-orders-quick','orders');
        $this->dropIndex('idx-orders-quick','orders');

        $this->dropForeignKey('fk-orders-pollution_id','orders');
        $this->dropIndex('idx-orders-pollution_id','orders');

        $this->dropForeignKey('fk-orders-spot_id','orders');
        $this->dropIndex('idx-orders-spot_id','orders');

        $this->dropForeignKey('fk-orders-marks_id','orders');
        $this->dropIndex('idx-orders-marks_id','orders');

        $this->dropTable('orders');
    }
}
