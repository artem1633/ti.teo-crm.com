<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_status`.
 */
class m180606_090323_create_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('order_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->insert('order_status',array(
            'name' => 'Предварительный',
        ));

        $this->insert('order_status',array(
            'name' => 'Новый',
        ));

        $this->insert('order_status',array(
            'name' => 'В работе',
        ));

        $this->insert('order_status',array(
            'name' => 'На проверке',
        ));

        $this->insert('order_status',array(
            'name' => 'Выполненный',
        ));

        $this->insert('order_status',array(
            'name' => 'Отмененный',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('order_status');
    }
}
