<?php

use yii\db\Migration;

/**
 * Handles adding act_draw to table `orders`.
 */
class m201112_102324_add_act_draw_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'act_draw', $this->binary()->comment('Рисунок акта'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'act_draw');
    }
}
