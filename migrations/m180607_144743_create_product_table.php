<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180607_144743_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'cost' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
