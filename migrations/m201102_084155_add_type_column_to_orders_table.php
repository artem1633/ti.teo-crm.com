<?php

use yii\db\Migration;

/**
 * Handles adding type to table `orders`.
 */
class m201102_084155_add_type_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'type', $this->integer()->after('id')->comment('Тип'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'type');
    }
}
