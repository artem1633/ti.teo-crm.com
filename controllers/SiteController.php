<?php

namespace app\controllers;

use app\helpers\ChatAPI;
use app\helpers\TelegramRequester;
use app\models\JobList;
use app\models\manual\Marking;
use app\models\manual\Spot;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Logs;
use app\models\RegisterForm;
use app\models\ResetPasswordForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            return $this->render('index');
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    public function actionTest()
    {
//        $client = new Client(new Version1X('http://demo-sklad.loc:3002'));

//        $client->initialize();
//        $client->emit('update', ['foo' => 'bar']);
//        $client->close();

        file_get_contents('http://demo-sklad.loc:3002/update');
    }

    public function actionPublic()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('public');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id))
        {
            return $this->render('error');
        }        
        else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $log = new Logs([
                'user_id' => Yii::$app->user->id,
                'event_datetime' => date('Y-m-d H:i:s'),
                'event' => Logs::EVENT_USER_AUTHORIZED,
            ]);
            $log->description = $log->generateEventDescription();
            $log->save();
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $log = new Logs([
            'user_id' => null,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_OPEN_REGISTRATION_PAGE,
        ]);
        $log->description = $log->generateEventDescription();
        $log->save();

        $this->layout = 'main-login';

        $model = new RegisterForm();

        if($model->load(Yii::$app->request->post()) && $model->register())
        {
            Yii::$app->session->setFlash('register_success', 'Регистрация прошла успешно. Пожалуйста, авторизируйтесь');
            $login_model = new LoginForm();
            $login_model->username = $model->login;
            $login_model->password = $model->password;
            if ($login_model->login()) {
                return $this->goBack();
            }
            else return $this->redirect(['login']);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new ResetPasswordForm();

        if($model->load(Yii::$app->request->post()) && $model->reset())
        {
            Yii::$app->session->setFlash('register_success', 'На вашу почту был выслан временный пароль. Воспользуйтесь им для авторизации');
            return $this->redirect(['login']);
        } else {
            return $this->render('reset', [
                'model' => $model,
            ]);
        }
    }
}
