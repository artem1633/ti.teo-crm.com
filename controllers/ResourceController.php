<?php

namespace app\controllers;

use Yii;
use app\models\Resource;
use app\models\ResourceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ResourceController implements the CRUD actions for Resource model.
 */
class ResourceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Resource models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ResourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewAvailable($id)
    {   
        $resource = Resource::find()->where(['number' => $this->findModel($id)->number ])->all();
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Склад",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view-available', [
                        'result' => $resource,
                    ]),
                ];    
        }else{
            return $this->render('view-available', [
                'result' => $resource,
            ]);
        }
    }

    public function actionView($id)
    {   
        $resource = Resource::find()->where(['number' => $this->findModel($id)->number ])->all();
        return $this->render('view', [
            'result' => $resource,
        ]);
    }
    
    /**
     * Updates an existing Resource model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        $status = $model->status_id;      

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=> ( $status == 3 ? '#waiting-pjax' : '#remain-pjax' ),
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionChangeStatus($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        $status = $model->status_id;
        $model->status_id = 2;
        $model->save();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'forceReload'=> ( $status == 3 ? '#waiting-pjax' : '#remain-pjax' ),
            'forceClose'=>true,
        ];    
    }

    /**
     * Delete an existing Resource model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $status = $this->findModel($id)->status_id;   
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>( $status == 3 ? '#waiting-pjax' : '#remain-pjax' ),];
        }else{
            return $this->redirect(['index']);
        }


    }

    public function actionDeleteResource($id)
    {
        $request = Yii::$app->request;
        $resources = Resource::find()->where(['number' => $this->findModel($id)->number])->all();
        foreach ($resources as $value) {           
            $status = $this->findModel($id)->status_id;   
            $value->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>( $status == 3 ? '#waiting-pjax' : '#remain-pjax' ),];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);

        return parent::beforeAction($action);
    }

     /**
     * Delete multiple existing Resource model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Resource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resource::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
