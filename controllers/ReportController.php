<?php

namespace app\controllers;

use app\models\Clients;
use app\models\JobList;
use app\models\manual\ListServices;
use app\models\Orders;
use app\models\User;
use ElephantIO\Client;
use Yii;
use app\models\manual\Goods;
use app\models\manual\GoodsSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Available;

/**
 * ReportController implements the CRUD actions for Goods model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goods models.
     * @return mixed
     */
    public function actionIndex($dateStart = null, $dateEnd = null)
    {
        $loansData = [];
        $mastersData = [];
        $jobsData = [];


        if($dateStart){
            $dateStartParts = explode('.', $dateStart);
            if($dateStartParts){
                $dateStart = implode('-', array_reverse($dateStartParts));
            }
        }


        if($dateEnd){
            $dateEndParts = explode('.', $dateEnd);
            if($dateEndParts){
                $dateEnd = implode('-', array_reverse($dateEndParts));
            }
        }


        foreach (JobList::find()->where(['!=', 'job_list.status_id', 5])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->all() as $jobList)
        {
            $order = Orders::findOne($jobList->order_id);
            $client = Clients::findOne($order->client_id);

            $loansData[] = [
                'client_name' => $client->fio,
                'auto_number' => $order->number,
                'mark' => $order->marks->name,
                'spot' => $order->spot->name,
                'job_name' => $jobList->job->name,
                'amount' => $jobList->price - $jobList->prepaid,
            ];
        }

        foreach (User::find()->where(['role_id' => 'master'])->all() as $user)
        {
            foreach (JobList::find()->where(['job_list.status_id' => 5])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->all() as $jobList)
            {
                $order = Orders::findOne($jobList->order_id);

                $mastersData[] = [
                    'master_name' => $user->name,
                    'auto_number' => $order->number,
                    'mark' => $order->marks->name,
                    'spot' => $order->spot->name,
                    'job_name' => $jobList->job->name,
                    'amount' => $jobList->master_price - $jobList->master_prepaid,
                ];
            }
        }

        foreach (ListServices::find()->all() as $listService)
        {
            $jobsData[] = [
                'job_name' => $listService->name,
                'count' => JobList::find()->where(['job_id' => $listService->id])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->count(),
            ];
        }

        $loansDataProvider = new ArrayDataProvider([
            'allModels' => $loansData,
            'sort' => [
                'attributes' => ['client_name', 'auto_number', 'mark', 'spot', 'job_name', 'amount'],
            ],
        ]);


        $mastersDataProvider = new ArrayDataProvider([
            'allModels' => $mastersData,
            'sort' => [
                'attributes' => ['master_name', 'auto_number', 'mark', 'spot', 'job_name', 'amount'],
            ],
        ]);

        $jobsDataProvider = new ArrayDataProvider([
            'allModels' => $jobsData,
            'sort' => [
                'attributes' => ['job_name', 'count'],
            ],
        ]);

        return $this->render('index', [
            'loansDataProvider' => $loansDataProvider,
            'mastersDataProvider' => $mastersDataProvider,
            'jobsDataProvider' => $jobsDataProvider,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    public function actionCredits($dateStart = null, $dateEnd = null)
    {
        $loansData = [];

        foreach (JobList::find()->where(['!=', 'job_list.status_id', 5])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->all() as $jobList)
        {
            $order = Orders::findOne($jobList->order_id);
            $client = Clients::findOne($order->client_id);

            $loansData[] = [
                'client_name' => $client->fio,
                'auto_number' => $order->number,
                'mark' => $order->marks->name,
                'spot' => $order->spot->name,
                'job_name' => $jobList->job->name,
                'amount' => $jobList->price - $jobList->prepaid,
            ];
        }

        $loansDataProvider = new ArrayDataProvider([
            'allModels' => $loansData,
            'sort' => [
                'attributes' => ['client_name', 'auto_number', 'mark', 'spot', 'job_name', 'amount'],
            ],
        ]);

        return $this->render('credits', [
            'loansDataProvider' => $loansDataProvider,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    public function actionMasters($dateStart = null, $dateEnd = null)
    {
        $mastersData = [];

        foreach (User::find()->where(['role_id' => 'master'])->all() as $user)
        {
            foreach (JobList::find()->where(['job_list.status_id' => 5])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->all() as $jobList)
            {
                $order = Orders::findOne($jobList->order_id);

                $mastersData[] = [
                    'master_name' => $user->name,
                    'auto_number' => $order->number,
                    'mark' => $order->marks->name,
                    'spot' => $order->spot->name,
                    'job_name' => $jobList->job->name,
                    'amount' => $jobList->master_price - $jobList->master_prepaid,
                ];
            }
        }

        $mastersDataProvider = new ArrayDataProvider([
            'allModels' => $mastersData,
            'sort' => [
                'attributes' => ['master_name', 'auto_number', 'mark', 'spot', 'job_name', 'amount'],
            ],
        ]);


        return $this->render('masters', [
            'mastersDataProvider' => $mastersDataProvider,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    public function actionServices($dateStart = null, $dateEnd = null)
    {
        $jobsData = [];


        foreach (ListServices::find()->all() as $listService)
        {
            $jobsData[] = [
                'job_name' => $listService->name,
                'count' => JobList::find()->where(['job_id' => $listService->id])->andFilterWhere(['between', 'fact_date', $dateStart, $dateEnd])->joinWith(['order'])->count(),
            ];
        }

        $jobsDataProvider = new ArrayDataProvider([
            'allModels' => $jobsData,
            'sort' => [
                'attributes' => ['job_name', 'count'],
            ],
        ]);


        return $this->render('services', [
            'jobsDataProvider' => $jobsDataProvider,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);

        if(Yii::$app->user->identity->can('reports') == false){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }
}
