<?php

namespace app\controllers;

use app\helpers\ChatAPI;
use app\models\JobListProduct;
use app\models\manual\OrderStatus;
use app\models\Move;
use app\models\Orders;
use app\models\Resource;
use app\models\StatusMessage;
use app\models\Users;
use Yii;
use app\models\JobList;
use app\models\JobListSearch;
use app\models\manual\ListServices;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\helpers\TelegramRequester;
use app\modules\api\controllers\TelegramNotifyController;

/**
 * JobListController implements the CRUD actions for JobList model.
 */
class JobListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JobList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JobList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JobList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JobList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JobList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param int $id
     * @param string $reloadPjaxContainer
     * @return array
     */
    public function actionApplyStatus($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($request->isGet)
        {
            return [
                'title'=> "Установить статус",
                'size' => 'normal',
                'content'=> $this->renderAjax('apply_status',[
                    'model' => $model,
                ]),
                'footer'=> Html::button('Нет',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Да',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
        else
        {
            if($model->load($request->post()) && $model->save(false))
            {
                $order = Orders::findOne($model->order_id);
                if($order && $model->status_id == 3)
                {
                    $order->status_id = 3;


                    // Списание со склада
                    $jobs = JobList::find()->where(['order_id' => $model->order_id])->all();
                    foreach ($jobs as $job)
                    {
                        $service = ListServices::findOne($job->job_id);
                        if($service){
                            \Yii::warning('SERVICE FOUND');
//                    $products = ListServiceProduct::find()->where(['list_service_id' => $service->id])->all();
                            $products = JobListProduct::find()->where(['order_id' => $model->order_id, 'job_list_id' => $job->id])->all();
                            Yii::warning($products, 'PRODUCTS');
                            foreach ($products as $product){
                                $res = Resource::find()->where(['product_id' => $product->product_id, 'atelier_id' => Yii::$app->user->identity->atelier_id])->one();
                                if($res != null){
                                    $oldCount = $res->count;

                                    $res->count = $res->count - $product->count;
                                    $result = $res->save(false);
                                    $move = new Move([
                                        'old_count' => $oldCount,
                                        'sending_count' =>  $res->count,
                                        'atelier_id' => Yii::$app->user->identity->atelier_id,
                                        'part_id' => $product->product_id,
                                        'storage_to' => $job->master_id,
                                        'job_id' => $job->job->id,
                                        'data' => date('Y-m-d H:i:s'),
                                        'table' => 'resource',
                                    ]);
                                    $move->save(false);
                                    Yii::warning($result, 'Resource saved');
                                }
                            }
                        }
                    }

                    $order->save(false, null, false);
                }

                $jobListsCount = JobList::find()->where(['order_id' => $model->order_id])->andWhere(['status_id' => 5])->count();
                if($jobListsCount == JobList::find()->where(['order_id' => $model->order_id])->count()){
                    JobList::updateAll(['status_id' => 7], ['order_id' => $model->order_id]);

                    $order = Orders::findOne($model->order_id);
                    if($order)
                    {
                        $order->status_id = 3;


                        $order->save(false, null, false);
                    }

                    $model->save(false);
                }

                if($model->status_id == 8){
                    $jobLists = JobList::find()->where(['order_id' => $model->order_id])->all();
                    foreach ($jobLists as $jobList)
                    {
                        $jobList->status_id = 8;
                        $jobList->save(false);
                    }

                    \Yii::warning('СПИСАНИЕ СО СКЛАДА', 'СПИСАНИЕ СО СКЛАДА');

                }

                if($model->status_id == 5){ // Статус "Выволнено"
                    $master = Users::find()->where(['id' => $model->master_id])->one();

                    if($master){
                        $result = TelegramNotifyController::sendReq('sendMessage', [
                            'chat_id' => $master->vk_id,
                            'text' => 'Услуга "'.$model->job->name.'" (Заказ #'.$model->order_id.') Выполнена',
                            'parse_mode' => 'HTML',
                        ]);
                    }
                }

                // Отправка WhatsApp сообшения
                /** @var StatusMessage $message */
                $order = Orders::findOne($model->order_id);
                $message = StatusMessage::find()->where(['status_id' => $model->status_id, 'is_active' => true])->one();
                if($message)
                {
                    if($order)
                    {
                        if($order->telephone != null){
                            if($message->files == null){
                                ChatAPI::sendMessage($order->telephone, $message->content);
                            } else {
                                $files = explode(',', $message->files);
                                if(count($files) > 0){
                                    $counter = 1;
                                    foreach ($files as $file)
                                    {
                                        if($file == '')
                                            continue;

                                        ChatAPI::sendFile($order->telephone, $file, ($counter == 1 ? $message->content : null));
                                        $counter++;
                                    }
                                } else {
                                    ChatAPI::sendMessage($order->telephone, $message->content);
                                }
                            }
                        }
                    }
                }

                $users = Users::find()->where(['accept_notifications' => true])->all();

                $status = OrderStatus::findOne($model->status_id);

                foreach ($users as $user){
                    if($user->vk_id != null){
                        $result = TelegramRequester::send('sendMessage', [
                            'chat_id' => $user->vk_id,
                            'text' => "Статус у заявки {$order->id} изменен на «{$status->name}»",
                            'parse_mode' => 'HTML',
                        ]);
                    }
                }


                return [
                    'forceReload'=>$reloadPjaxContainer,
                    'forceClose' => true,
                ];
            }


            else
            {
                return [
                    'title'=> "Установить статус",
                    'content'=>$this->renderAjax('apply_status', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }
    }


    /**
     * @param int $id
     * @param string $reloadPjaxContainer
     * @return array
     */
    public function actionApplyMaster($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($request->isGet)
        {
            return [
                'title'=> "Назначить мастера",
                'size' => 'normal',
                'content'=> $this->renderAjax('apply_master',[
                    'model' => $model,
                ]),
                'footer'=> Html::button('Нет',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Да',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
        else
        {
            if($model->load($request->post()) && $model->save(false))
            {
                $order = Orders::findOne($model->order_id);
                $master = Users::findOne($model->master_id);
                if($master && $order){
                    if($master->vk_id){

                        $keyboard = [];

                        $listService = ListServices::findOne($model->job_id);
                        $carName = ArrayHelper::getValue($order, 'marks.name').' '.ArrayHelper::getValue($order, 'spot.name').' '.$order->number;
//                        $keyboard[] = [['text' => 'Услуга "'.$listService->name.'" (Заказ #'.$this->id.'), '.$carName.' | Начать работу', 'callback_data' => 'job_'.$joblist->id]];


                        $result = TelegramRequester::send('sendMessage', [
                            'chat_id' => $master->vk_id,
                            'text' => 'Вам назначены следующие услуги: Услуга "'.$listService->name.'" (Заказ #'.$order->id.'), '.$carName,
                            'parse_mode' => 'HTML',
                            'reply_markup' => json_encode([
                                'inline_keyboard' => [[['text' => 'Начать работу', 'callback_data' => 'job_'.$model->id]]],
                            ])
                        ]);

                        $model->save(false);

                        Yii::warning($result, 'Telegram API Response');
                    }
                }

                return [
                    'forceReload'=>$reloadPjaxContainer,
                    'forceClose' => true,
                ];
            }
            else
            {
                return [
                    'title'=> "Назначить мастера",
                    'content'=>$this->renderAjax('apply_master', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);

        return parent::beforeAction($action);
    }

    /**
     * Deletes an existing JobList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JobList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JobList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JobList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
