<?php

namespace app\controllers;

use app\models\JobListProduct;
use app\models\JobListSearch;
use app\models\manual\ListServiceProduct;
use app\models\Move;
use app\models\OrderAct;
use app\models\OrderActSearch;
use app\models\Resource;
use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Clients;
use yii\helpers\ArrayHelper;
use app\models\manual\OrderStatus;
use app\models\JobList;
use yii\data\ActiveDataProvider;
use app\models\manual\ListServices;
use app\models\manual\Product;
use app\models\StatusMessage;
use app\helpers\ChatAPI;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],*/
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex($l = 10, $scrollTo = null)
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $dataProvider->query->andWhere(['orders.atelier_id' => Yii::$app->user->identity->atelier_id]);


        \Yii::warning($dataProvider->getCount().' '.$l, 'TOTAL COUNT');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'l' => $l,
            'scrollTo' => $scrollTo,
            'enableScroll' => ($dataProvider->getTotalCount() >= $l)
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $tab = 'home-fill')
    {
//        $visualButton = true;
//        $sum = 0;
//        $statusOrder = ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name');
        $model = $this->findModel($id);

//        if ($id > 0)
//        {
//            $query = JobList::find()->where(['order_id'=>$id]);
//            $jobList =JobList::find()->where(['order_id'=>$id])->all();
//            foreach($jobList as $job){$sum += $job->price * $job->count;}   //считаем сумму по заказу
//            if($model->sale > 0)
//            {
//                $discount = ($sum / 100) * $model->sale;
//                $sum = $sum - $discount;
//            }
//            if($model->prepay > 0) $sum = $sum - $model->prepay;
//        }
//        else $query = JobList::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'sort'=>array(
//                'defaultOrder'=>['id' => SORT_DESC],
//            ),
//            'pagination' => [
//                'pageSize'      => 10,
//                'validatePage'  => false,
//            ],
//
//        ]);
//
//        unset( $statusOrder[3] );
//        if ($model->status_id == 3)  $visualButton = false;
//
//        $contentJob = $this->renderPartial('job', [
//            'title' =>  'Заказ #' .$id,
//            'label' => 'Заказ',
//            'id'    =>$id,
//            'sum'   => $sum,
//            'visualButton'  => $visualButton,
//            'dataProvider'  => $dataProvider,
//            'statusOrder'   => $statusOrder,
//            'model'         => $model,
//        ]);
//
//        $contentOrder = $this->renderPartial('_ajax_view', [
//            'model' => $model,
//            'title' => 'Просмотр заказа',
//            'label' => 'Заказы',
//        ]);


        $jobListSearchModel = new JobListSearch();
        $jobListDataProvider = $jobListSearchModel->search(Yii::$app->request->queryParams);
        $jobListDataProvider->query->andWhere(['order_id' => $id]);
        $jobListDataProvider->pagination = false;

        $orderSearchModel = new OrderActSearch();
        $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams);
        $orderDataProvider->query->andWhere(['order_id' => $id]);
        $orderDataProvider->pagination = false;



        return $this->render('view', [
            'tab' => $tab,
            'jobListSearchModel' => $jobListSearchModel,
            'jobListDataProvider' => $jobListDataProvider,
            'orderSearchModel' => $orderSearchModel,
            'orderDataProvider' => $orderDataProvider,
            'model' => $model,
        ]);
    }

    public function actionNewStatus($idOrder, $newSum, $status_id)
    {
        $model = Orders::findOne($idOrder);
        $model->status_id = $status_id;
        $model->cost = $newSum;
        $model->save();

        $this->redirect(['view','id' => $idOrder]);
    }

    public function actionAddJobOld($id, $job)
    {
        $request = Yii::$app->request;
        $model = new JobList();
        $service = ArrayHelper::map(ListServices::find()->all(), 'id', 'name');
        $products = ArrayHelper::map(Product::find()->all(), 'id', 'name');
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> $job == 'service' ? "Услуги" : "Товары",
                'content'=>'<span class="text-success">Успешно выпольнено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Добавить ещё',['/orders/add-job','id'=>$id,'job' => $job],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }
        else
        {
            $model->count = 1;
            return [
                'title'=> "Добавить услугу",
                'size' => 'normal',
                'content'=>$this->renderAjax('modals/_form_joblist', [
                    'title' => 'Добавить к заказу: ',
                    'label' => 'Список работ',
                    'id' =>$id,
                    'model' => $model,
                    'service' => $service,
                    'products' => $products,
                    'job' => $job,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionAddJob($id, $job)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $service = ArrayHelper::map(ListServices::find()->all(), 'id', 'name');
        $products = ArrayHelper::map(Product::find()->all(), 'id', 'name');
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> $job == 'service' ? "Услуги" : "Товары",
                'content'=>'<span class="text-success">Успешно выполнено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Добавить ещё',['/orders/add-job','id'=>$id,'job' => $job],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }
        else
        {
            $model->jobs = JobList::find()->where(['order_id' => $model->id])->asArray()->all();

            $model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
            $model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();


            return [
                'title'=> "Добавить услугу",
                'size' => 'normal',
                'content'=>$this->renderAjax('modals/_form_joblist', [
                    'title' => 'Добавить к заказу: ',
                    'label' => 'Список работ',
                    'id' =>$id,
                    'model' => $model,
                    'service' => $service,
                    'products' => $products,
                    'job' => $job,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionUpdateJobList($id, $job, $job_id)
    {
        $request = Yii::$app->request;
        $model = JobList::findOne($job_id);
        $service = ArrayHelper::map(ListServices::find()->all(), 'id', 'name');
        $products = ArrayHelper::map(Product::find()->all(), 'id', 'name');
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose'=>true,
            ];
        }
        else
        {

            return [
                'title'=> "Изменить",
                'size' => 'normal',
                'content'=>$this->renderAjax('modals/_form_joblist', [
                    'title' => 'Добавить к заказу: ',
                    'label' => 'Список работ',
                    'id' =>$id,
                    'model' => $model,
                    'service' => $service,
                    'products' => $products,
                    'job' => $job,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionJobListDelete($id, $order_id)
    {
        $request = Yii::$app->request;
        JobList::findOne($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['view', 'id' => $order_id]);
        }


    }

    /**
     * Creates a new Orders model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Orders();

        $model->load($request->post());


        if ($request->post()['Orders']['step'] == 2 && $model->validate())
        {
//            $model->act_draw = $_POST['act_draw'];

            $redirect = ArrayHelper::getValue($_POST, 'redirect');
            $back = ArrayHelper::getValue($_POST, 'back');

            if($back == 1){
                $model->step = 1;

                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if($redirect == 1){
                $model->save(false, null, true, false);
                return $this->redirect(['signature', 'id' => $model->id]);
            } else {
                $model->save(false);
                return $this->redirect(['index']);
            }
        }
        if($request->post()['Orders']['step'] == 1 && $model->validate())
        {
            $model->step = 2;

            if($model->pre_date != null){
                $model->save(false);
                return $this->redirect(['index']);
            }

//            $model->save();
//            return $this->redirect(['index']);
        }
        else
        {
            $model->step = 1;
        }
        if($request->post()['Orders']['step'] == null)
        {
            $model->step = 1;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionEditOrder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#order-pjax',
                'forceClose' => true,
            ];
        } else {
            $model->quick_value = $model->quick;
            return [
                'title'=> "Редактировать",
                "size" => "large",
                'content'=>$this->renderAjax('modals/_form_order', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

//    public function actionConvertJobs()
//    {
//        /** @var JobList[] $jobLists */
//        $jobLists = JobList::find()->all();
//
//        foreach ($jobLists as $jobList)
//        {
//            $job = ListServices::findOne($jobList->job_id);
//            if($job)
//            {
//                /** @var ListServiceProduct[] $products */
//                $products = ListServiceProduct::find()->where(['list_service_id' => $job->id])->all();
//
//                VarDumper::dump($products, 10, true);
//
//
//                foreach ($products as $product)
//                {
//                    (new JobListProduct([
//                        'order_id' => $jobList->order_id,
//                        'job_list_id' => $jobList->id,
//                        'product_id' => $product->product_id,
//                        'count' => $product->count,
//                    ]))->save(false);
//                }
//            }
//        }
//    }



    /**
     * Updates an existing Customer model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->jobs = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->jobs = JobList::find()->where(['order_id' => $model->id])->asArray()->all();

        $model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();

        $model->load($request->post());

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                $model->step = 1;
                return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                if ($request->post()['Orders']['step'] == 2 && $model->validate())
                {
                    $model->act_draw = $_POST['act_draw'];
                    $model->save(false);
                    return [
                        'forceReload'=>'#detail-pjax',
                        'forceClose' => true,
                    ];
                }
                if($request->post()['Orders']['step'] == 1 && $model->validate())
                {
                    $model->step = 2;
//            $model->save();
//            return $this->redirect(['index']);
                }
                else
                {
                    $model->step = 1;
                }
                if($request->post()['Orders']['step'] == null)
                {
                    $model->step = 1;
                }

                return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else{
                return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{


            if ($request->post()['Orders']['step'] == 2 && $model->validate())
            {
                $model->act_draw = $_POST['act_draw'];
                $model->save();
                return $this->redirect(['index']);
            }
            if($request->post()['Orders']['step'] == 1 && $model->validate())
            {
                $model->step = 2;
//            $model->save();
//            return $this->redirect(['index']);
            }
            else
            {
                $model->step = 1;
            }
            if($request->post()['Orders']['step'] == null)
            {
                $model->step = 1;
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param int $id
     * @param string $reloadPjaxContainer
     * @return array
     */
    public function actionApplyStatus($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($request->isGet)
        {
            return [
                'title'=> "Установить статус",
                'size' => 'normal',
                'content'=> $this->renderAjax('apply_status',[
                    'model' => $model,
                ]),
                'footer'=> Html::button('Нет',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Да',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
        else
        {
            if($model->load($request->post()) && $model->save(false, null, false))
            {
                return [
                    'forceReload'=>$reloadPjaxContainer,
                    'forceClose' => true,
                ];
            }
            else
            {
                return [
                    'title'=> "Установить статус",
                    'content'=>$this->renderAjax('apply_status', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }
    }

    /**
     * Updates an existing Customer model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCommon($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->jobs = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->jobs = JobList::find()->where(['order_id' => $model->id])->asArray()->all();

        $model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            $model->step = 1;
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save(false)){
                return [
                    'forceReload'=>'#detail-pjax',
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update_common', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{


            if ($request->post()['Orders']['step'] == 2 && $model->validate())
            {
                $model->act_draw = $_POST['act_draw'];
                $model->save();
                return $this->redirect(['index']);
            }
            if($request->post()['Orders']['step'] == 1 && $model->validate())
            {
                $model->step = 2;
//            $model->save();
//            return $this->redirect(['index']);
            }
            else
            {
                $model->step = 1;
            }
            if($request->post()['Orders']['step'] == null)
            {
                $model->step = 1;
            }


            if($request->isPost){
                $model->load($request->post());
                $model->save(false);
                return $this->redirect(['orders/view', 'id' => $model->id]);
            }

            return $this->render('update_common', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateAdditional($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->jobs = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->jobs = JobList::find()->where(['order_id' => $model->id])->asArray()->all();

        $model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();


        $model->load($request->post());


        if ($request->post()['Orders']['step'] == 2 && $model->validate())
        {
            $model->act_draw = $_POST['act_draw'];

            $redirect = ArrayHelper::getValue($_POST, 'redirect');
            $back = ArrayHelper::getValue($_POST, 'back');

            if($back == 1){
                $model->step = 1;

                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if($redirect == 1){
                $model->save(false, null, true, false);
                return $this->redirect(['signature', 'id' => $model->id]);
            } else {
                $model->save(false);
                return $this->redirect(['index']);
            }
        }
        $model->step = 2;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdateJobProduct($id)
    {
        $request = Yii::$app->request;
        $model = JobList::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('update-job-product', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

                if($model->productList != null)
                {
                    $contacts = JobListProduct::find()->where(['job_list_id' => $model->id])->all();

                    foreach ($contacts as $contact){
                        $delete = true;

                        foreach ($model->productList as $item){
                            if($item['id'] == $contact->id){
                                $delete = false;
                            }
                        }

                        if($delete){
                            $contact->delete();
                        }
                    }


                    foreach ($model->productList as $item) {
                        $contact = JobListProduct::find()->where(['id' => $item['id']])->one();

                        if (!$contact) {
                            (new JobListProduct([
                                'job_list_id' => $model->id,
                                'product_id' => $item['product_id'],
                                'count' => $item['count'],
                            ]))->save(false);
                        }else{
                            $contact->job_list_id = $model->id;
                            $contact->product_id = $item['product_id'];
                            $contact->count = $item['count'];
                            $contact->save(false);
                        }
                    }
                } else {
                    JobListProduct::deleteAll(['job_list_id' => $model->id]);
                }

                return [
//                    'forceReload'=>'#crud-datatable-pjax',
                   'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Обновить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('update-job-product', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('/manual/list-services/update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEditDetail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#detail-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                "size" => "large",
                'content'=>$this->renderAjax('modals/_form_detail', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionUpdateAct($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->jobs = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->jobs = JobList::find()->where(['order_id' => $model->id])->asArray()->all();

        $model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
        $model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();



        if ($model->load($request->post()) && $model->validate()) {
            $model->act_draw = $_POST['act_draw'];
            $model->save();
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update_act', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionSignature($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->isPost){
            if($model->status_id == 3) {
                $model->signature_admin_second = $_POST['signature_admin'];
                $model->signature_client_second = $_POST['signature_client'];
            } else {
                $model->signature_admin = $_POST['signature_admin'];
                $model->signature_client = $_POST['signature_client'];
            }
            $model->save(false, null, false);

            if($model->status_id == 2){
                $message = StatusMessage::find()->where(['status_id' => $model->status_id, 'is_active' => true])->one();
                if($message)
                {
                    if($model->telephone != null){
                        if($message->files == null){
                            ChatAPI::sendMessage($model->telephone, $message->content);
                        } else {
                            $files = explode(',', $message->files);
                            if(count($files) > 0){
                                $counter = 1;
                                foreach ($files as $file)
                                {
                                    if($file == '')
                                        continue;

                                    ChatAPI::sendFile($model->telephone, $file, ($counter == 1 ? $message->content : null));
                                    $counter++;
                                }
                            } else {
                                ChatAPI::sendMessage($model->telephone, $message->content);
                            }
                        }
                    }
                }
            }

            return $this->redirect(['orders/index']);
        } else {
            return $this->render('signature', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoadImg()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['data'])){

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            if(is_dir('uploads/signatures') == false){
                mkdir('uploads/signatures');
            }

            $data_uri = $_POST['data'];
            $encoded_image = explode(",", $data_uri)[1];
            $decoded_image = base64_decode($encoded_image);

            $name = Yii::$app->security->generateRandomString();

            $path = "uploads/signatures/{$name}.png";

            file_put_contents($path, $decoded_image);

            return ['path' => $path];
        }

        return ['path' => null];
    }

    public function actionEditClient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = Clients::findOne($id);

        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#client-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                "size" => "large",
                'content'=>$this->renderAjax('modals/_form_client', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    /**
     * @param int $id
     * @param int $status_id
     * @return array
     */
    public function actionChangeStatus($id, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status_id = $value;


        if($model->status_id == 3) { // Статус "В работе"
            $jobs = JobList::find()->where(['order_id' => $model->id])->all();

            foreach ($jobs as $job)
            {
                Yii::warning('order has jobs');
                $service = ListServices::findOne($job->job_id);
                if($service){
                    Yii::warning('found service');
                    $products = ListServiceProduct::find()->where(['list_service_id' => $service->id])->all();
                    foreach ($products as $product){
                        $res = Resource::find()->where(['product_id' => $product->product_id, 'atelier_id' => Yii::$app->user->identity->atelier_id])->one();
                        Yii::warning('in product');
                        if($res != null){
                            Yii::warning('Resource found');
                            $oldCount = $res->count;

                            $res->count = $res->count - $product->count;
                            $result = $res->save(false);
                            $move = new Move([
                                'old_count' => $oldCount,
                                'sending_count' =>  $res->count,
                                'part_id' => $product->product_id,
                                'atelier_id' => Yii::$app->user->identity->atelier_id,
                                'storage_to' => $job->master_id,
                                'job_id' => $job->id,
                                'data' => date('Y-m-d H:i:s'),
                                'table' => 'resource',
                            ]);
                            $move->save(false);
                            Yii::warning($result, 'Resource saved');
                        }
                    }
                }
            }
        }

        return ['result' => $model->save(false, null, false)];
    }

    public function actionSetPhone($id)
    {
        $client = Clients::findOne($id);
        return $client->telephone;
    }

    public function actionSetAtelier($id)
    {
        $client = Clients::findOne($id);
        return $client->atelier_id;
    }

    public function actionGetProductPrice($id)
    {
        $product = Product::findOne($id);
        return $product->cost;
    }

    /**
     * Delete an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' ));
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest == false){
            $user = Yii::$app->user->identity;
            $user->last_activity_datetime = date('Y-m-d H:i:s');
            $user->save(false);


            if(Yii::$app->user->identity->can('orders') == false){
                return $this->redirect(['site/public']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
