<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Settings;
use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        if($request->isPost)
        {
            $data = $request->post();
            foreach ($data['Settings'] as $key => $value){
                $setting = Settings::findByKey($key);

                if($setting != null){
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }
        }

        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);


        if(Yii::$app->user->identity->isSuperAdmin() === false)
            throw new ForbiddenHttpException('Доступ запрещен');

        return parent::beforeAction($action);
    }
}
