<?php

namespace app\controllers;

use app\models\ClientsAuto;
use app\models\ClientsAutoSearch;
use app\models\OrdersSearch;
use Yii;
use app\models\Clients;
use app\models\ClientsSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],*/
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type' => Clients::TYPE_PERSON]);

        $dataProvider->query->andWhere(['atelier_id' => Yii::$app->user->identity->atelier_id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndexCompany()
    {
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type' => Clients::TYPE_COMPANY]);

        $dataProvider->query->andWhere(['atelier_id' => Yii::$app->user->identity->atelier_id]);

        return $this->render('index-company', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetNameList()
    {
        $clients = Clients::find()->where(['type' => Clients::TYPE_PERSON])->all();

        $output = '';

        foreach ($clients as $client){
            $output .= '<option value="'.$client->id.'">'.$client->fio.'</option>';
        }

        return $output;
    }

    public function actionGetCompanyList()
    {
        $clients = Clients::find()->where(['type' => Clients::TYPE_COMPANY])->all();

        $output = '';

        foreach ($clients as $client){
            $output .= '<option value="'.$client->id.'">'.$client->company_name.'</option>';
        }

        return $output;
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;

        $autoSearchModel = new ClientsAutoSearch();
        $autoDataProvider = $autoSearchModel->search(Yii::$app->request->queryParams);
        $autoDataProvider->query->andWhere(['client_id' => $id]);
        $autoDataProvider->pagination = false;

        $orderSearchModel = new OrdersSearch();
        $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams);
        $orderDataProvider->query->andWhere(['client_id' => $id]);
        $orderDataProvider->pagination = false;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ФИО : ".$this->findModel($id)->fio,
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'autoSearchModel' => $autoSearchModel,
                        'autoDataProvider' => $autoDataProvider,
                        'orderSearchModel' => $orderSearchModel,
                        'orderDataProvider' => $orderDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'autoSearchModel' => $autoSearchModel,
                'autoDataProvider' => $autoDataProvider,
                'orderSearchModel' => $orderSearchModel,
                'orderDataProvider' => $orderDataProvider,
            ]);
        }
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewCompany($id)
    {
        $request = Yii::$app->request;

        $autoSearchModel = new ClientsAutoSearch();
        $autoDataProvider = $autoSearchModel->search(Yii::$app->request->queryParams);
        $autoDataProvider->query->andWhere(['client_id' => $id]);
        $autoDataProvider->pagination = false;

        $orderSearchModel = new OrdersSearch();
        $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams);
        $orderDataProvider->query->andWhere(['client_id' => $id]);
        $orderDataProvider->pagination = false;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "ФИО : ".$this->findModel($id)->fio,
                'size' => 'normal',
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                    'autoSearchModel' => $autoSearchModel,
                    'autoDataProvider' => $autoDataProvider,
                    'orderSearchModel' => $orderSearchModel,
                    'orderDataProvider' => $orderDataProvider,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'autoSearchModel' => $autoSearchModel,
                'autoDataProvider' => $autoDataProvider,
                'orderSearchModel' => $orderSearchModel,
                'orderDataProvider' => $orderDataProvider,
            ]);
        }
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewAjax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Clients::find()->where(['id' => $id])->asArray()->one();
        $model['car'] = ClientsAuto::find()->where(['client_id' => $id])->asArray()->one();

        return $model;
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionGetAutos($id){
        $models = ClientsAuto::find()->where(['client_id' => $id])->all();

        $output = '';

        foreach ($models as $model)
        {
            $output .= "<option value='{$model->number}'>{$model->number}</option>";
        }

        return $output;
    }

    /**
     * Creates a new Clients model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {
        $request = Yii::$app->request;
        $model = new Clients();
        $model->type = $type;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Клиенты",
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $reloadPjaxContainer
     * @return mixed
     */
    public function actionUpdate($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->cars = ArrayHelper::getColumn(ClientsAuto::find()->where(['client_id' => $model->id])->all(), 'id');
        $model->cars = ClientsAuto::find()->where(['client_id' => $model->id])->asArray()->all();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$reloadPjaxContainer,
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить".$id,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCommon($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update-common', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateBank($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, '#' => 'bank-tab-fill']);
        } else {
            return $this->render('update-bank', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $redirect
     * @return mixed
     */
    public function actionDelete($id, $redirect = null)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($redirect != null){
                return $this->redirect($redirect);
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);

        if(Yii::$app->user->identity->can('clients') == false){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

     /**
     * Delete multiple existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
