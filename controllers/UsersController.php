<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Orders;
use app\models\Available;
use app\models\Resource;
use app\models\Selling;
use app\models\Storage;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],*/
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

//    /**
//     * Lists all Users models.
//     * @return mixed
//     */
//    public function actionIndexMasters()
//    {
//        $searchModel = new UsersSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query(['role' => Users::USER_ROLE_MASTER]);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'ФИО : ' . $this->findModel($id)->name,
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пользователи",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{
                \Yii::warning($model->errors);
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param null $user_id
     * @return array|bool|null|UploadedFile
     * @throws \yii\base\Exception
     */
    public function actionUploadFile($user_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $fileName = Yii::$app->security->generateRandomString();
        if (is_dir('uploads') == false) {
            mkdir('uploads');
        }
        if (is_dir('uploads/data') == false) {
            mkdir('uploads/data');
        }
        $uploadPath = 'uploads/data';

        if (isset($_FILES['file'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;

            if ($file->saveAs($path)) {
                //Now save file data to database

                $file = (array)$file;
                $file['realPath'] = $path;

                return $file;
            }
        }

        return false;
    }


    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $reloadPjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$reloadPjaxContainer,
                   'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        Yii::$app->response->format = Response::FORMAT_JSON;
           
        if($request->isGet)
        { 
            return [
                'title'=> "Замена пароля пользователя",
                'size' => 'normal',
                'content'=> $this->renderAjax('change_password_form',[
                    'model' => $model,
                    'message' => 0,
                ]),
                'footer'=> Html::button('Нет',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Да',['class'=>'btn btn-primary','type'=>"submit"])
            ];                      
        }
        else
        { 
            if($model->load($request->post()) && $model->save())
            {
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Замена пароля пользователя",
                    'content'=>$this->renderAjax('change_password_form', [
                        'model' => $model,
                        'message' => 1,
                    ]),                            
                ];    
            }
            else
            {
                return [
                    'title'=> "Замена пароля пользователя",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdateCommon($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = Users::SCENARIO_COMMON;

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update-common', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdateCredentials($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = Users::SCENARIO_CREDENTIALS;

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update-credentials', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $redirect
     * @return mixed
     */
    public function actionDelete($id, $redirect = null)
    {
        $request = Yii::$app->request;

        $order = Orders::find()->where(['user_id'=> $id])->one();             
        $orderAcceptor = Orders::find()->where(['acceptor_id'=> $id])->one();
        $orderCreator = Orders::find()->where(['creator_id'=> $id])->one();
        if( $order != null || $orderAcceptor != null || $orderCreator != null) 
        {   
            if($request->isAjax){ 
            Yii::$app->response->format = Response::FORMAT_JSON;      
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот пользователь. Потому что он связань с заказами!!!</b></span></center>',
                ];
            }
        }

        $available = Available::find()->where(['creator_id'=> $id])->one();             
        if( $available != null) 
        {   
            if($request->isAjax){ 
            Yii::$app->response->format = Response::FORMAT_JSON;      
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот пользователь. Потому что он связань с складами!!!</b></span></center>',
                ];
            }
        }

        $resource = Resource::find()->where(['creator_id'=> $id])->one();             
        if( $resource != null) 
        {   
            if($request->isAjax){ 
            Yii::$app->response->format = Response::FORMAT_JSON;      
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот пользователь. Потому что он связань с ресурсами!!!</b></span></center>',
                ];
            }
        }

        $selling = Selling::find()->where(['creater_id'=> $id])->one();           
        $sellingChanged = Selling::find()->where(['changed_id'=> $id])->one();
        if( $selling != null || $sellingChanged != null) 
        {   
            if($request->isAjax){ 
            Yii::$app->response->format = Response::FORMAT_JSON;      
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот пользователь. Потому что он связань с продажами!!!</b></span></center>',
                ];
            }
        }

        $storage = Storage::find()->where(['user_id'=> $id])->one();             
        if( $storage != null) 
        {   
            if($request->isAjax){ 
            Yii::$app->response->format = Response::FORMAT_JSON;      
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот пользователь. Потому что он связань с складами!!!</b></span></center>',
                ];
            }
        }

        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($redirect != null){
                return $this->redirect($redirect);
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * @param int $id
     * @param string $attr
     * @param string $value
     * @return mixed
     */
    public function actionUpdateAttribute($id, $attr, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        if($attr == 'isMaster'){
            if($value == 1){
                $model->role_id = Users::USER_ROLE_MASTER;
            } else {
                $model->role_id = Users::USER_ROLE_ADMIN;
            }
        } else {
            $model->$attr = $value;
        }


        $result = $model->save(false);

        return ['result' => $result];
    }

     /**
     * Delete multiple existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            
            if($pk != 1){                
                $model = $this->findModel($pk);
                $model->delete();
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionMenuOpen()
    {
        $session = Yii::$app->session;
        $session['menu'] = 1;
    }

    public function actionMenuClose()
    {
        $session = Yii::$app->session;
        $session['menu'] = 0;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $user->last_activity_datetime = date('Y-m-d H:i:s');
        $user->save(false);

        if(Yii::$app->user->identity->can('users') == false){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
