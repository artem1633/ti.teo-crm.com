<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class CanvasSignatureAsset
 * @package app\assets
 */
class CanvasSignatureAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        '/js/canvas.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
