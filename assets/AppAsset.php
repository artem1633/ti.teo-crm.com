<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'app-assets/vendors/css/vendors.min.css',
        'app-assets/vendors/css/charts/apexcharts.css',
        'app-assets/vendors/css/extensions/tether-theme-arrows.css',
        'app-assets/vendors/css/extensions/tether.min.css',
        'app-assets/vendors/css/extensions/shepherd-theme-default.css',
        'app-assets/css/bootstrap.css',
        'app-assets/css/bootstrap-extended.css',
        'app-assets/css/colors.css',
        'app-assets/css/components.css',
        'app-assets/css/themes/dark-layout.css',
        'app-assets/css/themes/semi-dark-layout.css',
        'app-assets/css/core/menu/menu-types/vertical-menu.css',
        'app-assets/css/core/colors/palette-gradient.css',
        'app-assets/css/pages/dashboard-analytics.css',
        'app-assets/css/pages/card-analytics.css',
        'app-assets/css/plugins/tour/tour.css',
        'app-assets/css/pages/authentication.css',
        'assets/css/style.css',
        'libs/pickadate/lib/themes/default.css',
        'libs/pickadate/lib/themes/classic.date.css',

        'css/site.css',
    ];

    public $js = [
//        'app-assets/vendors/js/vendors.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
        'app-assets/vendors/js/ui/unison.min.js',
        'app-assets/vendors/js/extensions/i18nextXHRBackend.min.js',
        'app-assets/vendors/js/extensions/i18next.min.js',
        'app-assets/vendors/js/ui/jquery-sliding-menu.js',
        'app-assets/js/core/app-menu.min.js',
        'app-assets/js/core/app.min.js',
        'app-assets/js/scripts/components.min.js',
        'app-assets/js/scripts/customizer.min.js',
        'app-assets/js/scripts/footer.min.js',
        'app-assets/js/scripts/modal/components-modal.js',
        'app-assets/js/scripts/navs/navs.min.js',
        'app-assets/vendors/js/charts/apexcharts.min.js',
        'app-assets/js/scripts/charts/chart-apex.min.js',
        'libs/pickadate/lib/legacy.js',
        'libs/pickadate/lib/picker.js',
        'libs/pickadate/lib/picker.date.js',
        'libs/pickadate/lib/translations/ru_RU.js',
        'https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js',
        'libs/socket.io/socket.io.js',

        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
