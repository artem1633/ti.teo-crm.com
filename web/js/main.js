$(function(){
	$('#modalButton').click(function(){
		$('#modal').modal('show')
			.find('#modalContent')
			.load($(this).attr('value'));
	});

	$('.nav.nav-tabs .nav-link').click(function(){
		if($(this).hasClass('active') === false){
            $('.nav.nav-tabs .nav-link.active').removeClass('active');
            $(this).addClass('active');
        }
	});

    $('.nav-vertical .nav.nav-tabs .nav-link').click(function(){
    	alert(123);
        if($(this).hasClass('active') === false){
            $('.nav.nav-tabs .nav-link.active').removeClass('active');
            $(this).addClass('active');
        }
    });

    // if(window.localStorage.getItem('vue-expand-menu', '0') != 0 && window.localStorage.getItem('vue-expand-menu', '0') != null){
    //     $('body').addClass('menu-expanded');
    //     $('body').removeClass('menu-collapsed');
    //     $('.shepherd-modal-target i').addClass('icon-disc');
    //     $('.shepherd-modal-target i').removeClass('icon-circle');
    // }
    //
    $('[data-toggle="collapse"]').click(function(){
        if($(this).find('i').one().hasClass('icon-circle')){
            // window.localStorage.setItem('vue-expand-menu', '1');
            $.get('/users/menu-open', function(){  });
        } else {
            // window.localStorage.setItem('vue-expand-menu', '0');
            $.get('/users/menu-close', function(){  });
        }
    });

    $('#common-clear-btn').click(function(e){
        e.preventDefault();

        $('#act-fld').val('');

        // context.clearRect(0, 0, canvas.width, canvas.height);

        var canvas = document.getElementById("imageViewAct");
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        var canvas = document.getElementById("imageViewAct"),
            context = canvas.getContext("2d");

        var img = new Image();
        img.src = "/images/act-image-dist.png";
        img.onload = function() {
            context.drawImage(img, 0, 0);
        };

        // var canvas = document.getElementById("imageViewAdmin");
        // var ctx = canvas.getContext("2d");
        // ctx.clearRect(0, 0, canvas.width, canvas.height);
    });

    initTabletPicker();

    $('.multiple-input').on('afterAddRow', function(){
        initTabletPicker();
    });

    $('#ajaxCrudModal').on('shown.bs.modal', function() {
        setTimeout(function(){
            initTabletPicker();

            $('.multiple-input').on('afterAddRow', function(){
                initTabletPicker();
            });
        }, 500);
    });

});

function initTabletPicker()
{
    // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('[type="date"]').each(function(){
            $(this).data('value', $(this).val());
            $(this).attr('placeholder', 'Выберите дату');
        });

        $('[type="date"]').pickadate({
            format: 'dd.mm.yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });
    // }
}
