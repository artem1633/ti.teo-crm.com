/**
 * Created by Ilusha on 11.11.2020.
 */
/* © 2009 ROBO Design
 * http://www.robodesign.ro
 */

// Keep everything in anonymous function, called on window load.
if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context, canvaso, contexto;

        // The active tool instance.
        var tool;
        var tool_default = 'pencil';

        var disabled = $('#imageViewClient').data('disabled');

        function init () {
            // Find the canvas element.
            canvaso = document.getElementById('imageViewClient');

            if (!canvaso) {
                // alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvaso.getContext) {
                // alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext('2d');
            if (!contexto) {
                // alert('Error: failed to getContext!');
                return;
            }

            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement('canvas');
            if (!canvas) {
                // alert('Error: I cannot create a new canvas element!');
                return;
            }

            canvas.id     = 'imageTempClient';
            canvas.width  = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext('2d');

            // Get the tool select input.
            var tool_select = document.getElementById('dtool');
            if (!tool_select) {
                // alert('Error: failed to get the dtool element!');
                return;
            }
            tool_select.addEventListener('change', ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
                tool_select.value = tool_default;
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);

            // ТУТ
            // КОД
            var coords = $('.data-client-coords').html();
            if(coords != null){
                var lines = coords.split(';');

                $.each(lines, function(){
                    context.beginPath();

                    var coords = this.split(',');

                    $.each(coords, function(i){
                        var coord = this.split(':');
                        if(i === 0){
                            context.moveTo(coord[0], coord[1]);
                        } else {
                            context.lineTo(coord[0], coord[1]);
                        }

                        context.stroke();
                    });
                });
            }


            $('#clear-btn-client').click(function(e){
                e.preventDefault();

                $('#signature-client-fld').val('');

                // context.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewClient");
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            });
        }

        // The general-purpose event handler. This function just determines the mouse
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change (ev) {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This function draws the #imageTemp canvas on top of #imageView, after which
        // #imageTemp is cleared. This function is called each time when the user
        // completes a drawing operation.
        function img_update () {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.

        if(disabled != 1){
            tools.pencil = function () {
                var tool = this;
                this.started = false;

                // This is called when you start holding down the mouse button.
                // This starts the pencil drawing.
                this.mousedown = function (ev) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);

                    var old = $('#signature-client-fld').val();

                    $('#signature-client-fld').val(old+','+ev._x+':'+ev._y);

                    tool.started = true;
                };

                // This function is called every time you move the mouse. Obviously, it only
                // draws if the tool.started state is set to true (when you are holding down
                // the mouse button).
                this.mousemove = function (ev) {
                    if (tool.started) {
                        context.lineTo(ev._x, ev._y);
                        var old = $('#signature-client-fld').val();


                        if(ev.which === 1){
                            $('#signature-client-fld').val(old+','+ev._x+':'+ev._y);
                            context.stroke();
                        }

                    }
                };

                // This is called when you release the mouse button.
                this.mouseup = function (ev) {
                    if (tool.started) {
                        tool.mousemove(ev);
                        tool.started = false;
                        var old = $('#signature-client-fld').val();
                        $('#signature-client-fld').val(old+';');
                        context.stroke();
                        img_update();
                    }
                };
            };
        }

        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                    y = Math.min(ev._y,  tool.y0),
                    w = Math.abs(ev._x - tool.x0),
                    h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        // The line tool.
        tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;

            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.moveTo(tool.x0, tool.y0);
                context.lineTo(ev._x,   ev._y);

                // var old = $('#signature-fld').val();
                //
                // $('#signature-fld').val(old+','+tool.x0+':'+tool.y0);

                context.stroke();
                context.closePath();
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        init();

    }, false); }

if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context, canvaso, contexto;

        // The active tool instance.
        var tool;
        var tool_default = 'pencil';

        var disabled = $('#imageViewClientSecond').data('disabled');

        function init () {
            // Find the canvas element.
            canvaso = document.getElementById('imageViewClientSecond');

            if (!canvaso) {
                // alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvaso.getContext) {
                // alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext('2d');
            if (!contexto) {
                // alert('Error: failed to getContext!');
                return;
            }

            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement('canvas');
            if (!canvas) {
                // alert('Error: I cannot create a new canvas element!');
                return;
            }

            canvas.id     = 'imageTempClientSecond';
            canvas.width  = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext('2d');

            // Get the tool select input.
            var tool_select = document.getElementById('dtool');
            if (!tool_select) {
                // alert('Error: failed to get the dtool element!');
                return;
            }
            tool_select.addEventListener('change', ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
                tool_select.value = tool_default;
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            // canvas.addEventListener('mousedown', ev_canvas, false);
            // canvas.addEventListener('mousemove', ev_canvas, false);
            // canvas.addEventListener('mouseup',   ev_canvas, false);

            // ТУТ
            // КОД
            var coords = $('.data-client-second-coords').html();
            if(coords != null){
                var lines = coords.split(';');

                $.each(lines, function(){
                    context.beginPath();

                    var coords = this.split(',');

                    $.each(coords, function(i){
                        var coord = this.split(':');
                        if(i === 0){
                            context.moveTo(coord[0], coord[1]);
                        } else {
                            context.lineTo(coord[0], coord[1]);
                        }

                        context.stroke();
                    });
                });
            }


            $('#clear-btn-second-client').click(function(e){
                e.preventDefault();

                $('#signature-client-fld').val('');

                // context.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewClientSecond");
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            });
        }

        // The general-purpose event handler. This function just determines the mouse
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change (ev) {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This function draws the #imageTemp canvas on top of #imageView, after which
        // #imageTemp is cleared. This function is called each time when the user
        // completes a drawing operation.
        function img_update () {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.

        if(disabled != 1){
            tools.pencil = function () {
                var tool = this;
                this.started = false;

                // This is called when you start holding down the mouse button.
                // This starts the pencil drawing.
                this.mousedown = function (ev) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);

                    var old = $('#signature-client-second-fld').val();

                    $('#signature-client-second-fld').val(old+','+ev._x+':'+ev._y);

                    tool.started = true;
                };

                // This function is called every time you move the mouse. Obviously, it only
                // draws if the tool.started state is set to true (when you are holding down
                // the mouse button).
                this.mousemove = function (ev) {
                    if (tool.started) {
                        context.lineTo(ev._x, ev._y);
                        var old = $('#signature-client-second-fld').val();


                        if(ev.which === 1){
                            $('#signature-client-second-fld').val(old+','+ev._x+':'+ev._y);
                            context.stroke();
                        }

                    }
                };

                // This is called when you release the mouse button.
                this.mouseup = function (ev) {
                    if (tool.started) {
                        tool.mousemove(ev);
                        tool.started = false;
                        var old = $('#signature-client-second-fld').val();
                        $('#signature-client-second-fld').val(old+';');
                        context.stroke();
                        img_update();
                    }
                };
            };
        }

        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                    y = Math.min(ev._y,  tool.y0),
                    w = Math.abs(ev._x - tool.x0),
                    h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        // The line tool.
        tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;

            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.moveTo(tool.x0, tool.y0);
                context.lineTo(ev._x,   ev._y);

                // var old = $('#signature-fld').val();
                //
                // $('#signature-fld').val(old+','+tool.x0+':'+tool.y0);

                context.stroke();
                context.closePath();
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        init();

    }, false); }


if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context, canvaso, contexto;

        // The active tool instance.
        var tool;
        var tool_default = 'pencil';

        var disabled = $('#imageViewAdmin').data('disabled');

        function init () {
            // Find the canvas element.
            canvaso = document.getElementById('imageViewAdmin');
            if (!canvaso) {
                // alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvaso.getContext) {
                // alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext('2d');
            if (!contexto) {
                // alert('Error: failed to getContext!');
                return;
            }

            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement('canvas');
            if (!canvas) {
                // alert('Error: I cannot create a new canvas element!');
                return;
            }

            canvas.id     = 'imageTempAdmin';
            canvas.width  = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext('2d');

            // Get the tool select input.
            var tool_select = document.getElementById('dtool');
            if (!tool_select) {
                // alert('Error: failed to get the dtool element!');
                return;
            }
            tool_select.addEventListener('change', ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
                tool_select.value = tool_default;
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);

            // ТУТ
            // КОД
            var coords = $('.data-admin-coords').html();
            if(coords != null){
                var lines = coords.split(';');

                $.each(lines, function(){
                    context.beginPath();

                    var coords = this.split(',');

                    $.each(coords, function(i){
                        var coord = this.split(':');
                        if(i === 0){
                            context.moveTo(coord[0], coord[1]);
                        } else {
                            context.lineTo(coord[0], coord[1]);
                        }

                        context.stroke();
                    });
                });
            }

            $('#clear-btn-admin').click(function(e){
                e.preventDefault();

                $('#signature-admin-fld').val('');

                // context.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewAdmin");
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            });
        }

        // The general-purpose event handler. This function just determines the mouse
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change (ev) {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This function draws the #imageTemp canvas on top of #imageView, after which
        // #imageTemp is cleared. This function is called each time when the user
        // completes a drawing operation.
        function img_update () {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.
        if(disabled != 1){
            tools.pencil = function () {
                var tool = this;
                this.started = false;

                // This is called when you start holding down the mouse button.
                // This starts the pencil drawing.
                this.mousedown = function (ev) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);

                    var old = $('#signature-admin-fld').val();

                    $('#signature-admin-fld').val(old+','+ev._x+':'+ev._y);

                    tool.started = true;
                };

                // This function is called every time you move the mouse. Obviously, it only
                // draws if the tool.started state is set to true (when you are holding down
                // the mouse button).
                this.mousemove = function (ev) {
                    if (tool.started) {
                        context.lineTo(ev._x, ev._y);
                        var old = $('#signature-admin-fld').val();


                        if(ev.which === 1){
                            $('#signature-admin-fld').val(old+','+ev._x+':'+ev._y);
                            context.stroke();
                        }

                    }
                };

                // This is called when you release the mouse button.
                this.mouseup = function (ev) {
                    if (tool.started) {
                        tool.mousemove(ev);
                        tool.started = false;
                        var old = $('#signature-admin-fld').val();
                        $('#signature-admin-fld').val(old+';');
                        context.stroke();
                        img_update();
                    }
                };
            };
        }


        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                    y = Math.min(ev._y,  tool.y0),
                    w = Math.abs(ev._x - tool.x0),
                    h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        // The line tool.
        tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;

            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.moveTo(tool.x0, tool.y0);
                context.lineTo(ev._x,   ev._y);

                // var old = $('#signature-fld').val();
                //
                // $('#signature-fld').val(old+','+tool.x0+':'+tool.y0);

                context.stroke();
                context.closePath();
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        init();

    }, false); }

if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context, canvaso, contexto;

        // The active tool instance.
        var tool;
        var tool_default = 'pencil';

        var disabled = $('#imageViewAdminSecond').data('disabled');

        function init () {
            // Find the canvas element.
            canvaso = document.getElementById('imageViewAdminSecond');
            if (!canvaso) {
                // alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvaso.getContext) {
                // alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext('2d');
            if (!contexto) {
                // alert('Error: failed to getContext!');
                return;
            }

            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement('canvas');
            if (!canvas) {
                // alert('Error: I cannot create a new canvas element!');
                return;
            }

            canvas.id     = 'imageTempAdminSecond';
            canvas.width  = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext('2d');

            // Get the tool select input.
            var tool_select = document.getElementById('dtool');
            if (!tool_select) {
                // alert('Error: failed to get the dtool element!');
                return;
            }
            tool_select.addEventListener('change', ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
                tool_select.value = tool_default;
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);

            // ТУТ
            // КОД
            var coords = $('.data-admin-second-coords').html();
            if(coords != null){
                var lines = coords.split(';');

                $.each(lines, function(){
                    context.beginPath();

                    var coords = this.split(',');

                    $.each(coords, function(i){
                        var coord = this.split(':');
                        if(i === 0){
                            context.moveTo(coord[0], coord[1]);
                        } else {
                            context.lineTo(coord[0], coord[1]);
                        }

                        context.stroke();
                    });
                });
            }

            $('#clear-btn-second-admin').click(function(e){
                e.preventDefault();

                $('#signature-admin-fld').val('');

                // context.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewAdminSecond");
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            });
        }

        // The general-purpose event handler. This function just determines the mouse
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change (ev) {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This function draws the #imageTemp canvas on top of #imageView, after which
        // #imageTemp is cleared. This function is called each time when the user
        // completes a drawing operation.
        function img_update () {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.
        if(disabled != 1){
            tools.pencil = function () {
                var tool = this;
                this.started = false;

                // This is called when you start holding down the mouse button.
                // This starts the pencil drawing.
                this.mousedown = function (ev) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);

                    var old = $('#signature-admin-fld').val();

                    $('#signature-admin-fld').val(old+','+ev._x+':'+ev._y);

                    tool.started = true;
                };

                // This function is called every time you move the mouse. Obviously, it only
                // draws if the tool.started state is set to true (when you are holding down
                // the mouse button).
                this.mousemove = function (ev) {
                    if (tool.started) {
                        context.lineTo(ev._x, ev._y);
                        var old = $('#signature-admin-fld').val();


                        if(ev.which === 1){
                            $('#signature-admin-fld').val(old+','+ev._x+':'+ev._y);
                            context.stroke();
                        }

                    }
                };

                // This is called when you release the mouse button.
                this.mouseup = function (ev) {
                    if (tool.started) {
                        tool.mousemove(ev);
                        tool.started = false;
                        var old = $('#signature-admin-fld').val();
                        $('#signature-admin-fld').val(old+';');
                        context.stroke();
                        img_update();
                    }
                };
            };
        }


        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                    y = Math.min(ev._y,  tool.y0),
                    w = Math.abs(ev._x - tool.x0),
                    h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        // The line tool.
        tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;

            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.moveTo(tool.x0, tool.y0);
                context.lineTo(ev._x,   ev._y);

                // var old = $('#signature-fld').val();
                //
                // $('#signature-fld').val(old+','+tool.x0+':'+tool.y0);

                context.stroke();
                context.closePath();
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        init();

    }, false); }

if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context, canvaso, contexto;

        // The active tool instance.
        var tool;
        var tool_default = 'pencil';

        var disabled = $('#imageViewAct').data('disabled');

        function init () {
            // Find the canvas element.
            canvaso = document.getElementById('imageViewAct');
            if (!canvaso) {
                // alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvaso.getContext) {
                // alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext('2d');
            if (!contexto) {
                // alert('Error: failed to getContext!');
                return;
            }

            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement('canvas');
            if (!canvas) {
                // alert('Error: I cannot create a new canvas element!');
                return;
            }

            canvas.id     = 'imageTempAct';
            canvas.width  = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext('2d');

            context.strokeStyle = "red";

            // Get the tool select input.
            var tool_select = document.getElementById('dtool');
            if (!tool_select) {
                // alert('Error: failed to get the dtool element!');
                return;
            }
            tool_select.addEventListener('change', ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
                tool_select.value = tool_default;
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);

            // ТУТ
            // КОД
            var coords = $('.data-act-coords').html();
            if(coords != null){
                var lines = coords.split(';');

                $.each(lines, function(){
                    context.beginPath();

                    var coords = this.split(',');

                    $.each(coords, function(i){
                        var coord = this.split(':');
                        if(i === 0){
                            context.moveTo(coord[0], coord[1]);
                        } else {
                            context.lineTo(coord[0], coord[1]);
                        }

                        context.stroke();
                    });
                });
            }

            $('#clear-btn').click(function(e){
                e.preventDefault();

                $('#act-fld').val('');

                // context.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewAct");
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);

                var canvas = document.getElementById("imageViewAct"),
                    context = canvas.getContext("2d");

                var img = new Image();
                img.src = "/images/act-image-dist.png";
                img.onload = function() {
                    context.drawImage(img, 0, 0);
                };

                // var canvas = document.getElementById("imageViewAdmin");
                // var ctx = canvas.getContext("2d");
                // ctx.clearRect(0, 0, canvas.width, canvas.height);
            });
        }

        // The general-purpose event handler. This function just determines the mouse
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change (ev) {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This function draws the #imageTemp canvas on top of #imageView, after which
        // #imageTemp is cleared. This function is called each time when the user
        // completes a drawing operation.
        function img_update () {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.
        if(disabled != 1){
            tools.pencil = function () {
                var tool = this;
                this.started = false;

                // This is called when you start holding down the mouse button.
                // This starts the pencil drawing.
                this.mousedown = function (ev) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);

                    var old = $('#act-fld').val();

                    $('#act-fld').val(old+','+ev._x+':'+ev._y);

                    tool.started = true;
                };

                // This function is called every time you move the mouse. Obviously, it only
                // draws if the tool.started state is set to true (when you are holding down
                // the mouse button).
                this.mousemove = function (ev) {
                    if (tool.started) {
                        context.lineTo(ev._x, ev._y);
                        var old = $('#act-fld').val();


                        if(ev.which === 1){
                            $('#act-fld').val(old+','+ev._x+':'+ev._y);
                            context.stroke();
                        }

                    }
                };

                // This is called when you release the mouse button.
                this.mouseup = function (ev) {
                    if (tool.started) {
                        tool.mousemove(ev);
                        tool.started = false;
                        var old = $('#act-fld').val();
                        $('#act-fld').val(old+';');
                        context.stroke();
                        img_update();
                    }
                };
            };
        }


        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                    y = Math.min(ev._y,  tool.y0),
                    w = Math.abs(ev._x - tool.x0),
                    h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        // The line tool.
        tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;

            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                context.clearRect(0, 0, canvas.width, canvas.height);

                context.beginPath();
                context.moveTo(tool.x0, tool.y0);
                context.lineTo(ev._x,   ev._y);

                // var old = $('#signature-fld').val();
                //
                // $('#signature-fld').val(old+','+tool.x0+':'+tool.y0);

                context.stroke();
                context.closePath();
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                    img_update();
                }
            };
        };

        init();

    }, false); }


// vim:set spell spl=en fo=wan1croql tw=80 ts=2 sw=2 sts=2 sta et ai cin fenc=utf-8 ff=unix:
