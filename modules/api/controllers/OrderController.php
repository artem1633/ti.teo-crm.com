<?php

namespace app\modules\api\controllers;

use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Customer;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\CompanyTelegram;
use app\models\Settings;
use app\models\Company;
use app\models\User;
use app\models\Orders;
use app\models\JobList;
use app\models\manual\ListServices;

/**
 * Class OrderController
 * @package app\modules\api\controllers
 */
class OrderController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testpush','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     */
    public function actionDayUpdate()
    {
        $orders = Orders::find()->where(['status_id' => 1])->andWhere(['like', 'pre_date', date('Y-m-d')])->all();

        foreach ($orders as $order) {
            $order->status_id = 2;

            $jobs = JobList::find()->where(['order_id' => $order->id])->all();

            foreach ($jobs as $job)
            {
                $job->status_id = 2;
                $job->save(false);
            }

            $order->save(false, null, false);
        }
    }
}