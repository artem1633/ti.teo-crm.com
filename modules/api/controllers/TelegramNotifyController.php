<?php

namespace app\modules\api\controllers;

use app\helpers\ChatAPI;
use app\helpers\TelegramRequester;
use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Clients;
use app\models\Customer;
use app\models\manual\ListServiceProduct;
use app\models\manual\OrderStatus;
use app\models\Move;
use app\models\Resource;
use app\models\StatusMessage;
use app\models\Users;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\CompanyTelegram;
use app\models\Settings;
use app\models\Company;
use app\models\User;
use app\models\Orders;
use app\models\JobList;
use app\models\manual\ListServices;

/**
 * Class TelegramNotifyController
 * @package app\modules\api\controllers
 */
class TelegramNotifyController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testpush','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @var Customer $user
     * @var BotScenario $scenario
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {
        /** @var Bot $bot */

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        \Yii::warning($result, 'Telegram request');


        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $first = $result["message"]["chat"]["first_name"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $last = $result["message"]["from"]["last_name"]; //Юзернейм пользователя

        /** @var UsersList $user */
        $message_id = null;
        if ($result["callback_query"]) {
            $text = $result["callback_query"]['data'];
            $chat_id = $result['callback_query']['message']['chat']['id'];
            $message_id = $result['callback_query']['message']['message_id'];

            $this->setClear($chat_id, $message_id);
        }

        if(strpos($text, '_') == true){
            $textParts = explode('_', $text);
            $command = $textParts[0];
            $id = $textParts[count($textParts)-1];
            $order = JobList::findOne($id);
            $usr = User::find()->where(['vk_id' => $chat_id])->one();
            if($usr){
                $order->apiUserId = $usr->id;
            }
            if($order == false){
                self::sendTelMessage($chat_id, "Заказ не найден");
                return true;
            } else {
                if($command == 'job'){
                    $order->status_id = 3;
                    $order->save(false);
                    $result = $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => 'Услуга "'.$order->job->name.'" (Заказ #'.$order->order_id.') в работе. Как будете готовы, нажмите "Проверить"',
                        'parse_mode' => 'HTML',
                        'reply_markup' => json_encode([
                            'inline_keyboard' => [
                                [
                                    ['text' => 'Проверить', 'callback_data' => 'check_'.$order->id],
                                ],

                            ],
                        ])
                    ]);
                } else if($command == 'check'){
                    $order->status_id = 4;
                    $order->save(false);
                    $result = $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => 'Услуга "'.$order->job->name.'"  (Заказ #'.$order->order_id.') на проверке',
                        'parse_mode' => 'HTML',
                        // 'reply_markup' => json_encode([
                        //     'inline_keyboard' => [
                        //         [
                        //             ['text' => 'Выполнить', 'callback_data' => 'done_'.$order->id],
                        //         ],

                        //     ],
                        // ])
                    ]);
                } else if($command == 'done'){
                    $order->status_id = 5;
                    $order->save(false);
                    $result = $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => 'Услуга "'.$order->job->name.'" (Заказ #'.$order->order_id.') выполнена. Спасибо за работу!',
                        'parse_mode' => 'HTML',
                    ]);
                }



                $jobListsCount = JobList::find()->where(['order_id' => $order->order_id])->andWhere(['status_id' => 5])->count();

                $statusMessageId = null;

                if($jobListsCount == JobList::find()->where(['order_id' => $order->order_id])->count()){
//                    JobList::updateAll(['status_id' => 7], ['order_id' => $order->order_id]);

                    $jobsLists = JobList::find()->where(['order_id' => $order->order_id])->all();

                    foreach ($jobsLists as $jobsList)
                    {
                        $jobsList->status_id = 7;
                        $jobsList->save(false);
                    }

                    $statusMessageId = 7;

//                    $order->status_id = 5;
//                    $order->save(false);

                    /** @var Orders $model */
                    $model = Orders::findOne($order->order_id);
                    $model->status_id = 3;

                    $model->save(false, null, false);

                }

//                $client = new Client(new Version1X('http://localhost:1337'));
//
//                $client->initialize();
//                $client->emit('update', ['foo' => 'bar']);
//                $client->close();



                $orderModel = Orders::findOne($order->order_id);

                if($order->status_id == 3 && $orderModel){
                    $orderModel->status_id = 3;
                    $orderModel->save(false, null, false);
                }


                // Отправка WhatsApp сообшения
                /** @var StatusMessage $message */
                $message = StatusMessage::find()->where(['status_id' => $statusMessageId ? $statusMessageId : $order->status_id, 'is_active' => true])->one();
                \Yii::warning($message, 'Status Message');
                if($message)
                {
                    if($orderModel)
                    {
                        if($orderModel->telephone != null){
                            if($message->files == null){
                                ChatAPI::sendMessage($orderModel->telephone, $message->content);
                            } else {
                                $files = explode(',', $message->files);
                                if(count($files) > 0){
                                    $counter = 1;
                                    foreach ($files as $file)
                                    {
                                        if($file == '')
                                            continue;

                                        ChatAPI::sendFile($orderModel->telephone, $file, ($counter == 1 ? $message->content : null));
                                        $counter++;
                                    }
                                } else {
                                    ChatAPI::sendMessage($orderModel->telephone, $message->content);
                                }
                            }
                        }
                    }
                }

                $users = Users::find()->where(['accept_notifications' => true])->all();
                $status = OrderStatus::findOne($order->status_id);
                foreach ($users as $user){
                    if($user->vk_id != null){

                        $clientName = null;
                        $client = Clients::findOne($orderModel->client_id);

                        if($client){
                            $clientName = $client->fio;
                        }

                        $result = TelegramRequester::send('sendMessage', [
                            'chat_id' => $user->vk_id,
                            'text' => "Статус у заявки {$orderModel->id} (Клиент: {$clientName}, Номер: {$orderModel->number}, Марка и модель: ".ArrayHelper::getValue($orderModel, 'marks.name')." "..ArrayHelper::getValue($orderModel, 'spot.name').") изменен на «{$status->name}»",
                            'parse_mode' => 'HTML',
                        ]);
                    }
                }

            }

        }


        if ($text == '/start') {
            self::sendTelMessage($chat_id, "Для подключения уведомлений введите логин");
            return true;
        }

        $user = User::find()->where(['vk_id' => $chat_id])->one();

        if($user == false){
            $user = User::find()->where(['login' => $text])->one();

            if($user == false){
                self::sendTelMessage($chat_id, 'Пользователь не найден');
            } else {
                $user->vk_id = $chat_id;
                $user->save(false);
                self::sendTelMessage($chat_id, 'Вы успешно подключены. Ждите новостей');
            }

            return true;
        }

        if ($text == 'my') {

            $this->setClear($chat_id, $message_id);
            $this->getMenu($user, $chat_id, $user->info);
            return true;
        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        if ($message_id) {
            $this->getReq('editMessageReplyMarkup',[
                'chat_id'=> $chat_id,
                'message_id'=>$message_id,
                'text' => 'my text',
                'reply_markup' => null
            ]);
        }
        return true;
    }

    public function actionTest()
    {

        $jobLists = JobList::find()->where(['master_id' => 4])->all();

        $keyboard = [];

        foreach ($jobLists as $job) {
            $listService = ListServices::findOne($job->job_id);
            $keyboard[] = [['text' => 'Услуга «'.$listService->name.'» | ПОСТАВИТЬ В РАБОТУ', 'callback_data' => 'job_'.$job->id]];
        }


        $result = $this->getReq('sendMessage', [
            'chat_id' => '300640816',
            'text' => 'Вам назначены следующие услуги',
            'parse_mode' => 'HTML',
            'reply_markup' => json_encode([
                'inline_keyboard' => $keyboard,
            ])
        ]);

        // $result = $this->getReq('sendMessage', [
        //     'chat_id' => '300640816',
        //     'text' => 'Вам назначен заказ #1. Как будите готовы нажмите кнопку "В работе"',
        //     'parse_mode' => 'HTML',
        //     'reply_markup' => json_encode([
        //         'inline_keyboard' => [
        //             [
        //                 ['text' => 'В работе', 'callback_data' => 'job_1'],
        //             ],

        //         ],
        //     ])
        // ]);

        var_dump($result);
        exit;
    }


    public function getReq($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $url = "https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        return $result; //Или просто возращаем ответ в виде строки
    }

    public static function sendReq($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $url = "https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        return $result; //Или просто возращаем ответ в виде строки
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     * @return boolean
     */
    public static function sendTelMessage($userId, $text)
    {
        // $token = Settings::findByKey('telegram_token')->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

            }
        } else {
            $url = 'https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }

    public function actionSetWebhook()
    {

        $result = null;

        $url = 'https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/setWebhook?url=https://ti.teo-crm.com/api/telegram-notify/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
        //        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
        var_dump($result);
        $result = json_decode($result, true);
        // return $result;
    }

    public static function actionWebhookinfo()
    {
//        $token = '963981719:AAFD-R2r_Ui9MjW3ieZasBnKhfHYPrJ9IpI';//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        /** @var Bot $bot */

        $url = 'https://api.telegram.org/bot1480184951:AAF9KXFJ7ApReWbq3SSVATHCYIBoA3KJ6Yo/getWebhookInfo';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
//        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
        var_dump($result);
        $result = json_decode($result, true);

    }

}