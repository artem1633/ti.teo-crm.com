var app = require('express')();
const log = require('simple-node-logger').createSimpleLogger('server.log');
const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const $request = require('request');

// $request({
//     url: 'https://ads.asusmkd.ru/api/v1/add-incoming-call',
//     method: 'POST',
//     json: true,
//     body: {number: 'super'},
// }, function(error, response, data){
//     console.log(data);
// });

// $request.post(
//                 'https://ads.asusmkd.ru/api/v1/add-incoming-call',
//                 {phone: 'super'},
//                 function(error, response, data){
//                     console.log(data);
//                     if(clients[companyCallApiKey] != undefined){
//                         for(let i = 0; i < clients[companyCallApiKey].length; i++)
//                         {
//                             clients[companyCallApiKey][i].emit('message', body.from.number);
//                         }
//                     }
//                 }
//             );

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
    origin: true,
    credentials: true,
}));

// var https = require('https').createServer({
//     key: privateKey,
//     cert: certificate
// }, app);

var https = require('http').createServer(app);

// var privateKey = fs.readFileSync(__dirname+'/key.pem');
// var certificate = fs.readFileSync(__dirname+'/cert.pem');

var server = https.listen(3002, function(){
    log.info('Server started');
});

const io = require('socket.io').listen(server);


var clients = [];

// Сообщение для клиента
app.get('/update', function(req, res){
    console.log('request handled');

    io.sockets.emit('update');

    res.send('ok');
});

io.on('rate-update', function(data){
    console.log(data);
});

io.sockets.on('connection', function(socket){
    var data = socket.request;

    var userId = data._query['userId'];

    clients[userId] = socket;


    console.log('new client connected with ID '+userId);


    socket.on('rate-update', function(data){
        socket.broadcast.emit('update-rates');
    });
    // if(data._query['companyCallApiKey'] !== undefined)
    // {
    //     var apiKey = data._query['companyCallApiKey'].toString();
    //     console.log(apiKey);
    //     if(apiKey != '')
    //     {
    //         log.info('User company call api key: '+apiKey+'');

    //         if(clients[apiKey] !== undefined){
    //             clients[apiKey].push(socket);
    //         } else {
    //             clients[apiKey] = [];
    //             clients[apiKey].push(socket);
    //         }
    //         console.log(clients);
    //     }
    // }

});

io.sockets.on('update', function(socket){
    var data = socket.request;

    log.info('Current connections count is '+Object.keys(clients).length);
});