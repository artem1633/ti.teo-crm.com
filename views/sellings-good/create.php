<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SellingsGood */

?>
<div class="sellings-good-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
