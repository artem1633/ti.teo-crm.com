<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SellingsGood */
?>
<div class="sellings-good-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'selling_id',
            'product_id',
            'price',
            'count',
        ],
    ]) ?>

</div>
