<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\SellingsGood */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sellings-good-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'product_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getPro(),
                        'options' => ['placeholder' => 'Выберите'],  
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            "change" => "function() 
                            {
                                $.get('/orders/get-product-price',
                                {
                                    'id':$(this).val()
                                },
                                function(data)
                                {
                                    $('#price').val(data);
                                }
                                );
                            }",
                        ],
                    ]);
                ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput(['type' => 'number', 'id' => 'price']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
        </div>
    </div>  
    <div style="display: none;">
        <?= $form->field($model, 'selling_id')->textInput() ?>  
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
