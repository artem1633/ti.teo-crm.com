<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SellingsGood */
?>
<div class="sellings-good-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
