<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Resource */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'product_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getProductList(),
                        'options' => ['placeholder' => 'Выберите товара', 'disabled' => true], 
                        'pluginEvents' => [
                            "change" => "function() 
                            {
                                $.get('set-price',
                                {
                                    'id':$(this).val()
                                },
                                    function(data) { $('#price').val(data); }     
                                );
                                
                            }",
                        ], 
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]);
                ?>        
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status_id')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите статуса']) ?>
        </div>
        <div class="col-md-4">    
            <?= $form->field($model, 'good_id')->dropDownList($model->getGoodList(), ['prompt' => 'Выберите категорию']) ?>
        </div>    
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price_shop')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'available_id')->textInput() ?>
        <?= $form->field($model, 'creator_id')->textInput() ?>
        <?= $form->field($model, 'storage_id')->textInput() ?>        
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
