<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Resource */

?>
<div class="resource-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
