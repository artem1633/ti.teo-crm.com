<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StatusMessage */

?>
<div class="status-message-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
