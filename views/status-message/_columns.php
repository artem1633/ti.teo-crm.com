<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    'id',
    [
        'attribute' => 'is_active',
        'content' => function($model){
            if($model->is_active == true){
                return '<i class="fa fa-check text-success" style="font-size: 22px;"></i>';
            } else {
                return '<i class="fa fa-times text-danger" style="font-size: 22px;"></i>';
            }
        },
        'width' => '5%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    'name',
    'content',
    [
        'attribute' => 'status_id',
        'value' => 'status.name',
        'content' => function($model){
            $statusName = ArrayHelper::getValue($model, 'status.name');

            if($statusName == 'Новый'){
                $statusName = '<div class="chip chip-success">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'На записи'){
                $statusName = '<div class="chip chip-info">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'В работе'){
                $statusName = '<div class="chip chip-yellow">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'На проверке'){
                $statusName = '<div class="chip chip-warning">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'Выполнено'){
                $statusName = '<div class="chip chip-purple">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'На выдачи'){
                $statusName = '<div class="chip chip-primary">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'Отмененный'){
                $statusName = '<div class="chip chip-black">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            } else if($statusName == 'Закрытые'){
                $statusName = '<div class="chip chip-red">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$statusName.'</div>
                                  </div>
                                </div>';
            }

            return $statusName;
        }
    ],
    [
        'attribute' => 'files',
        'content' => function($model){
            if($model->files == null){
                return null;
            }

            $files = explode(',', $model->files);
            $fileLinks = [];

            $counter = 1;
            foreach($files as $file){
                if($file == '')
                    continue;

                $fileLinks[] = Html::a("Файл {$counter}", Url::base(true).'/'.$file, ['download' => true, 'data-pjax' => 0]);
                $counter++;
            }

            $output = implode(', ', $fileLinks);

            return $output;
        }
    ],

    // 'is_active',
    // 'created_at',

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update} {delete}',
        'buttons' => [
            'view' => function($url, $model){
                return Html::a('<i class="feather icon-eye"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                ]);
            },
            'update' => function($url, $model){
                return Html::a('<i class="feather icon-edit"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;',
                ]);
            },
            'delete' => function($url, $model){
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    'style' => 'font-size: 23px;',
                ]);
            },
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip', 'style' => 'display: none;'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'],
    ],
];