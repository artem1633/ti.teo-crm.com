<?php

use app\models\manual\OrderStatus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StatusMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="status-message-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(OrderStatus::find()->orderBy('sort asc')->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите статус'],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="hidden">
                <?= $form->field($model, 'uploadFiles[]')->fileInput(['multiple' => true]) ?>
            </div>
            <?= Html::a('<i class="feather icon-upload"></i> Загрузить файлы', '#', ['id' => 'file-btn', 'class' => 'btn btn-primary', 'onclick' => '
                            event.preventDefault();
                            
                            $("#statusmessage-uploadfiles").trigger("click");
                        ']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_active')->checkbox() ?>
        </div>
    </div>




    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS

$("#statusmessage-uploadfiles").change(function(){
    $("#file-btn").attr('disabled', true);
    $("#file-btn").attr('class', 'btn btn-outline-primary');
    $("#file-btn").text('Файлы загружены');
    $("#file-btn").attr('onclick', '');
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>