<?php

use yii\helpers\Url;

$dashboardActive = Yii::$app->controller->id == 'dashboard' ? ' active' : '';
$orderActive = Yii::$app->controller->id == 'orders' ? ' active' : '';
$clientsActive = Yii::$app->controller->id == 'clients' ? ' open' : '';
$usersActive = Yii::$app->controller->id == 'users' ? ' active' : '';
$atelierActive = Yii::$app->controller->id == 'atelier' ? ' active' : '';
$articlesActive = Yii::$app->controller->id == 'articles' ? ' active' : '';
$clientsGroupActive = Yii::$app->controller->id == 'clients-group' ? ' active' : '';
$advertisingActive = Yii::$app->controller->id == 'advertising' ? ' active' : '';
$productStatusActive = Yii::$app->controller->id == 'product-status' ? ' active' : '';
$orderStatusActive = Yii::$app->controller->id == 'order-status' ? ' active' : '';
$claimStatusesActive = Yii::$app->controller->id == 'claim-statuses' ? ' active' : '';
$usersActive = Yii::$app->controller->id == 'users' ? ' active' : '';
$ordersColorActive = Yii::$app->controller->id == 'orders-color' ? ' active' : '';
$typeClothesActive = Yii::$app->controller->id == 'type-clothes' ? ' active' : '';
$quickOrderActive = Yii::$app->controller->id == 'quick-order' ? ' active' : '';
$typePollutionActive = Yii::$app->controller->id == 'type-pollution' ? ' active' : '';
$spotActive = Yii::$app->controller->id == 'spot' ? ' active' : '';
$markingActive = Yii::$app->controller->id == 'marking' ? ' active' : '';
$availableActive = Yii::$app->controller->id == 'available' ? ' active' : '';
$cashboxActive = Yii::$app->controller->id == 'cashbox' ? ' active' : '';
$suppliersActive = Yii::$app->controller->id == 'suppliers' ? ' active' : '';
$listServicesActive = Yii::$app->controller->id == 'list-services' ? ' active' : '';
$productActive = Yii::$app->controller->id == 'product' ? ' active' : '';
$aboutCompanyActive = Yii::$app->controller->id == 'about-company' ? ' active' : '';
$sellingActive = Yii::$app->controller->id == 'selling' ? ' active' : '';
$reportActive = Yii::$app->controller->id == 'report' ? ' active' : '';
$groupActive = Yii::$app->controller->id == 'group' ? ' active' : '';
$manufacturerActive = Yii::$app->controller->id == 'manufacturer' ? ' active' : '';
$clientsPersonActive = (Yii::$app->controller->id == 'clients' && (Yii::$app->controller->action->id == 'index' || Yii::$app->controller->action->id == 'view')) ? ' active' : '';
$clientsCompanyActive = (Yii::$app->controller->id == 'clients' && (Yii::$app->controller->action->id == 'index-company' || Yii::$app->controller->action->id == 'view-company')) ? ' active' : '';
$statusMessageActive = Yii::$app->controller->id == 'status-message' ? ' active' : '';


$bookOpen = '';

if(in_array(Yii::$app->controller->id, ['manufacturer', 'group', 'suppliers', 'list-services', 'product', 'type-clothes', 'marking', 'spot', 'status-message'])){
    $bookOpen = ' open';
}


?>

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="<?= Url::toRoute(['dashboard/index']) ?>">
                    <div class="brand-logo" style="background: unset; margin-top: -17px; color: #000; font-family: 'Montserrat';">TI</div>
                    <h2 class="brand-text mb-0" style="color: #000;">Detailing</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0 shepherd-modal-target" data-toggle="collapse"><i class="icon-x d-block d-xl-none font-medium-4 primary toggle-icon feather icon-disc"></i><i class="toggle-icon font-medium-4 d-none d-xl-block collapse-toggle-icon primary feather icon-disc" data-ticon="icon-disc" tabindex="0"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content ps ps--active-y" style="height: 874px !important; overflow: auto !important;;">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="navigation-header"><span>Меню</span>
            </li>
            <?php if(Yii::$app->user->identity->can('dashboard')): ?>
            <li class="nav-item<?=$dashboardActive?>">
                <a href="<?=Url::toRoute(['/dashboard'])?>"><i class="feather icon-activity"></i><span class="menu-title">Dashboard</span></a>
            </li>
            <?php endif; ?>
            <li class="nav-item<?=$atelierActive?>">
                <a href="<?= Url::toRoute(['/atelier']) ?>"><i class="feather icon-map-pin"></i><span class="menu-title">Филиалы</span></a>
            </li>
            <?php if(Yii::$app->user->identity->can('orders')): ?>
            <li class="nav-item<?=$orderActive?>">
                <a href="<?= Url::toRoute(['/orders']) ?>"><i class="feather icon-crosshair"></i><span class="menu-title">Заявки</span></a>
            </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->can('clients')): ?>
            <li class="nav-item<?=$clientsActive?> has-sub"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title">Клиенты</span></a>
                <ul class="menu-content" style="">
                    <li class="is-shown<?=$clientsPersonActive?>"><a href="<?= Url::toRoute(['/clients/index']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Физические лица</span></a>
                    </li>
                    <li class="is-shown<?=$clientsCompanyActive?>"><a href="<?= Url::toRoute(['/clients/index-company']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Юридические лица</span></a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->can('reports')): ?>
            <li class="nav-item<?=$reportActive?>">
                <a href="<?=Url::toRoute(['/report'])?>"><i class="feather icon-calendar"></i><span class="menu-title">Отчеты</span></a>
            </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->can('users')): ?>
            <li class="nav-item<?=$usersActive?>">
                <a href="<?= Url::toRoute(['/users']) ?>"><i class="feather icon-users"></i><span class="menu-title">Пользователи</span></a>
            </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->can('storage')): ?>
                <li class="nav-item<?=$availableActive?>">
                    <a href="<?= Url::toRoute(['/available']) ?>"><i class="feather icon-package"></i><span class="menu-title">Склад</span></a>
                </li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->can('book')): ?>
                <li class="nav-item has-sub<?=$bookOpen?>"><a href="#"><i class="feather icon-zap"></i><span class="menu-title">Справочники</span></a>
                    <ul class="menu-content" style="">
                        <!--                    ['label' => 'Статьи расходов/доходов', 'icon' => 'file-text-o', 'url' => ['/articles'],],-->
                        <!--                    ['label' => 'Группы клиентов', 'icon' => 'file-text-o', 'url' => ['/clients-group'],],-->
                        <!--                    ['label' => 'Реклама', 'icon' => 'file-text-o', 'url' => ['/advertising'],],-->
                        <!--                    ['label' => 'Статус товаров', 'icon' => 'file-text-o', 'url' => ['/product-status'],],-->
                        <!--                    ['label' => 'Статусы заказов', 'icon' => 'file-text-o', 'url' => ['/order-status'],],-->
                        <!--                    ['label' => 'Статусы заявок', 'icon' => 'file-text-o', 'url' => ['/claim-statuses'],],-->
                        <!--                    ['label' => 'Цветовая маркировка', 'icon' => 'file-text-o', 'url' => ['/orders-color'],],-->
                        <!--                    ['label' => 'Тип одежды', 'icon' => 'file-text-o', 'url' => ['/type-clothes'],],-->
                        <!--                    ['label' => 'Срочный заказ', 'icon' => 'file-text-o', 'url' => ['/quick-order'],],-->
                        <!--                    ['label' => 'Cтепень и тип загрязнения', 'icon' => 'file-text-o', 'url' => ['/type-pollution'],],-->
                        <!--                    ['label' => 'Наличие пятен', 'icon' => 'file-text-o', 'url' => ['/spot'],],-->
                        <!--                    ['label' => 'Маркировка', 'icon' => 'file-text-o', 'url' => ['/marking'],],-->
                        <li class="is-shown<?=$groupActive?>"><a href="<?= Url::toRoute(['/group']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Группы пользователей</span></a>
                        </li>
                        <li class="is-shown<?=$listServicesActive?>"><a href="<?= Url::toRoute(['/list-services']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Услуги</span></a>
                        </li>
                        <li class="is-shown<?=$productActive?>"><a href="<?= Url::toRoute(['/product']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Расходники</span></a>
                        </li>
                        <li class="is-shown<?=$atelierActive?> hidden"><a href="<?= Url::toRoute(['/atelier']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Магазины</span></a>
                        </li>
                        <li class="is-shown<?=$suppliersActive?>"><a href="<?= Url::toRoute(['/suppliers']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Поставщики</span></a>
                        </li>
                        <li class="is-shown<?=$manufacturerActive?>"><a href="<?= Url::toRoute(['/manufacturer']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Производители</span></a>
                        </li>
                        <li class="is-shown<?=$typeClothesActive?>"><a href="<?= Url::toRoute(['/type-clothes']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Типы транспорта</span></a>
                        </li>
                        <li class="is-shown<?=$markingActive?>"><a href="<?= Url::toRoute(['/marking']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Марки</span></a>
                        </li>
                        <li class="is-shown<?=$spotActive?>"><a href="<?= Url::toRoute(['/spot']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Модели</span></a>
                        </li>
                        <li class="is-shown<?=$aboutCompanyActive?> hidden"><a href="<?= Url::toRoute(['/about-company/view', 'company_id' => Yii::$app->user->identity->company_id]) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="2 columns">Юридические лица</span></a>
                        </li>
                        <li class="is-shown<?=$clientsGroupActive?> hidden"><a href="<?= Url::toRoute(['/clients-group']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed navbar">Группы клиентов</span></a>
                        </li>
                        <li class="is-shown<?=$advertisingActive?> hidden"><a href="<?= Url::toRoute(['/advertising']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Floating navbar">Реклама</span></a>
                        </li>
                        <li class="is-shown<?=$productStatusActive?> hidden"><a href="<?= Url::toRoute(['/product-status']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы Расходников</span></a>
                        </li>
                        <li class="is-shown<?=$orderStatusActive?> hidden"><a href="<?= Url::toRoute(['/order-status']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы заказов</span></a>
                        </li>
                        <li class="is-shown<?=$claimStatusesActive?> hidden"><a href="<?= Url::toRoute(['/claim-statuses']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Статусы заявок</span></a>
                        </li>
                        <li class="is-shown<?=$ordersColorActive?> hidden"><a href="<?= Url::toRoute(['/orders-color']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Цветовая маркировка</span></a>
                        </li>
                        <li class="is-shown<?=$quickOrderActive?> hidden"><a href="<?= Url::toRoute(['/quick-order']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Срочный заказ</span></a>
                        </li>
                        <li class="is-shown<?=$typePollutionActive?> hidden"><a href="<?= Url::toRoute(['/type-pollution']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Cтепень и тип загрязнения</span></a>
                        </li>
                        <li class="is-shown<?=$statusMessageActive?>"><a href="<?= Url::toRoute(['/status-message']) ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Fixed layout">Сообщения статусов</span></a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
        <!--        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 853px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 282px;"></div></div></div>-->
    </div>
</div>