<?php
use app\models\JobList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Soglosovaniya;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Orders;

\Yii::$app->db->createCommand()->update('users', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->id ])->execute(); 
\Yii::$app->db->createCommand()->update('companies', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->company_id ])->execute();

$preOrderCount = Orders::find()->where(['status_id' => 1])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$newOrderCount = Orders::find()->where(['status_id' => 2])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$workingOrderCount = Orders::find()->where(['status_id' => 3])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$checkOrderCount = Orders::find()->where(['status_id' => 4])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$doneOrderCount = Orders::find()->where(['status_id' => 8])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$cancelOrderCount = Orders::find()->where(['status_id' => 6])->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();
$allOrderCount = Orders::find()->andFilterWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->count();

?>

    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav d-xl-none">
                            <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu ficon"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                                <li class="nav-item d-none d-lg-block">
                                    <div class="d-flex justify-content-start flex-wrap">
                                        <a href="<?= Url::toRoute(['orders/index', 'OrdersSearch[status_id]' => 1]) ?>" style="padding: 0 8px;" class="text-center bg-info colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$preOrderCount?> На записи</span>
                                        </a>
                                        <a href="<?= Url::toRoute(['orders/index', 'OrdersSearch[status_id]' => 2]) ?>" style="padding: 0 8px;" class="text-center bg-success colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$newOrderCount?> Новые</span>
                                        </a>
                                        <a href="<?= Url::toRoute(['orders/index', 'OrdersSearch[status_id]' => 3]) ?>" style="padding: 0 8px;" class="text-center bg-yellow colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$workingOrderCount?> В работе</span>
                                        </a>
                                        <a href="<?= Url::toRoute(['orders/index', 'OrdersSearch[status_id]' => 8]) ?>" style="padding: 0 8px;" class="text-center bg-red colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$doneOrderCount?> Закрытые</span>
                                        </a>
                                        <a href="<?= Url::toRoute(['orders/index', 'OrdersSearch[status_id]' => 6]) ?>" style="padding: 0 8px;" class="text-center bg-black colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$cancelOrderCount?> Отмененные</span>
                                        </a>
                                        <a href="<?= Url::toRoute(['orders/index']) ?>" style="padding: 0 8px;" class="text-center bg-secondary colors-container rounded text-white height-50 d-flex align-items-center justify-content-center mr-1 ml-50 my-1 shadow">
                                            <span class="align-middle"><?=$allOrderCount?> Все</span>
                                        </a>
                                    </div>
                                    <!--   option Chat-->
                                    <!--   option email-->
                                    <!--   option todo-->
                                    <!--   option Calendar-->
                                </li>
                                <?php if(Yii::$app->user->identity->can('orders')): ?>
                                    <li class="nav-item d-none d-lg-block">
                                        <?=Html::a('<i class="feather icon-plus" style="font-size: 27px;"></i>', ['orders/create'],
                                            ['title'=> 'Создать','class'=>'btn btn-primary btn-icon rounded-circle', 'data-pjax' => 0, 'style' => 'margin-top: 15px; margin-left: 20px; font-size: 27px;'])?>
                                    </li>
                                <?php endif; ?>
                        </ul>
                    </div>
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-notification nav-item" style="display: none;">
                            <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-bell"></i>
<!--                                <span class="badge badge-pill badge-primary badge-up">5</span>-->
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header" style="display: none;">
                                    <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">5 New</h3><span class="notification-title">App Notifications</span>
                                    </div>
                                </li>
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">Уведомления</h3>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list ps">
                                        <?php

                                        $jobList = JobList::find()
                                            ->where(['master_id' => Yii::$app->user->getId()])
                                            ->andWhere(['or', ['status_id' => [1, 2, 3, 4]], ['is', 'status_id', null]])
                                            ->andWhere(['is not', 'date', null])
                                            ->andWhere(['>', 'date', date('Y-m-d')])
                                            ->all();

                                        $jobList = array_filter($jobList, function(&$job){
                                            $date = strtotime($job->date);

                                            if(($date - time()) < 172800){
                                                return true;
                                            }
                                            return false;
                                        });

                                        ?>
                                        <?php foreach ($jobList as $job): ?>
                                    <a class="d-flex justify-content-between" href="javascript:void(0)">
                                            <div class="media d-flex align-items-start">
                                                <div class="media-left"><i class="feather icon-crosshair font-medium-5 primary"></i></div>
                                                <div class="media-body">
                                                    <h6 class="primary media-heading">Услуга «<?=$job->job->name?>»</h6><small class="notification-text"> <?= ArrayHelper::getValue($job, 'order.client.fio') ?>, <?= ArrayHelper::getValue($job, 'order.marks.name') ?> <?= ArrayHelper::getValue($job, 'order.spot.name') ?></small>
                                                </div><small>
                                                    <time class="media-meta" datetime="2015-06-11T18:29:20+08:00"><?=Yii::$app->formatter->asDate($job->date, 'php:d.m.Y')?></time></small>
                                            </div>
                                    </a>

                                        <?php endforeach; ?>
                                    <a class="d-flex justify-content-between" href="javascript:void(0)">


                                    </a><div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="javascript:void(0)">Показать все</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600"><?= Yii::$app->user->identity->name ?></span><span class="user-status">Администратор</span></div>

<!--                                <span><img class="round" src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></span>-->

                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="<?= Url::toRoute(['site/logout']) ?>" data-method="POST"><i class="feather icon-power"></i> Выйти</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
