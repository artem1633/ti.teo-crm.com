<aside class="main-sidebar">

    <section class="sidebar">

        <?php 
        if(isset(Yii::$app->user->identity->id))
            { $role = Yii::$app->user->identity->role_id;
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            //['label' => 'Показатели', 'icon' => 'desktop', 'url' => ['#']],
                            ['label' => 'Заказы', 'icon' => 'archive', 'url' => ['/orders']],
                            //['label' => 'Химчистка', 'icon' => 'gear', 'url' => ['#']],
                            //['label' => 'Продажа', 'icon' => 'shopping-cart', 'url' => ['#']],
                            //['label' => 'График работы персонала', 'icon' => 'bar-chart-o', 'url' => ['#']],
                            //['label' => 'Заявка', 'icon' => 'file-text-o', 'url' => ['#']],
                            //['label' => 'Касса', 'icon' => 'money', 'url' => ['#']],
                            //['label' => 'Отчёт', 'icon' => 'area-chart', 'url' => ['#']],
                            [
                                'label' => 'Справочники',
                                'icon' => 'book',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Статьи расходов/доходов', 'icon' => 'file-text-o', 'url' => ['/articles'],],
                                    ['label' => 'Группы клиентов', 'icon' => 'file-text-o', 'url' => ['/clients-group'],],
                                    ['label' => 'Реклама', 'icon' => 'file-text-o', 'url' => ['/advertising'],],
                                    ['label' => 'Статус товаров', 'icon' => 'file-text-o', 'url' => ['/product-status'],],
                                    ['label' => 'Статусы заказов', 'icon' => 'file-text-o', 'url' => ['/order-status'],],
                                    ['label' => 'Статусы заявок', 'icon' => 'file-text-o', 'url' => ['/claim-statuses'],],
                                    ['label' => 'Цветовая маркировка', 'icon' => 'file-text-o', 'url' => ['/orders-color'],],
                                    ['label' => 'Тип одежды', 'icon' => 'file-text-o', 'url' => ['/type-clothes'],],
                                    ['label' => 'Срочный заказ', 'icon' => 'file-text-o', 'url' => ['/quick-order'],],
                                    ['label' => 'Cтепень и тип загрязнения', 'icon' => 'file-text-o', 'url' => ['/type-pollution'],],
                                    ['label' => 'Наличие пятен', 'icon' => 'file-text-o', 'url' => ['/spot'],],
                                    ['label' => 'Маркировка', 'icon' => 'file-text-o', 'url' => ['/marking'],],
                                ],
                            ],
                            [
                                'label' => 'Настройки',
                                'icon' => 'wrench',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'],],
                                    ['label' => 'Клиенты', 'icon' => 'user', 'url' => ['/clients'],],
                                    ['label' => 'Услугы', 'icon' => 'fire', 'url' => ['/list-services'],],
                                    ['label' => 'Касса', 'icon' => 'chain', 'url' => ['/cashbox'],],
                                    ['label' => 'Юридические лица', 'icon' => 'file-text-o', 'url' => ['/about-company/view', 'id' => 1],],
                                ],
                            ],
                            //['label' => 'Инструкция', 'icon' => 'book', 'url' => ['#']],
                        ],
                    ]
                );
            } 
            else{
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Signup', 'icon' => 'sign-in', 'url' => ['/signup']],                            
                        ],
                    ]
                );
            }
        ?>

    </section>

</aside>
