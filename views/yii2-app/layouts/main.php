<?php
use yii\helpers\Html;
use app\models\Companies;

/* @var $this \yii\web\View */
/* @var $content string */

if(isset(Yii::$app->user->identity))$company = Companies::findOne(Yii::$app->user->identity->getCompany());
$left_menu = 'left.php';
if($company != null)
{
    if($company->access_end_datetime === null || (new \DateTime($company->access_end_datetime))->getTimestamp() > time())
    {
        $left_menu = 'left.php';
    }
    else
    {
        if($company->isSuperCompany() === false)
        {
            $left_menu = 'left-second.php';
        }
    }
}


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

//    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php
        $session = Yii::$app->session;
        if(isset($session['menu'])){
            if($session['menu'] == 1){
                $bodyClass = " menu-expanded";
                $iconClass = " icon-circle";
            } else {
                $bodyClass = " menu-collapsed";
                $iconClass = " icon-disc";
            }
        } else {
            $bodyClass = " menu-collapsed";
            $iconClass = " icon-disc";
        }
    ?>
    <body class="vertical-layout vertical-menu-modern 2-columns navbar-floating footer-static pace-done<?= $bodyClass ?>" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <?php $this->beginBody() ?>
    <div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div></div>
    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>
    <?= $this->render(
        $left_menu,
        ['directoryAsset' => $directoryAsset]
    )
    ?>


    <div class="app-content content">

        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>

        <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>

    </div>

    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">2021 TI Detailing. Все права защищены</span><span class="float-md-right d-none d-md-block"><a
                        href="http://dataplus.kz" target="_blank">Made with enjoy by DATA Plus</a></span>
            <button class="btn btn-primary btn-icon scroll-top waves-effect waves-light" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
