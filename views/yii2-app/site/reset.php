<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Войти';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<section class="row flexbox-container">
    <div class="col-xl-8 col-11 d-flex justify-content-center">
        <div class="card bg-authentication rounded-0 mb-0" style="width: 1150px;">
            <div class="row m-0">
                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                    <img src="../../../app-assets/images/pages/login.png" alt="branding logo">
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2" style="height: 100%;">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Восстановить свой пароль</h4>
                            </div>
                        </div>
                        <p class="px-2">Введите свой логин, и мы вышлем Вам на Telegram сообщение с паролем.</p>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

                                <?= $form
                                    ->field($model, 'login', $fieldOptions1)
                                    ->label(false)
                                    ->textInput(['placeholder' => $model->getAttributeLabel('login')]) ?>

                                <a href="<?= Url::toRoute(['site/login']) ?>" class="btn btn-primary float-left btn-inline waves-effect waves-light">Назад</a>
                                <button type="submit" class="btn btn-primary float-right btn-inline waves-effect waves-light">Восстановить пароль</button>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                        <div class="login-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
