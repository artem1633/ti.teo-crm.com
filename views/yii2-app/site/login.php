<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Войти';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<section class="row flexbox-container">
    <div class="col-xl-8 col-11 d-flex justify-content-center">
        <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0" style="width: 400px; height: 100%; background: #000;">
                    <h1 style="color: #fff; font-weight: 700; margin-top: 40%; font-size: 40px;">TI Detailing</h1>
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Вход</h4>
                            </div>
                        </div>
                        <p class="px-2">Пожалуйста, введите ваш логин и пароль</p>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
                                    <?= $form
                                        ->field($model, 'username', $fieldOptions1)
                                        ->label(false)
                                        ->textInput(['placeholder' => 'Логин']) ?>
                                    <?= $form
                                        ->field($model, 'password', $fieldOptions2)
                                        ->label(false)
                                        ->passwordInput(['placeholder' => 'Пароль']) ?>

                                    <div class="form-group d-flex justify-content-between align-items-center">
                                        <div class="text-left">
                                            <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить меня') ?>
                                        </div>
                                        <div class="text-right"><a href="<?= Url::toRoute(['site/reset']) ?>" style="padding-bottom: 20px; display: inline-block;" class="card-link">Забыли пароль?</a></div>
                                    </div>
                                    <button type="submit" class="btn btn-primary float-right btn-inline waves-effect waves-light">Войти</button>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                        <div class="login-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="login-box hidden">
    <div class="login-logo">
        <a href="#"><b>TEO-SKLAD</b></a>
    </div>
    <?php if(Yii::$app->session->hasFlash('register_success')): ?>
        <p>
            <div class="alert alert-success show m-b-0">
                <span class="close" data-dismiss="alert">×</span>
                <strong>Успех!</strong>
                <?=Yii::$app->session->getFlash('register_success')?>
            </div>
        </p>
    <?php endif; ?>
    <div class="login-box-body">
        <p class="login-box-msg">Введите данные авторизации</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
            </div>
            <div class="col-xs-12">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block btn-flat',  'style' => 'width:100%', 'name' => 'login-button']) ?>
            </div>
        </div>
        <div class="row">
            <br>
            <div class="col-xs-6">
                <?= Html::a('Востановить пароль', $url = '/site/reset', ['option' => 'value']); ?>
            </div>
            <div class="col-xs-6">
                <?= Html::a('Зарегистрироваться', $url = '/site/register', ['option' => 'value']); ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

        <!-- <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                in using Google+</a>
        </div> -->
        <!-- /.social-auth-links -->

        <!-- <a href="#">Забыл пароль</a><br> -->
        <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
