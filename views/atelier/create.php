<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Atelier */

?>
<div class="atelier-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
