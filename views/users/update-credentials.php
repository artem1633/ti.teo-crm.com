<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'new_password')->passwordInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'repeatPassword')->passwordInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'vk_id')->textInput() ?>
                    </div>
                </div>
                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
