<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<style>
.modal .modal-body {
    width: 1000px;
}


@media(max-width:1300px) {
    .modal .modal-body {
        width: 800px;
    }
}
</style>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Пользователи</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'panelBeforeTemplate' => Html::a('Создать', ['create'],
                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary', 'data-pjax' => 0]).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],

                ]
            ])?>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="ajaxCrudModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document" style="padding-right: 49%;">
        <div class="modal-content" style="width: 99969px;">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Basic Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Check First Paragraph</h5>
                <p>Oat cake ice cream candy chocolate cake chocolate cake cotton candy dragée apple pie.
                    Brownie carrot cake candy canes bonbon fruitcake topping halvah. Cake sweet roll cake
                    cheesecake cookie chocolate cake liquorice.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Accept</button>
            </div>
        </div>
    </div>
</div>
