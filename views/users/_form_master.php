<?php
use app\models\Group;
use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

if($model->role_id == Users::USER_ROLE_MASTER){
    $model->isMaster = true;
}

//var_dump(array_keys($model->errors));

$tabPaneFirst = " active";
$tabPaneSecond = "";

if(in_array('login', array_keys($model->errors)) || in_array('new_password', array_keys($model->errors)) || in_array('repeatPassword', array_keys($model->errors))){
    $tabPaneFirst = "";
    $tabPaneSecond = " active";
}


?>

    <div class="users-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="nav-vertical">
            <ul class="nav nav-tabs nav-left flex-column" role="tablist" style="height: 56px;">
                <li class="nav-item">
                    <a class="nav-link<?=$tabPaneFirst?>" id="baseVerticalLeft-tab1" data-toggle="tab" aria-controls="tabVerticalLeft1" href="#tabVerticalLeft1" role="tab" aria-selected="false">Основная информация</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?=$tabPaneSecond?>" id="baseVerticalLeft-tab2" data-toggle="tab" aria-controls="tabVerticalLeft2" href="#tabVerticalLeft2" role="tab" aria-selected="false">Данные приложения</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane<?=$tabPaneFirst?>" id="tabVerticalLeft1" role="tabpanel" aria-labelledby="baseVerticalLeft-tab1">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'telephone')->widget(MaskedInput::class, [
                                'mask' => '+7(999) 999-99-99',
                            ]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'role_id')->dropDownList((new Users())->getRoleList()) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'state')->dropDownList(Users::stateLabels()) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'groups')->widget(Select2::class, [
                                'data' => ArrayHelper::map(Group::find()->all(), 'id', 'name'),
                                'pluginOptions' => [
                                    'multiple' => true,
                                    'tags' => true,
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'atelier_id')->dropDownList(ArrayHelper::map(\app\models\Atelier::find()->all(), 'id', 'name'), ['prompt' => 'Выберите']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'isMaster')->checkbox() ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'can_dashboard')->checkbox() ?>
                            <?= $form->field($model, 'can_clients')->checkbox() ?>
                            <?= $form->field($model, 'can_storage')->checkbox() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'can_book')->checkbox() ?>
                            <?= $form->field($model, 'can_reports')->checkbox() ?>
                            <?= $form->field($model, 'can_users')->checkbox() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($model->isNewRecord): ?>
                                <?= \kato\DropZone::widget([
                                    'id'        => 'dzImage', // <-- уникальные id
                                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file' ]),
                                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                                    'options' => [
                                        'dictDefaultMessage' => 'Перетащите файлы чтобы загрузить',
                                        'autoDiscover' => false,
                                        'maxFilesize' => '50',
                                    ],
                                    'clientEvents' => [
                                        'success' => "function(file, response){
                                console.log(response);

                                var path = response.realPath;

                                var value = $('#users-files').val();
                                $('#users-files').val(value+','+path);
                             }",
//                        'complete' => "function(file){ console.log(file); }",
                                    ],
                                ]);?>
                            <?php else: ?>
                                <?= \kato\DropZone::widget([
                                    'id'        => 'dzImage', // <-- уникальные id
                                    'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'user_id' => $model->id]),
                                    'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                                    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                                    'options' => [
                                        'dictDefaultMessage' => 'Перетащите файлы чтобы загрузить',
                                        'maxFilesize' => '50',
                                    ],
                                ]);?>
                            <?php endif; ?>
                        </div>
                        <?= $form->field($model, 'files')->hiddenInput()->label(false) ?>
                    </div>
                </div>
                <div class="tab-pane<?=$tabPaneSecond?>" id="tabVerticalLeft2" role="tabpanel" aria-labelledby="baseVerticalLeft-tab2" style="overflow-x: hidden;">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'repeatPassword')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4 hidden">
                            <?= $form->field($model, 'vk_id')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php

$script = <<< JS
    $('.nav-vertical .nav.nav-tabs .nav-link').click(function(){
        if($(this).hasClass('active') === false){
            $('.nav.nav-tabs .nav-link.active').removeClass('active');
            $(this).addClass('active');
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>