<?php
use app\models\User;
use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Group;
use app\models\UsersGroup;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$model->groups = ArrayHelper::getColumn(UsersGroup::find()->where(['users_id' => $model->id])->all(), 'group_id');


?>

<div class="users-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="card">
        <div class="card-content">
            <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'groups')->widget(Select2::class, [
                                'data' => ArrayHelper::map(Group::find()->all(), 'id', 'name'),
                                'pluginOptions' => [
                                    'multiple' => true,
                                    'tags' => true,
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'state')->dropDownList(Users::stateLabels()) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'role_id')->dropDownList($model->getRoleList(), [/*'prompt' => 'Выберите должность'*/]) ?>
                        </div>
                    </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="hidden">
                            <?=  $form->field($model, 'file')->fileInput() ?>
                        </div>
                        <?= Html::a('<i class="feather icon-upload"></i> Загрузить файл', '#', ['id' => 'file-btn', 'class' => 'btn btn-primary', 'onclick' => '
                            event.preventDefault();
                            
                            $("#users-file").trigger("click");
                        ']) ?>
                    </div>
                </div>

                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group" style="margin-top: 15px;">
                        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS

$("#users-file").change(function(){
    $("#file-btn").attr('disabled', true);
    $("#file-btn").attr('class', 'btn btn-outline-primary');
    $("#file-btn").text('Файл загружен');
    $("#file-btn").attr('onclick', '');
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>