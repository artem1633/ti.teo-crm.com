<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'telephone')->widget(MaskedInput::class, [
                'mask' => '+7(999) 999-99-99',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'role_id')->dropDownList($model->getRoleList(), [/*'prompt' => 'Выберите должность'*/]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'access_for_moving')->dropDownList($model->getMovingList(), [/*'prompt' => 'Выберите должность'*/]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusList(), [/*'prompt' => 'Выберите должность'*/]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'atelier_id')->dropDownList($model->getAtelierList(), ['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'sales_percent')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cleaner_percent')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sewing_percent')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'repairs_percent')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'data_cr')->textInput() ?>        
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
