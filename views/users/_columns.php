<?php
use app\models\Group;
use app\models\UsersGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
        'label' => 'Логин',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'filter' => Users::getRoleList(),
        'attribute'=>'role_id',
        'content' => function($data){
            return $data->getRoleDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'groups',
        'content' => function($data){
            $groups = ArrayHelper::getColumn(UsersGroup::find()->where(['users_id' => $data->id])->all(), 'group_id');
            $groups = implode(', ', ArrayHelper::getColumn(Group::find()->where(['id' => $groups])->all(), 'name'));
            return $groups;
        },
        'width' => '20%',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'filter' => Users::getStatusList(),
//        'attribute'=>'status',
//        'content' => function($data){
//            return $data->getStatusDescription();
//        }
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'atelier_id',
//        'filter' => Users::getAtelierList(),
//        'content' => function($data){
//            return $data->atelier->name;
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'isOnline',
        'label' => 'Статус',
        'content' => function($data){
            if($data->isOnline){
                return '<div class="chip chip-success">
                                  <div class="chip-body">
                                    <div class="chip-text">Онлайн</div>
                                  </div>
                                </div>';
            } else {
                return '<div class="chip chip-danger">
                                  <div class="chip-body">
                                    <div class="chip-text">Оффлайн</div>
                                  </div>
                                </div>';
            }
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
//        'template' => '{leadView} {leadUpdate}',
        'template' => '{leadView} {leadDelete}',
        'buttons'  => [
            'leadView' => function ($url, $model) 
            {
                $url = Url::to(['/users/view', 'id' => $model->id]);
                return Html::a('<i class="feather icon-eye"></i>', $url, ['data-pjax'=>'0','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
            },           
            'leadUpdate' => function ($url, $model) 
            {
                if($model->id != 1){
                    $url = Url::to(['/users/update', 'id' => $model->id]);
                    return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
                }
            },
            'leadDelete' => function ($url, $model) 
            {
                if($model->id == 1){
                    return '';
                }

                if($model->id != $model->companies[0]->admin_id)
                {
                    $url = Url::to(['/users/delete', 'id' => $model->id]);
                    return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                        'style' => 'font-size: 23px;'
                    ]);
                }
            },
        ]
    ]

];   