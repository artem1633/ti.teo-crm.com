<?php

use app\models\Group;
use app\models\Users;
use app\models\UsersGroup;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<style>
    .checkbox-inline label {
        font-size: 16px;
        margin-right: 10px;
    }
</style>
<div class="users-view">
    <div class="clients-view">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= $model->login ?></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab-fill" data-toggle="tab" href="#home-fill" role="tab" aria-controls="home-fill" aria-selected="true">Основное</a>
                        </li>
                        <?php if($model->id != 1): ?>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill" role="tab" aria-controls="profile-fill" aria-selected="false">Привилегии доступа</a>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" id="messages-tab-fill" data-toggle="tab" href="#messages-fill" role="tab" aria-controls="messages-fill" aria-selected="false">Данные входа</a>
                        </li>
                    </ul>
                    <?php Pjax::begin(['id' => 'pjax-detail-view-container', 'enablePushState' => false]) ?>

                    <div class="tab-content pt-1">
                        <div class="tab-pane active" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                            <div class="row">
                                <div class="col-md-4">
                                    <?= DetailView::widget([
                                        'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                        'model' => $model,
                                        'attributes' => [
                                            //'id',
                                            'name',
                                            //'password',
//                                    [
//                                        'attribute' => 'access_for_moving',
//                                        'value' => function($data){
//                                            return $data->getMovingDescription();
//                                        }
//                                    ],
                                            [
                                                'attribute' => 'role_id',
                                                'value' => function($data){
                                                    return $data->getRoleDescription();
                                                }
                                            ],
                                            'telephone',
//                                    [
//                                        'attribute' => 'status',
//                                        'value' => function($data){
//                                            return $data->getStatusDescription();
//                                        }
//                                    ],
//                                    [
//                                        'attribute' => 'atelier_id',
//                                        'value' => function($data){
//                                            return $data->atelier->name;
//                                        }
//                                    ],
//                                    [
//                                        'attribute' => 'role_id',
//                                        'value' => function($data){
//                                            return \yii\helpers\ArrayHelper::getColumn($data->getRoleList(), $data->role_id);
//                                        }
//                                    ],
//                                    'sales_percent',
//                                    'cleaner_percent',
//                                    'sewing_percent',
//                                    'repairs_percent',
//                                    //'auth_key',
//                                    'data_cr',
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= DetailView::widget([
                                        'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                        'model' => $model,
                                        'attributes' => [
                                            //'id',
                                            //'password',
                                    [
                                        'attribute' => 'state',
                                        'value' => function($data){
                                            return \yii\helpers\ArrayHelper::getValue(\app\models\User::stateLabels(), $data->state);
                                        }
                                    ],
                                    [
                                        'attribute' => 'groups',
                                        'value' => function($data){
                                            $groups = ArrayHelper::getColumn(UsersGroup::find()->where(['users_id' => $data->id])->all(), 'group_id');
                                            $groups = implode(', ', ArrayHelper::getColumn(Group::find()->where(['id' => $groups])->all(), 'name'));
                                            return $groups;
                                        },
                                    ],
                                    'address',
//                                    [
//                                        'attribute' => 'status',
//                                        'value' => function($data){
//                                            return $data->getStatusDescription();
//                                        }
//                                    ],
//                                    [
//                                        'attribute' => 'atelier_id',
//                                        'value' => function($data){
//                                            return $data->atelier->name;
//                                        }
//                                    ],
//                                    [
//                                        'attribute' => 'role_id',
//                                        'value' => function($data){
//                                            return \yii\helpers\ArrayHelper::getColumn($data->getRoleList(), $data->role_id);
//                                        }
//                                    ],
//                                    'sales_percent',
//                                    'cleaner_percent',
//                                    'sewing_percent',
//                                    'repairs_percent',
//                                    //'auth_key',
//                                    'data_cr',
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= DetailView::widget([
                                        'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                        'model' => $model,
                                        'attributes' => [
                                            [
                                                'attribute' => 'documents',
                                                'label' => 'Файлы',
                                                'format' => 'raw',
                                                'value' => function($model){
                                                    $documents = json_decode($model->documents);

                                                    $documentsHTML = [];

                                                    $counter = 1;
                                                    foreach ($documents as $document)
                                                    {
                                                        $documentsHTML[] = Html::a("{$counter} Документ", "/{$document}", ['data-pjax' => 0, 'download' => "{$counter} Документ"]);
                                                        $counter++;
                                                    }

                                                    $output = implode(', ', $documentsHTML);

                                                    return $output;
                                                }
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                            <p style="margin-top: 15px;">
                                <?php if($model->id != 1): ?>
                                    <?= Html::a('Изменить', ['update-common', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => 0]) ?>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="tab-pane" id="profile-fill" role="tabpanel" aria-labelledby="profile-tab-fill">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox-inline">
                                        <?php

                                            if($model->role_id == Users::USER_ROLE_MASTER){
                                                $model->isMaster = 1;
                                            } else {
                                                $model->isMaster = 0;
                                            }

                                        ?>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_view', ['data-update' => 'can_view']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_update', ['data-update' => 'can_update']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_delete', ['data-update' => 'can_delete']) ?>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_dashboard', ['data-update' => 'can_dashboard']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_orders', ['data-update' => 'can_orders']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_clients', ['data-update' => 'can_clients']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_reports', ['data-update' => 'can_reports']) ?>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_users', ['data-update' => 'can_users']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_storage', ['data-update' => 'can_storage']) ?>
                                                </p>
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'can_book', ['data-update' => 'can_book']) ?>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'isMaster', ['data-update' => 'isMaster']) ?>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
                                                    <?= Html::activeCheckbox($model, 'accept_notifications', ['data-update' => 'accept_notifications']) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages-fill" role="tabpanel" aria-labelledby="messages-tab-fill">
                            <?= DetailView::widget([
                                'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                'model' => $model,
                                'attributes' => [
                                    'login',
                                    'vk_id',
                                ],
                            ]) ?>
                            <p style="margin-top: 15px;">
                                <?php if($model->id != 1): ?>
                                    <?= Html::a('Изменить', ['update-credentials', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => 0]) ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS

$('[data-update]').change(function(){
    var attr = $(this).data('update');
    if($(this).is(':checked')){
        $.get('/users/update-attribute?id={$model->id}&value=1&attr='+attr, function(){
            
        });
    } else {
        $.get('/users/update-attribute?id={$model->id}&value=0&attr='+attr, function(){
            
        });
    }
});

JS;


$this->registerJs($script, \yii\web\View::POS_READY);


?>
