<?php
use app\models\manual\Product;
use app\models\Manufacturer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;

$role = Yii::$app->user->identity->role_id;
if($role == Users::USER_ROLE_ADMIN) $disabled = false;
else $disabled = true;


$items = [];

/** @var Product[] $products */
$products = Product::find()->all();

foreach ($products as $product)
{
    $unitLabel = '';

    if($product->unit !== null){
        $unitLabel = ' ('.ArrayHelper::getValue(Product::unitLabels(), $product->unit).')';
    }

    $items[$product->id] = $product->name.$unitLabel;
}


?>

<div class="box box-default">   
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'product_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $items,
                        'options' => ['placeholder' => 'Выберите расходник'],
                        'pluginEvents' => [
                            "change" => "function() 
                            {
                                $.get('set-price',
                                {
                                    'id':$(this).val()
                                },
                                    function(data) { $('#price').val(data); }     
                                );
                                
                            }",
                        ], 
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label('Название расходника');
                ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'postavshik_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getSuppliersList(),
                    'options' => ['placeholder' => 'Выберите поставщика'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>

            <div class="hidden">
                <?= $form->field($model, 'storage_name')->textInput(['disabled' => 'true']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
            </div>


            <div class="col-md-3">
                <?= $form->field($model, 'price_shop')->textInput(['type' => 'number']) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'manufacturer_id')->dropDownList(ArrayHelper::map(Manufacturer::find()->all(), 'id', 'name'), ['prompt' => 'Выберите производителя']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>
            </div>
        </div>    

        <div style="display: none;">
            <?= $form->field($model, 'status_id')->dropDownList($model->getStatusList(), [/*'prompt' => 'Выберите статуса'*/]) ?>
            <?= $form->field($model, 'creator_id')->textInput() ?>
            <?= $form->field($model, 'number')->textInput() ?>     
        </div>
      
    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Оприходовать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть', ['index'],['data-pjax'=>0, 'class'=>'btn btn-warning']);?>
    	    </div>
    	<?php } ?>

        <?php ActiveForm::end(); ?>    
    </div>
</div>
