<?php

use app\models\manual\Product;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\AvailableSearch;

$session = Yii::$app->session;
$result = [];
foreach ($session['provider']->getModels() as $model) {
    if($model->number == $number) $result [] = $model->id;
}

$provider1 = AvailableSearch::searchById($result);
?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $provider1,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'product_id',
            'label' => 'Название расходника',
            'content' => function($data){
                return $data->product->name;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'count',
            'content' => function($model){
                return "{$model->count} ". ArrayHelper::getValue(Product::unitLabels(), $model->product->unit);
            }
        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'price',
//        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'price_shop',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'manufacturer.name',
            'label' => 'Производитель',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'postavshik_id',
            'content' => function($data){
                return $data->postavshik->name;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'comment',
        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'storage_id',
//            'content' => function($data){
//                return $data->storage->name;
//            }
//        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'good_id',
//            'content' => function($data){
//                return $data->good->name;
//            }
//        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'storage_id',
//            'label' => 'Магазин',
//            'content' => function($data){
//                return $data->storage->atelier->name;
//            }
//        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
