<?php
use app\models\manual\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

?>
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'remain',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
			    [
			        'class' => 'kartik\grid\SerialColumn',
			        'width' => '30px',
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'product_id',
			        'label' => 'Название расходника',
			        'content' => function($data){
			        	return $data->product->name;
			        }
			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'creator_id',
//			        'content' => function($data){
//			        	return $data->creator->name;
//			        }
//			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'postavshik_id',
//			        'content' => function($data){
//			        	return $data->postavshik->name;
//			        }
//			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'count',
                    'label' => 'Объем',
                    'value' => function($data){
                        $unitLabel = '';

                        if($data->product->unit !== null){
                            $unitLabel = ' '.ArrayHelper::getValue(Product::unitLabels(), $data->product->unit);
                        }

                        return $data->count.$unitLabel;
                    },
			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'price',
//			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'price_shop',
//			    ],
//			    [
//			        'class'    => 'kartik\grid\ActionColumn',
//			        'template' => '{leadMove} {leadDelete}',
//			        'buttons'  => [
//
//			            'leadView' => function ($url, $model) {
//			                $url = Url::to(['/resource/view', 'id' => $model->id]);
//			                return Html::a('<i class="feather icon-eye"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
//			            },
//			            'leadMove' => function ($url, $model) {
//			                $url = Url::to(['/available/move-storage', 'id' => $model->id, 'type' => 'resource', 'forceReload' => '#remain-pjax']);
//			                return Html::a('<i class="feather icon-corner-down-left"></i>', $url, ['role'=>'modal-remote','title'=>'Выдача', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
//			            },
//			            'leadUpdate' => function ($url, $model) {
//			                $url = Url::to(['/resource/update', 'id' => $model->id]);
//			                return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
//			            },
//			            'leadDelete' => function ($url, $model) {
//			                $url = Url::to(['/resource/delete', 'id' => $model->id]);
//			                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
//			                    'role'=>'modal-remote','title'=>'',
//			                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//			                          'data-request-method'=>'post',
//			                          'data-toggle'=>'tooltip',
//			                          'data-confirm-title'=>'Подтвердите действие',
//			                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?', 'style' => 'font-size: 16px;'
//			                ]);
//			            },
//			        ]
//			    ]
			],
			'toolbar'=> [
                [
                	'{export}' 
            	],
            ],  
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>