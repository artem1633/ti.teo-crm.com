﻿<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Available;
use app\models\AvailableSearch;

CrudAsset::register($this);

$numbers = [];
$models_id = [];
$result = [];
foreach ($dataProvider->getModels() as $model) 
{
	$numbers [] = $model->number;
	$models_id [] = $model->id;
}
$numbers = array_unique($numbers);

foreach ($numbers as $number) {
	$available = Available::find()->where(['number' => $number])->filterWhere(['like', 'product_id', $post['Available']['tovar'] ])->andFilterWhere(['like', 'storage_id', $post['Available']['storages'] ])->one();
	if($available != null) $result [] = $available->id;
}

$provider = AvailableSearch::searchById($result);
$session = Yii::$app->session;
$session['provider'] = $dataProvider;

?>
	<div class="box box-default">  
        <div class="box-body">
            <div class="hidden">
                <?= $this->render('_search', ['model' => new \app\models\Available(), 'post'=>$post]); ?>
            </div>
       </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id'=>'available',
            'dataProvider' => $provider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
			    return ['data-id' => $model->id];
			},
            'pjax'=>true,
            'columns' => [
			    [
			        'class' => 'kartik\grid\SerialColumn',
			        'width' => '30px',
			    ],
			    [
			        'class'=>'kartik\grid\ExpandRowColumn', 
			        'width'=>'50px',
			        'value'=>function ($model, $key, $index, $column) {
			            return GridView::ROW_COLLAPSED;
			        },
			        'detail'=>function ($model, $key, $index, $column) {

			            return \Yii::$app->controller->renderPartial('_expand-goods', ['number'=>$model->number, /*'provider' => $provider*/]);
			        },
			        'headerOptions'=>['class'=>'kartik-sheet-style'],
                    'collapseIcon' => '<i class="text-primary feather icon-corner-right-up"></i>',
                    'expandIcon' => '<i class="text-primary feather icon-corner-right-down"></i>',
                    'expandOneOnly'=>true
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'number',
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Принял',
			        'attribute'=>'creator_id',
			        'content' => function($data){
			        	return $data->creator->name;
			        },
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'date_cr',
                    'format' => ['date', 'php:d-m-Y'],
			    ],
			    [
			        'class'    => 'kartik\grid\ActionColumn',
			        'template' => '{leadDelete}',
			        'buttons'  => [
			            'leadView' => function ($url, $model) {
			                $url = Url::to(['/available/view-available', 'id' => $model->id]);
			                return Html::a('<i class="feather icon-eye"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
			            },
			            'leadMove' => function ($url, $model) {
			                $url = Url::to(['/available/move-storage', 'id' => $model->id, 'type' => 'available', 'forceReload' => '#available-pjax']);
			                return Html::a('<i class="feather icon-corner-down-left"></i>', $url, ['role'=>'modal-remote','title'=>'Перемещения', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
			            },
			            'leadUpdate' => function ($url, $model) {
			                $url = Url::to(['/available/update', 'id' => $model->id]);
			                return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
			            },
			            'leadDelete' => function ($url, $model) {
			                $url = Url::to(['/available/delete-available', 'id' => $model->id]);
			                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
			                    'role'=>'modal-remote','title'=>'',
			                          'data-confirm'=>false, 'data-method'=>false,
			                          'data-request-method'=>'post',
			                          'data-toggle'=>'tooltip',
			                          'data-confirm-title'=>'Подтвердите действие',
			                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?', 'style' => 'font-size: 23px;'
			                ]);
			            },
			        ]
			    ]
			],
//			'toolbar'=> [
//                [
//                	'content'=> Html::a('Создать', ['/available/create', 'status' => 1], ['data-pjax'=>'0','title'=> 'Создать','class'=>'btn btn-primary']) . '{export}'
//            	],
//            ],
            'panelBeforeTemplate' => Html::a('Принять', ['/available/create', 'status' => 1],
                    ['data-pjax' => 0, 'title'=> 'Создать','class'=>'btn btn-primary']).
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
$this->registerJs("
	$('#available-container  tbody td').css('cursor', 'pointer');

    $('#available-container tr td').on('dblclick', function(e){
	    var id = $(this).closest('tr').data('id');
	    location.href = '" . Url::to(['/available/view']) . "?id=' +id;
	});

");
?>