<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use kartik\tabs\TabsX;

$this->title = '';
CrudAsset::register($this);

$waiting_visible = false;
$insource_visible = false;
$application_visible = false;
$part_visible = true;

if(isset($post['Available']['active_window'])) { $insource_visible = true; $part_visible = false; }
if(isset($post['Resource']['active_window'])) { $waiting_visible = true; $part_visible = false; }
if(isset($post['Application']['active_window'])) { $application_visible = true; $part_visible = false; }

?>
<div class="available-index">
    <div id="ajaxCrudDatatable">
        <br>
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Склад</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="messages-tab-fill" data-toggle="tab" href="#messages-fill" role="tab" aria-controls="messages-fill" aria-selected="false">Оприходовано</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#home-fill" role="tab" aria-controls="home-fill" aria-selected="true">Остатки</a>
                        </li>
                        <li class="nav-item hidden">
                            <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill" role="tab" aria-controls="profile-fill" aria-selected="false">Ожидает поступления</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="settings-tab-fill" data-toggle="tab" href="#settings-fill" role="tab" aria-controls="settings-fill" aria-selected="false">История</a>
                        </li>
                    </ul>
                    <div class="tab-content pt-1">
                        <div class="tab-pane active" id="messages-fill" role="tabpanel" aria-labelledby="messages-tab-fill">
                            <?=$contentInResourceWaid?>
                        </div>
                        <div class="tab-pane" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                            <?=$remainResource?>
                        </div>
                        <div class="tab-pane hidden" id="profile-fill" role="tabpanel" aria-labelledby="profile-tab-fill">
                            <?=$waitingResource?>
                        </div>
                        <div class="tab-pane" id="settings-fill" role="tabpanel" aria-labelledby="settings-tab-fill">
                            <?= $this->render('changing', [
                                'result' => $result ,
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
