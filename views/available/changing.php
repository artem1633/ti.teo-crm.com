<?php

use app\models\Storage;
use app\models\manual\Product;
use yii\helpers\ArrayHelper;

?> 
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">
                        <tr>
                            <th>Дата/Время</th>
                            <th>Услуга</th>
                            <th>Расходник</th>
                            <th>Мастер</th>
                            <th>Старое значение</th>
                            <th>Списано</th>
                            <th>Изменение</th>
                        </tr>
                        <?php
                        foreach ($result as $change) {
                            if($change->table == 'resource') $tip = 'Остатки';
                            if($change->table == 'waiting') $tip = 'Ожидает поступления';
                            if($change->table == 'available') $tip = 'Оприходовано';
                            $storage_from = Storage::find()->where(['id' => $change->storage_form])->one();
                            $storage_to = \app\models\Users::find()->where(['id' => $change->storage_to])->one();
                            $tovar = Product::find()->where(['id' => $change->part_id])->one();
                        ?>
                            <tr>
                                <td><?=\Yii::$app->formatter->asDate($change->data, 'php:H:i, d.m.Y')?></td>
                                <td><?= ArrayHelper::getValue($change, 'job.name') ?></td>
                                <td><?=$tovar->name?></td>
                                <td><?=$storage_to->name?></td>
                                <td><?=$change->old_count?></td>
                                <td><?=($change->old_count - $change->sending_count)?></td>
                                <td><?=$change->sending_count?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
