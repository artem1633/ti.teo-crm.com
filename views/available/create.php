<?php

use app\models\manual\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\Available */
$this->title = 'Поступление на склад:';
?>
<div class="available-create">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Форма</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Документ</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
			<?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'columns' => [
		            'id',			
					[
						'attribute' => 'product_id',	
						'value'=>'product.name',
                        'label' => 'Название расходника'
					],
                    [
                        'attribute' => 'count',
                        'value' => function($data){
                            $unitLabel = '';

                            if($data->product->unit !== null){
                                $unitLabel = ' '.ArrayHelper::getValue(Product::unitLabels(), $data->product->unit);
                            }

                            return $data->count.$unitLabel;
                        },
                    ],
//		            'price',
                    'price_shop',
					[
						'attribute' => 'postavshik_id',
						'value'=>'postavshik.name',
					],
                    [
                        'attribute' => 'manufacturer_id',
                        'value'=>'manufacturer.name',
                    ],
		            'comment',
		            ['class' => 'yii\grid\ActionColumn',
					'template' => '{delete}'
					],
		        ],
		    ]); ?>
        </div>
    </div>
</div>
