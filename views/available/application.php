<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="application-index">
    <div class="box box-default">  
        <div class="box-body">
            <?= $this->render('_search_application', ['model' => new \app\models\Application(), 'post'=>$post]); ?> 
       </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'application',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'atelier_id',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                    'content' => function($data){
                        return $data->atelier->name;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'product_id',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                    'content' => function($data){
                        return $data->product->name;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'count',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'status_id',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                    'content' => function($data){
                        return $data->status->name;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'date_cr',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                    'content' => function($data){
                        return \Yii::$app->formatter->asDatetime($data->date_cr, "php:d.m.Y");
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'creator',
                    'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'color:black;background-color:'.$model->status->color ];
                    },
                    'content' => function($data){
                        return $data->creator0->name;
                    }
                ],
                [
                    'class'    => 'kartik\grid\ActionColumn',
                    'template' => '{leadUpdate} {leadDelete}',
                    'buttons'  => [
                        'leadUpdate' => function ($url, $model) {
                            $url = Url::to(['/application/update', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                        },
                        'leadDelete' => function ($url, $model) {
                            $url = Url::to(['/application/delete', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'role'=>'modal-remote','title'=>'', 
                                      'data-confirm'=>false, 'data-method'=>false,
                                      'data-request-method'=>'post',
                                      'data-toggle'=>'tooltip',
                                      'data-confirm-title'=>'Подтвердите действие',
                                      'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                            ]);
                        },
                    ]
                ]
            ],          
            'toolbar'=> [
                ['content'=> Html::a('Создать', ['/application/add'],
                    ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).'{export}' ],
            ],  
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
