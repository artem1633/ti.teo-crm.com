<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Available */
?>
<div class="available-update">

    <?= $this->render('_update_form', [
        'model' => $model,
    ]) ?>

</div>
