<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storage-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    	<div class="col-md-12">
    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>    		
    	</div>
    	<div class="col-md-6">
		    <?= $form->field($model, 'user_id')->dropDownList($model->getUsersList(), ['prompt' => 'Выберите пользователя']) ?>
    	</div>
    	<div class="col-md-6">
        	<?= $form->field($model, 'atelier_id')->dropDownList($model->getAtelierList(), ['prompt' => 'Выберите ателье']) ?>
    	</div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
