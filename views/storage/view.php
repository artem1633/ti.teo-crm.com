<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
?>
<div class="storage-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            //'user_id',
            [
            	'attribute' => 'user_id',
            	'value' => function($data){
            		return $data->user->name;
            	}
            ],
            //'atelier_id',
            [
            	'attribute' => 'atelier_id',
            	'value' => function($data){
            		return $data->atelier->name;
            	}
            ],
        ],
    ]) ?>

</div>
