<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Storage */

?>
<div class="storage-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
