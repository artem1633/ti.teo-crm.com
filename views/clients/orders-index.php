<?php

use app\models\JobList;
use app\models\manual\ListServices;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?=GridView::widget([
    'id'=>'crud-datatable',
    'options' => ['class' => 'theme-table'],
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'tableOptions' => ['style' => "background: #f5f5f5;"],
    'pjax'=>true,
    'columns' => [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'label' => 'Админ',
            'attribute'=>'company_id',
            'value'=>'company.admin.name',
            'contentOptions' => ['style' => 'font-size: 14px;'],
            'width' => '10%',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'fact_date',
            'label' => 'Дата',
            'format' => ['date', 'php:d.m.Y H:i'],
            'contentOptions' => ['style' => 'font-size: 14px;'],
            'width' => '10%',
        ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_id',
//        'content' => function($data){
//            return $data->status->name;
//        }
//    ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'client_id',
            'content' => function($data){
                $output = '';

                $output .= $data->client->fio.'<br>';
                $output .= ArrayHelper::getValue($data, 'marks.name').' '.ArrayHelper::getValue($data, 'spot.name').', '.$data->number;

                return $output;
            },
            'contentOptions' => ['style' => 'font-size: 14px;'],
        ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_id',
//        'value' => 'status.name',
//        'content' => function($model){
//            return Html::dropDownList('status_id_'.$model->id, $model->status_id, ArrayHelper::map(\app\models\manual\OrderStatus::find()->all(), 'id', 'name'), [
//                'class' => 'form-control',
//                'data-status' => $model->id,
//            ]);
//        },
//    ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'label'=>'Услуги',
            'content' => function($model){
                $services = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $model->id])->orderBy('id asc')->all(),'job_id');
                $serviceList = ArrayHelper::getColumn(ListServices::find()->where(['id' => $services])->orderBy('id asc')->all(), 'name');

                array_walk($serviceList, function(&$service){
                    $service = "<span style='display: inline-block; padding: 5px 0;'>{$service}</span>";
                });

                return "<div style='font-size: 14px;'>".implode('<br>', $serviceList)."</div>";
            },
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'user_id',
            'content' => function($model){
                $output = '';
                $services = JobList::find()->select('job_list.id, users.name as master_name')->joinWith('master')->where(['order_id' => $model->id])->orderBy('job_list.id asc')->asArray()->all();

                foreach ($services as $service)
                {
                    $output .= "<span style='display: inline-block; padding: 5px 0;'>".($service['master_name'] != '' ? $service['master_name'] :
                            Html::a('Назначить мастера', ['job-list/apply-master', 'id' => $service['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['role' => 'modal-remote']))."</span>";
                    $output .= '<br>';
                }

                return "<div style='font-size: 14px;'>{$output}</div>";
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'cost',
            'label' => 'Сумма/Аванс',
            'content' => function($data){
//            $price = JobList::find()->where(['order_id' => $data->id])->sum('price');
//            $debt = JobList::find()->where(['order_id' => $data->id])->sum('prepaid');

                $output = '';

                $jobLists = JobList::find()->where(['order_id' => $data->id])->orderBy('id asc')->all();

                foreach ($jobLists as $jobList)
                {
                    $output .= "<span style='display: inline-block; padding: 5px 0;'>{$jobList->price}/{$jobList->prepaid}</span><br>";
                }

                return "<div style='font-size: 14px;'>{$output}</div>";
            }
        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'status_id',
//            'label' => 'Статус ',
////        'content' => function($data){
////            return $data->status->name;
////        },
//            'content' => function($model){
//                $services = JobList::find()->select('job_list.id as id, order_status.name as status_name')->joinWith('status')->where(['order_id' => $model->id])->orderBy('job_list.id asc')->asArray()->all();
//
//                $output = '';
//
//                array_walk($services, function(&$service) use (&$output){
//                    if($service['status_name'] == 'Новый'){
//                        $service['status_name'] = '<div class="chip chip-success">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    } else if($service['status_name'] == 'Предварительный'){
//                        $service['status_name'] = '<div class="chip chip-info">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    } else if($service['status_name'] == 'В работе'){
//                        $service['status_name'] = '<div class="chip chip-yellow">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    } else if($service['status_name'] == 'На проверке'){
//                        $service['status_name'] = '<div class="chip chip-warning">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    } else if($service['status_name'] == 'Выполненный'){
//                        $service['status_name'] = '<div class="chip chip-purple">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    } else if($service['status_name'] == 'Отмененный'){
//                        $service['status_name'] = '<div class="chip chip-black">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.$service['status_name'].'</div>
//                                  </div>
//                                </div>';
//                    }
//
//                    $output .= $service['status_name']."<br>";
//                });
//
//                return $output;
//            },
//        ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'acceptor_id',
//        'content' => function($data){
//            return $data->acceptor->name;
//        }
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'atelier_id',
//        'content' => function($data){
//            return $data->atelier->name;
//        }
//    ],
//        [
//            'class'    => 'kartik\grid\ActionColumn',
//            'template' => '{add-job} {leadView} {leadDelete}',
//            'buttons'  => [
//                'add-job' => function ($url, $model)
//                {
//                    $url = Url::to(['/orders/add-job', 'id' => $model->id, 'job' => 'service']);
//                    return Html::a('<i class="feather icon-plus-square"></i>', $url, ['role' => 'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
//                        'onclick' => '$("#ajaxCrudModal").addClass("modal-slg");',
//                    ]);
//                },
//                'leadView' => function ($url, $model)
//                {
//                    $url = Url::to(['/orders/view', 'id' => $model->id]);
//                    return Html::a('<i class="feather icon-eye"></i>', $url, ['data-pjax' => 0,'title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
//                },
//                'leadUpdate' => function ($url, $model)
//                {
//                    $url = Url::to(['/orders/update', 'id' => $model->id]);
//                    return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
//                },
//                'leadDelete' => function ($url, $model)
//                {
//                    $url = Url::to(['/orders/delete', 'id' => $model->id]);
//                    return Html::a('<i class="feather icon-trash-2"></i>', $url, [
//                        'role'=>'modal-remote','title'=>'',
//                        'style' => 'font-size: 16px;',
//                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                        'data-request-method'=>'post',
//                        'data-toggle'=>'tooltip',
//                        'data-confirm-title'=>'Подтвердите действие',
//                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
//                    ]);
//                },
//            ]
//        ]

    ],
    'summaryOptions' => ['style' => 'display: none;'],
    'showPageSummary' => false,
    'panelBeforeTemplate' => '',
    'striped' => false,
    'bordered' => false,
    'condensed' => false,
    'responsive' => false,
    'panel' => '',
])?>
