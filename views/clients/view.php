<?php

use app\models\Clients;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

CrudAsset::register($this);


?>
<div class="clients-view">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#home-fill" role="tab" aria-controls="home-fill" aria-selected="true"><i class="fa fa-user"></i> Основное</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill" role="tab" aria-controls="profile-fill" aria-selected="false"><i class="fa fa-car"></i> Автомобили</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="messages-tab-fill" data-toggle="tab" href="#messages-fill" role="tab" aria-controls="messages-fill" aria-selected="false" onclick="setTimeout(function(){ $(window).scrollTop(0); }, 30);"><i class="fa fa-clock-o"></i> История заказов</a>
                    </li>
                    <li class="nav-item<?=$model->type == 0 ? ' hidden' : ''?>">
                        <a class="nav-link" id="bank-tab-fill" data-toggle="tab" href="#bank-fill" role="tab" aria-controls="bank-fill" aria-selected="false"><i class="fa fa-credit-card"></i> Банковские реквезиты</a>
                    </li>
                </ul>
                <?php Pjax::begin(['id' => 'pjax-detail-view-container', 'enablePushState' => false]) ?>

                <div class="tab-content pt-1">
                    <div class="tab-pane" id="home-fill" role="tabpanel" aria-labelledby="#home-tab-fill">
                            <h4 style="margin-bottom: 15px;"><?= $model->type === Clients::TYPE_PERSON ? "Физическое лицо" : "Юридическое лицо" ?></h4>
                            <div class="row">
                                <?php if($model->type === Clients::TYPE_PERSON): ?>
                                    <div class="col-md-4">
                                        <?= DetailView::widget([
                                            'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                            'model' => $model,
                                            'attributes' => [
                                                //'id',
                                                [
                                                    'attribute' => 'fio',
                                                    'visible' => $model->type === Clients::TYPE_PERSON,
                                                ],
//                                            [
//                                                'attribute' => 'group_id',
//                                                'value' => function($data){
//                                                    return $data->group->name;
//                                                }
//                                            ],
//                                            [
//                                                'attribute' => 'id',
//                                                'label' => 'Автомобили',
//                                                'value' => function($data) use ($autoDataProvider){
//                                                    $autos = $autoDataProvider->models;
//                                                    $autosNames = null;
//                                                    foreach ($autos as $auto){
//                                                        $autosNames[] = implode(' ', [$auto->marks->name, $auto->spot->name]);
//                                                    }
//                                                    return implode(', ', $autosNames);
//                                                },
//                                            ],
                                            ],
                                        ]) ?>
                                    </div>
                                <?php endif; ?>
                                <?php if($model->type === Clients::TYPE_COMPANY): ?>
                                    <div class="col-md-4">
                                        <?= DetailView::widget([
                                            'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                            'model' => $model,
                                            'attributes' => [
                                                //'id',
                                                [
                                                    'attribute' => 'company_name',
                                                ],
                                                'company_full_name',
                                                'contact_person',
                                                'address',

//
                                            ],
                                        ]) ?>
                                    </div>
                                <?php endif; ?>
                                <div class="col-md-4">
                                    <?= DetailView::widget([
                                        'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                        'model' => $model,
                                        'attributes' => [
                                            //'id',
                                            [
                                                'attribute' => 'address',
                                                'visible' => $model->type === Clients::TYPE_PERSON
                                            ],
                                            'telephone',
                                            'description',
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        <p>
                            <?= Html::a('Изменить', ['update-common', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => '0']) ?>
                            <span class="hidden">
                                <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'redirect' => Url::toRoute(['index'])], [
                                    'class' => 'btn btn-danger',
                                    'role'=>'modal-remote', 'title'=>'Удалить',
                                    'data-confirm'=>false, 'data-method'=>false,
                                    'data-request-method'=>'post',
                                    'data-toggle'=>'tooltip',
                                    'data-confirm-title'=>'Подтвердите действие',
                                    'data-confirm-message'=>'Вы уверены что хотите удалить этого клиета?'
                                ]) ?>
                            </span>
                        </p>
                    </div>
                    <div class="tab-pane" id="profile-fill" role="tabpanel" aria-labelledby="#profile-tab-fill">
                        <?= $this->render('cars-index', [
                            'searchModel' => $autoSearchModel,
                            'dataProvider' => $autoDataProvider,
                            'client' => $model,
                        ]) ?>
                    </div>
                    <div class="tab-pane" id="messages-fill" role="tabpanel" aria-labelledby="#messages-tab-fill">
                        <?= $this->render('orders-index', [
                            'searchModel' => $orderSearchModel,
                            'dataProvider' => $orderDataProvider,
                        ]) ?>
                    </div>
                    <div class="tab-pane" id="bank-fill" role="tabpanel" aria-labelledby="#bank-tab-fill">
                        <h4 style="margin-bottom: 15px;"><?= $model->type === Clients::TYPE_PERSON ? "Физическое лицо" : "Юридическое лицо" ?></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <?= DetailView::widget([
                                    'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                    'model' => $model,
                                    'attributes' => [
                                        //'id',
                                        'bank_name',
                                        [
                                            'attribute' => 'bin',
                                            'label' => 'БИН/ИНН',
                                        ],
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= DetailView::widget([
                                    'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                    'model' => $model,
                                    'attributes' => [
                                        //'id',
                                        'iban',
                                        'bik',
                                    ],
                                ]) ?>
                            </div>
                        </div>
                        <p>
                            <?= Html::a('Изменить', ['update-bank', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => '0']) ?>
                        </p>
                    </div>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS

if(window.location.hash != ''){
    $(window.location.hash).addClass('active');
    $('[aria-labelledby="'+window.location.hash+'"]').addClass('active');
} else {
    $('.nav-tabs .nav-link').first().addClass('active');
    $('[aria-labelledby]').first().addClass('active');
}

$('[data-toggle]').click(function(){
    var id = $(this).attr('id');
    window.location.hash = id;
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>