<?php
use app\models\ClientsAuto;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Clients;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_name',
        'label' => 'Компания'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contact_person',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'group_id',
//        'filter' => Clients::getGroupList(),
//        'content' => function($data){
//            return $data->group->name;
//        }
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'atelier_id',
//        'filter' => Clients::getAtelierList(),
//        'content' => function($data){
//            return $data->atelier->name;
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        'label' => 'Контакты'
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'email',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'address',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Автомобили',
        'content' => function($model){
            $autos = ClientsAuto::find()->where(['client_id' => $model->id])->all();
            $autosNames = null;
            foreach ($autos as $auto){
                $autosNames[] = implode(' ', [$auto->marks->name, $auto->spot->name]);
            }
            return implode(', ', $autosNames);
        }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sale_for_work',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sale_for_product',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'usd_ball',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'buttons' => [
            'view' => function($url, $model){
                return Html::a('<i class="feather icon-eye"></i>', ['clients/view-company', 'id' => $model->id], [
                    'data-pjax' => 0, 'title'=>'Просмотр','data-toggle'=>'tooltip', 'style' => 'font-size: 23px;',
                ]);
            },
            'delete' => function($url, $model){
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    'style' => 'font-size: 23px;',
                ]);
            },
        ],
        'template' => '{view} {delete}',
        'viewOptions'=>['pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'display: none;'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Подтвердите действие',
            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'],
    ],

];