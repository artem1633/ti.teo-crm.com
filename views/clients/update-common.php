<?php
use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, $model->type === Clients::TYPE_PERSON ? 'fio' : 'company_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6 <?=$model->type === Clients::TYPE_PERSON ? 'hidden' : ''?>">
                        <?= $form->field($model, 'company_full_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6 <?=$model->type === Clients::TYPE_PERSON ? 'hidden' : ''?>">
                        <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'address')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'telephone')->widget(\yii\widgets\MaskedInput::class, [
                            'mask' => '+7(999) 999-99-99'
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'description')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? 'Готово' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <?= Html::a('Закрыть', ['view', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <?php ActiveForm::end(); ?>

</div>
