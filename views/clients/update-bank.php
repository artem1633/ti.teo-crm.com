<?php
use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'bin')->textInput(['maxlength' => true])->label('БИН/ИНН') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'bik')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? 'Готово' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <?= Html::a('Закрыть', ['view', 'id' => $model->id, '#' => 'bank-tab-fill'], ['class' => 'btn btn-warning']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <?php ActiveForm::end(); ?>

</div>
