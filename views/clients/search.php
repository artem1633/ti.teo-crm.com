<?php
use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(['id' => 'form-search', 'method' => 'GET', 'class' => 'form-inline']); ?>

        <?= $form->field($model, 'fio')->textInput(['placeholder' => 'Поиск', 'style' => 'width: 10%; float: left;'])->label(false) ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
$('#form-search input').change(function(){
   $('#form-search').submit();
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
