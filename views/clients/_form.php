<?php
use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\color\ColorInput;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php if($model->type == Clients::TYPE_PERSON): ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'telephone')->widget(MaskedInput::class, [
                    'mask' => '+7(999) 999-99-99',
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'description')->textarea() ?>
            </div>
            <div class="col-md-12">
                <?php
                            echo $form->field($model, 'cars')->widget(\unclead\multipleinput\MultipleInput::class, [
                                'id' => 'my_id',
                                'addButtonOptions' => [
                                    'class' => 'btn btn-sm btn-primary',
                                    'label' => '+',
                                ],
                                'removeButtonOptions' => [
                                    'label' => 'x',
                                ],
                                'min' => 0,
                                'columns' => [
                                    [
                                        'name' => 'id',
                                        'options' => [
                                            'type' => 'hidden'
                                        ]
                                    ],

                                    [
                                        'name'  => 'type_id',
                                        'title' => 'Тип транспорта',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\TypeClothes::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'marks_id',
                                        'title' => 'Марка',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\Marking::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'spot_id',
                                        'title' => 'Модель',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\Spot::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'number',
                                        'title' => 'Номер',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                    [
                                        'name'  => 'passport',
                                        'title' => 'Тех паспорт',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                    [
                                        'name'  => 'color',
                                        'title' => 'Цвет',
                                        'type'  => ColorInput::class,
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                    ],

                                    [
                                        'name'  => 'year',
                                        'title' => 'Год выпуска',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                ],
                            ])->label(false);
                ?>
            </div>
        </div>

    <?php elseif($model->type == Clients::TYPE_COMPANY): ?>

        <div class="nav-vertical">
            <ul class="nav nav-tabs nav-left flex-column" role="tablist" style="height: 56px;">
                <li class="nav-item">
                    <a class="nav-link" id="baseVerticalLeft-tab1" data-toggle="tab" aria-controls="tabVerticalLeft1" href="#tabVerticalLeft1" role="tab" aria-selected="false">Основная информация</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="baseVerticalLeft-tab2" data-toggle="tab" aria-controls="tabVerticalLeft2" href="#tabVerticalLeft2" role="tab" aria-selected="false">Банковские реквизиты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="baseVerticalLeft-tab3" data-toggle="tab" aria-controls="tabVerticalLeft3" href="#tabVerticalLeft3" role="tab" aria-selected="false">Авто</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" style="overflow-y: initial;" id="tabVerticalLeft1" role="tabpanel" aria-labelledby="baseVerticalLeft-tab1">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'company_full_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'telephone')->widget(MaskedInput::class, [
                                'mask' => '+7(999) 999-99-99',
                            ]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'description')->textarea() ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" style="overflow-y: initial;" id="tabVerticalLeft2" role="tabpanel" aria-labelledby="baseVerticalLeft-tab2">

                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'bin')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'bik')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" style="overflow-y: initial;" id="tabVerticalLeft3" role="tabpane3" aria-labelledby="baseVerticalLeft-tab3">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $form->field($model, 'cars')->widget(\unclead\multipleinput\MultipleInput::class, [
                                'id' => 'my_id',
                                'addButtonOptions' => [
                                    'class' => 'btn btn-sm btn-primary',
                                    'label' => '+',
                                ],
                                'removeButtonOptions' => [
                                    'label' => 'x',
                                ],
                                'min' => 0,
                                'columns' => [
                                    [
                                        'name' => 'id',
                                        'options' => [
                                            'type' => 'hidden'
                                        ]
                                    ],

                                    [
                                        'name'  => 'type_id',
                                        'title' => 'Тип транспорта',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\TypeClothes::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'marks_id',
                                        'title' => 'Марка',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\Marking::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'spot_id',
                                        'title' => 'Модель',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ],
                                        'items' => ArrayHelper::map(\app\models\manual\Spot::find()->all(), 'id', 'name')
                                    ],

                                    [
                                        'name'  => 'number',
                                        'title' => 'Номер',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                    [
                                        'name'  => 'passport',
                                        'title' => 'Тех паспорт',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                    [
                                        'name'  => 'color',
                                        'title' => 'Цвет',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                    [
                                        'name'  => 'year',
                                        'title' => 'Год выпуска',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority'
                                        ]
                                    ],

                                ],
                            ])->label(false);

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <?php endif; ?>


    <div class="row">
        <div class="col-md-12">
            <?php

            ?>
        </div>
    </div>
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Готово' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
