<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsAutoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $client \app\models\Clients */

CrudAsset::register($this);

?>
<div class="clients-auto-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-auto-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_cars_columns.php'),
            'panelBeforeTemplate' => $this->render('search-cars', ['model' => $searchModel, 'client' => $client]) . Html::a('Создать', ['clients-auto/create', 'client_id' => $client->id, 'reloadPjaxContainer' => '#crud-auto-datatable-pjax'],
                    ['role' => 'modal-remote', 'title'=> 'Добавить','class'=>'btn btn-primary', 'style' => 'margin-left: 6px;']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'headingOptions' => ['style' => 'display: none;'],
            ]
        ])?>
    </div>
</div>