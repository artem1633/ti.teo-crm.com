<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\ProductStatus */
?>
<div class="product-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
