<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\ProductStatus */

?>
<div class="product-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
