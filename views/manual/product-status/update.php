<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\ProductStatus */
?>
<div class="product-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
