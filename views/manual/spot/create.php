<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Spot */

?>
<div class="spot-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
