<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Spot */
?>
<div class="spot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
