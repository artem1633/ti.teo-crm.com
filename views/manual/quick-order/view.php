<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\QuickOrder */
?>
<div class="quick-order-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
