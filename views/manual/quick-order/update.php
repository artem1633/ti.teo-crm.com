<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\QuickOrder */
?>
<div class="quick-order-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
