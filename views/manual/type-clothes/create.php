<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\TypeClothes */

?>
<div class="type-clothes-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
