<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Advertising */
?>
<div class="advertising-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
