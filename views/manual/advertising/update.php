<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Advertising */
?>
<div class="advertising-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
