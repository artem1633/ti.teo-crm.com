<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Advertising */

?>
<div class="advertising-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
