<?php
use kartik\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update} {delete}',
        'buttons' => [
            'view' => function($url, $model){
                return Html::a('<i class="feather icon-eye"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                ]);
            },
            'update' => function($url, $model){
                return Html::a('<i class="feather icon-edit"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;',
                ]);
            },
            'delete' => function($url, $model){
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    'style' => 'font-size: 23px;',
                ]);
            },
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip', 'style' => 'display: none;'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];   