<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\ListServices */

?>
<div class="list-services-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
