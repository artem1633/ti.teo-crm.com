<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\ListServices */
?>
<div class="list-services-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
