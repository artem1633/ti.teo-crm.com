<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\ListServices */
?>
<div class="list-services-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
