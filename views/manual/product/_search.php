<?php
use app\models\Clients;
use app\models\manual\Product;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(['id' => 'form-search', 'method' => 'GET', 'class' => 'form-inline']); ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Название', 'style' => 'width: 10%; float: left;'])->label(false) ?>

    <?= $form->field($model, 'unit')->dropDownList(Product::unitLabels(), ['prompt' => 'Ед. измерения', 'style' => 'width: 10%; float: left; margin-left: 3px;'])->label(false) ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
$('#form-search input, #form-search select').change(function(){
   $('#form-search').submit();
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
