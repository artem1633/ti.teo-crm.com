<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\TypePollution */
?>
<div class="type-pollution-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
