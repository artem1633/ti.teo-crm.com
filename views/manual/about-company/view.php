<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\manual\AboutCompany */
$this->title = '';
CrudAsset::register($this);
?>
<div class="box box-solid box-primary">    
    <div class="box-header">
        <div class="btn-group pull-right">
            <?=Html::a('<i class="fa fa-pencil"></i></a>', ['/about-company/update', 'id' => $model->id], [ 'role'=>'modal-remote', 'class'=> 'btn btn-warning btn-sm' ])?>                
            </div>
        <h3 class="box-title">Юридические лица</h3>
    </div>
    <div class="box box-default">   
        <div class="box-body">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>  
            <div class="row"> 
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('name')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('director')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('address')?></b></th>
                                </tr>                                                    
                                <tr>
                                    <td><?=Html::encode($model->name)?></td>
                                    <td><?=Html::encode($model->director)?></td>
                                    <td><?=Html::encode($model->address)?></td>   
                                </tr>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('bank')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('inn')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('kpp')?></b></th>
                                </tr>
                                <tr>
                                    <td><?=Html::encode($model->bank)?></td>        
                                    <td><?=Html::encode($model->inn)?></td> 
                                    <td><?=Html::encode($model->kpp)?></td>
                                </tr>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('okpo')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('telephone')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('ogrn')?></b></th>
                                </tr>
                                <tr>
                                    <td><?=Html::encode($model->okpo)?></td>
                                    <td><?=Html::encode($model->telephone)?></td>
                                    <td><?=Html::encode($model->ogrn)?></td>
                                </tr>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('bik')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('kor_schot')?></b></th>
                                    <th colspan="2"><b><?=$model->getAttributeLabel('site')?></b></th>
                                </tr>                                                    
                                <tr>
                                    <td><?=Html::encode($model->bik)?></td>
                                    <td><?=Html::encode($model->kor_schot)?></td>
                                    <td colspan="2"><?=Html::encode($model->site)?></td>
                                </tr>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('email')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('service_login')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('service_parol')?></b></th>
                                </tr>
                                <tr>
                                    <td><?=Html::encode($model->email)?></td>
                                    <td><?=Html::encode($model->service_login)?></td>
                                    <td><?=Html::encode($model->service_parol)?></td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
            <?php Pjax::end() ?> 
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>