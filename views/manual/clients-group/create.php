<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\ClientsGroup */

?>
<div class="clients-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
