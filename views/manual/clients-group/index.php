<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы клиентов';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Группы клиентов</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns.php'),
                        'panelBeforeTemplate' => Html::a('Создать', ['create'],
                                ['role' => 'modal-remote', 'title'=> 'Создать','class'=>'btn btn-primary']).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],

                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
