<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Cashbox */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cashbox-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
    	<div class="col-md-5">
    		<?= $form->field($model, 'atelier_id')->dropDownList($model->getAtelierList(), []) ?>
    	</div>
    	<div class="col-md-5">
    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>    		
    	</div>
    	<div class="col-md-2">
    		<?= $form->field($model, 'online_cash')->dropDownList($model->getTypeList(), []) ?>
    	</div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
