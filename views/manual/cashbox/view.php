<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Cashbox */
?>
<div class="cashbox-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'atelier_id',
                'value' => function($data){
                    return $data->atelier->name;
                }
            ],
            [
                'attribute' => 'online_cash',
                'value' => function($data){
                    return $data->getTypeDescription();
                }
            ],
        ],
    ]) ?>

</div>
