<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Marking */

?>
<div class="marking-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
