<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Marking */
?>
<div class="marking-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
