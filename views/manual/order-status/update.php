<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\OrderStatus */
?>
<div class="order-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
