<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JobOrderStatus */
?>
<div class="job-order-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'job_list_id',
            'order_status_id',
            'user_id',
            'created_at',
        ],
    ]) ?>

</div>
