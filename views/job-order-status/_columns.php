<?php
use yii\helpers\Url;

return [
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_status_id',
//        'value' => 'orderStatus.name',
        'content' => function($model){
    
            if($model->orderStatus == null){
                return null;
            }
    
            $name = $model->orderStatus->name;
    
            if($name == 'Новый'){
                $name = '<div class="chip chip-success">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'На записи'){
                $name = '<div class="chip chip-info">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'В работе'){
                $name = '<div class="chip chip-yellow">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'На проверке'){
                $name = '<div class="chip chip-warning">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'Выполнено'){
                $name = '<div class="chip chip-purple">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'На выдачи'){
                $name = '<div class="chip chip-primary">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'Отмененный'){
                $name = '<div class="chip chip-black">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            } else if($name == 'Закрытые'){
                $name = '<div class="chip chip-red">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$name.'</div>
                                  </div>
                                </div>';
            }

            return $name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.name'
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'format' => ['date', 'php:H:i d.m.Y'],
     ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],

];   