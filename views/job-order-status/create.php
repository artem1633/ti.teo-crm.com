<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JobOrderStatus */

?>
<div class="job-order-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
