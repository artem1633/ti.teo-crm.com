<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobOrderStatus */
?>
<div class="job-order-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
