<?php

use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JobList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'master_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Users::find()->where(['role_id' => Users::USER_ROLE_MASTER])->all(), 'id', 'name'),
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
