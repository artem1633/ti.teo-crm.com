<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JobList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Job Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'job_id',
            'type',
            'count',
            'price',
            'material_pay',
            'master_id',
            'prepaid',
            'description:ntext',
        ],
    ]) ?>

</div>
