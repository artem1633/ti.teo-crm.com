<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderAct */

$this->title = 'Create Order Act';
$this->params['breadcrumbs'][] = ['label' => 'Order Acts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-act-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
