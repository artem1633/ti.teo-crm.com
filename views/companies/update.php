<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
