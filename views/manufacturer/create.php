<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Suppliers */

?>
<div class="suppliers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
