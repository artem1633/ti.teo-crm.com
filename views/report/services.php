<?php

use yii\grid\GridView;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $loansDataProvider \yii\data\ArrayDataProvider */
/** @var $mastersDataProvider \yii\data\ArrayDataProvider */
/** @var $jobsDataProvider \yii\data\ArrayDataProvider */
/** @var $dateStart string */
/** @var $dateEnd string */

$this->title = 'Отчет';

?>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php $form = ActiveForm::begin(['id' => 'report-search-form', 'method' => 'GET']) ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Дата "С"</label>
                                    <?= Html::input('date', 'dateStart', $dateStart, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Дата "По"</label>
                                    <?= Html::input('date', 'dateEnd', $dateEnd, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Отчет по услугам</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?= GridView::widget([
                            'dataProvider' => $jobsDataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'attribute' => 'job_name',
                                    'label' => 'Наименование услуги'
                                ],
                                [
                                    'attribute' => 'count',
                                    'label' => 'Количество'
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>




<?php

$script = <<< JS

$("#report-search-form input").change(function(){
    $("#report-search-form").submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>