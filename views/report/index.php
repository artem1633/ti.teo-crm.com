<?php

use yii\helpers\Html;

/** @var $this \yii\web\View */

$this->title = 'Отчет';

?>


<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="text-primary feather icon-bar-chart" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-md-10">
                            <p style="font-size: 18px; margin-bottom: 7px;">Отчет по не оплаченным счетам (по должникам)</p>
                            <p>Описание отчета</p>
                            <p>
                                <?= Html::a('<i class="feather icon-printer"></i> Показать', ['report/credits'], ['class' => 'btn btn-primary pull-right']) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="text-primary feather icon-bar-chart" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-md-10">
                            <p style="font-size: 18px; margin-bottom: 7px;">Отчет по мастерам</p>
                            <p>Описание отчета</p>
                            <p>
                                <?= Html::a('<i class="feather icon-printer"></i> Показать', ['report/masters'], ['class' => 'btn btn-primary pull-right']) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="text-primary feather icon-bar-chart" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-md-10">
                            <p style="font-size: 18px; margin-bottom: 7px;">Отчет по услугам</p>
                            <p>Описание отчета</p>
                            <p>
                                <?= Html::a('<i class="feather icon-printer"></i> Показать', ['report/services'], ['class' => 'btn btn-primary pull-right']) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
