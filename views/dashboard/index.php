<?php

use yii\helpers\Html;

?>

<div class="card">
    <div class="card-header">
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <span class="text-success" style="font-size: 20px; font-weight: 600;">609 Открыто</span>
                    <span class="text-danger" style="margin-left: 10px; font-size: 20px; font-weight: 600;">571 Просрочено</span>
                    <span style="margin-left: 10px; font-size: 20px; font-weight: 600;">5 Скрыто</span>
                </div>
                <div class="col-md-3">
                    <?= Html::dropDownList('test', 0, [
                        0 => 'За 7 дней',
                        1 => 'За месяц'
                    ], ['class' => 'form-control pull-right']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Пользователи</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div id="mixed-chart" style="min-height: 365px;">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header d-flex align-items-start pb-0">
                <div>
                    <h2 class="text-bold-700 mb-0">1990</h2>
                    <p>Новые</p>
                </div>
                <div class="avatar bg-rgba-primary p-50 m-0">
                    <div class="avatar-content">
                        <i class="feather icon-cpu text-primary font-medium-5"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header d-flex align-items-start pb-0">
                <div>
                    <h2 class="text-bold-700 mb-0">1990</h2>
                    <p>Закрытые</p>
                </div>
                <div class="avatar bg-rgba-primary p-50 m-0">
                    <div class="avatar-content">
                        <i class="feather icon-cpu text-primary font-medium-5"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header d-flex align-items-start pb-0">
                <div>
                    <h2 class="text-bold-700 mb-0">1990</h2>
                    <p>Другие</p>
                </div>
                <div class="avatar bg-rgba-primary p-50 m-0">
                    <div class="avatar-content">
                        <i class="feather icon-cpu text-primary font-medium-5"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Аналитика</h4>
            </div>
            <div class="card-content">
                <div class="card-body" style="position: relative;">
                    <div id="product-order-chart" class="mb-3" style="min-height: 350px;"><div id="apexchartsfd8yueo4" class="apexcharts-canvas apexchartsfd8yueo4 light" style="width: 526px; height: 350px;"><svg id="SvgjsSvg1768" width="526" height="350" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1770" class="apexcharts-inner apexcharts-graphical" transform="translate(130, 0)"><defs id="SvgjsDefs1769"><clipPath id="gridRectMaskfd8yueo4"><rect id="SvgjsRect1771" width="270" height="292" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMaskfd8yueo4"><rect id="SvgjsRect1772" width="270" height="292" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><linearGradient id="SvgjsLinearGradient1778" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1779" stop-opacity="1" stop-color="rgba(242,242,242,1)" offset="0"></stop><stop id="SvgjsStop1780" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop><stop id="SvgjsStop1781" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop></linearGradient><linearGradient id="SvgjsLinearGradient1784" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1785" stop-opacity="1" stop-color="rgba(242,242,242,1)" offset="0"></stop><stop id="SvgjsStop1786" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop><stop id="SvgjsStop1787" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop></linearGradient><linearGradient id="SvgjsLinearGradient1790" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1791" stop-opacity="1" stop-color="rgba(242,242,242,1)" offset="0"></stop><stop id="SvgjsStop1792" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop><stop id="SvgjsStop1793" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop></linearGradient><linearGradient id="SvgjsLinearGradient1801" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1802" stop-opacity="1" stop-color="rgba(115,103,240,1)" offset="0"></stop><stop id="SvgjsStop1803" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop><stop id="SvgjsStop1804" stop-opacity="1" stop-color="rgba(143,128,249,1)" offset="1"></stop></linearGradient><linearGradient id="SvgjsLinearGradient1807" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1808" stop-opacity="1" stop-color="rgba(255,159,67,1)" offset="0"></stop><stop id="SvgjsStop1809" stop-opacity="1" stop-color="rgba(255,192,133,1)" offset="1"></stop><stop id="SvgjsStop1810" stop-opacity="1" stop-color="rgba(255,192,133,1)" offset="1"></stop></linearGradient><linearGradient id="SvgjsLinearGradient1813" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1814" stop-opacity="1" stop-color="rgba(234,84,85,1)" offset="0"></stop><stop id="SvgjsStop1815" stop-opacity="1" stop-color="rgba(242,146,146,1)" offset="1"></stop><stop id="SvgjsStop1816" stop-opacity="1" stop-color="rgba(242,146,146,1)" offset="1"></stop></linearGradient><filter id="SvgjsFilter1982" filterUnits="userSpaceOnUse"><feComponentTransfer id="SvgjsFeComponentTransfer1983" result="SvgjsFeComponentTransfer1983Out" in="SourceGraphic"><feFuncR id="SvgjsFeFuncR1984" type="linear" slope="0.65"></feFuncR><feFuncG id="SvgjsFeFuncG1985" type="linear" slope="0.65"></feFuncG><feFuncB id="SvgjsFeFuncB1986" type="linear" slope="0.65"></feFuncB><feFuncA id="SvgjsFeFuncA1987" type="identity"></feFuncA></feComponentTransfer></filter></defs><g id="SvgjsG1774" class="apexcharts-radialbar"><g id="SvgjsG1775"><g id="SvgjsG1776" class="apexcharts-tracks"><g id="SvgjsG1777" class="apexcharts-radialbar-track apexcharts-track" rel="1"><path id="apexcharts-radialbarTrack-0" d="M 134 22 A 123 123 0 1 1 133.97853245030944 22.000001873397125" fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area" data:pathOrig="M 134 22 A 123 123 0 1 1 133.97853245030944 22.000001873397125"></path></g><g id="SvgjsG1783" class="apexcharts-radialbar-track apexcharts-track" rel="2"><path id="apexcharts-radialbarTrack-1" d="M 134 55 A 90 90 0 1 1 133.98429203681178 55.00000137077838" fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area" data:pathOrig="M 134 55 A 90 90 0 1 1 133.98429203681178 55.00000137077838"></path></g><g id="SvgjsG1789" class="apexcharts-radialbar-track apexcharts-track" rel="3"><path id="apexcharts-radialbarTrack-2" d="M 134 88 A 57 57 0 1 1 133.99005162331412 88.00000086815965" fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area" data:pathOrig="M 134 88 A 57 57 0 1 1 133.99005162331412 88.00000086815965"></path></g></g><g id="SvgjsG1795"><g id="SvgjsG1800" class="apexcharts-series apexcharts-radial-series" seriesName="Finished" rel="1" data:realIndex="0"><path id="SvgjsPath1805" d="M 134 22 A 123 123 0 1 1 17.020048495696116 183.00909030811854" fill="none" fill-opacity="0.85" stroke="url(#SvgjsLinearGradient1801)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area apexcharts-radialbar-slice-0" data:angle="252" data:value="70" index="0" j="0" data:pathOrig="M 134 22 A 123 123 0 1 1 17.020048495696116 183.00909030811854" selected="false"></path></g><g id="SvgjsG1806" class="apexcharts-series apexcharts-radial-series" seriesName="Pending" rel="2" data:realIndex="1"><path id="SvgjsPath1811" d="M 134 55 A 90 90 0 1 1 123.03175909353673 234.32915364771898" fill="none" fill-opacity="0.85" stroke="url(#SvgjsLinearGradient1807)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area apexcharts-radialbar-slice-1" data:angle="187" data:value="52" index="0" j="1" data:pathOrig="M 134 55 A 90 90 0 1 1 123.03175909353673 234.32915364771898" selected="false"></path></g><g id="SvgjsG1812" class="apexcharts-series apexcharts-radial-series" seriesName="Rejected" rel="3" data:realIndex="2"><path id="SvgjsPath1817" d="M 134 88 A 57 57 0 0 1 190.86115086480999 148.97611900341514" fill="none" fill-opacity="0.85" stroke="url(#SvgjsLinearGradient1813)" stroke-opacity="1" stroke-linecap="round" stroke-width="18" stroke-dasharray="0" class="apexcharts-radialbar-area apexcharts-radialbar-slice-2" data:angle="94" data:value="26" index="0" j="2" data:pathOrig="M 134 88 A 57 57 0 0 1 190.86115086480999 148.97611900341514" selected="true" filter="url(#SvgjsFilter1982)"></path></g><circle id="SvgjsCircle1796" r="43" cx="134" cy="145" class="apexcharts-radialbar-hollow" fill="transparent"></circle><g id="SvgjsG1797" class="apexcharts-datalabels-group" transform="translate(0, 0)" style="opacity: 1;"><text id="SvgjsText1798" font-family="Helvetica, Arial, sans-serif" x="134" y="145" text-anchor="middle" dominant-baseline="auto" font-size="18px" font-weight="regular" fill="#373d3f" class="apexcharts-datalabel-label" style="font-family: Helvetica, Arial, sans-serif; fill: rgb(115, 103, 240);">Total</text><text id="SvgjsText1799" font-family="Helvetica, Arial, sans-serif" x="134" y="177" text-anchor="middle" dominant-baseline="auto" font-size="16px" font-weight="regular" fill="#373d3f" class="apexcharts-datalabel-value" style="font-family: Helvetica, Arial, sans-serif;">42459</text></g></g></g></g><line id="SvgjsLine1818" x1="0" y1="0" x2="268" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1819" x1="0" y1="0" x2="268" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line></g></svg><div class="apexcharts-legend"></div></div></div>
                    <div class="chart-info d-flex justify-content-between mb-1">
                        <div class="series-info d-flex align-items-center">
                            <i class="fa fa-circle-o text-bold-700 text-primary"></i>
                            <span class="text-bold-600 ml-50">Finished</span>
                        </div>
                        <div class="product-result">
                            <span>23043</span>
                        </div>
                    </div>
                    <div class="chart-info d-flex justify-content-between mb-1">
                        <div class="series-info d-flex align-items-center">
                            <i class="fa fa-circle-o text-bold-700 text-warning"></i>
                            <span class="text-bold-600 ml-50">Pending</span>
                        </div>
                        <div class="product-result">
                            <span>14658</span>
                        </div>
                    </div>
                    <div class="chart-info d-flex justify-content-between mb-75">
                        <div class="series-info d-flex align-items-center">
                            <i class="fa fa-circle-o text-bold-700 text-danger"></i>
                            <span class="text-bold-600 ml-50">Rejected</span>
                        </div>
                        <div class="product-result">
                            <span>4758</span>
                        </div>
                    </div>
                    <div class="resize-triggers"><div class="expand-trigger"><div style="width: 569px; height: 534px;"></div></div><div class="contract-trigger"></div></div></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Заявки</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="row avg-sessions pt-50">
                        <div class="col-12">
                            <p class="mb-0">Goal: $100000</p>
                            <div class="progress progress-bar-primary mt-25">
                                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-0">Goal: $100000</p>
                            <div class="progress progress-bar-primary mt-25">
                                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-0">Goal: $100000</p>
                            <div class="progress progress-bar-primary mt-25">
                                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-0">Goal: $100000</p>
                            <div class="progress progress-bar-primary mt-25">
                                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Рейтинг счастья</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div id="avg-session-chart" style="min-height: 200px;"><div id="apexcharts6ykh8gk3" class="apexcharts-canvas apexcharts6ykh8gk3 light" style="width: 398px; height: 200px;"><svg id="SvgjsSvg2377" width="398" height="200" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG2379" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs2378"><linearGradient id="SvgjsLinearGradient2381" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop2382" stop-opacity="0.4" stop-color="rgba(216,227,240,0.4)" offset="0"></stop><stop id="SvgjsStop2383" stop-opacity="0.5" stop-color="rgba(190,209,230,0.5)" offset="1"></stop><stop id="SvgjsStop2384" stop-opacity="0.5" stop-color="rgba(190,209,230,0.5)" offset="1"></stop></linearGradient><clipPath id="gridRectMask6ykh8gk3"><rect id="SvgjsRect2386" width="398" height="200" x="0" y="0" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMask6ykh8gk3"><rect id="SvgjsRect2387" width="400" height="202" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><filter id="SvgjsFilter2607" filterUnits="userSpaceOnUse"><feComponentTransfer id="SvgjsFeComponentTransfer2608" result="SvgjsFeComponentTransfer2608Out" in="SourceGraphic"><feFuncR id="SvgjsFeFuncR2609" type="linear" slope="0.65"></feFuncR><feFuncG id="SvgjsFeFuncG2610" type="linear" slope="0.65"></feFuncG><feFuncB id="SvgjsFeFuncB2611" type="linear" slope="0.65"></feFuncB><feFuncA id="SvgjsFeFuncA2612" type="identity"></feFuncA></feComponentTransfer></filter></defs><rect id="SvgjsRect2385" width="26.11875" height="200" x="10.88123779296875" y="0" rx="0" ry="0" fill="url(#SvgjsLinearGradient2381)" opacity="1" stroke-width="0" stroke-dasharray="3" class="apexcharts-xcrosshairs" y2="200" filter="none" fill-opacity="0.9" x1="10.88123779296875" x2="10.88123779296875"></rect><g id="SvgjsG2399" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG2400" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG2403" class="apexcharts-grid"><line id="SvgjsLine2405" x1="0" y1="200" x2="398" y2="200" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine2404" x1="0" y1="1" x2="0" y2="200" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG2389" class="apexcharts-bar-series apexcharts-plot-series"><g id="SvgjsG2390" class="apexcharts-series" rel="1" seriesName="Sessions" data:realIndex="0"><path id="SvgjsPath2392" d="M 11.815625000000004 200L 11.815625000000004 144.0296875Q 24.875000000000004 130.9703125 37.934375 144.0296875L 37.934375 200L 11.815625000000004 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 11.815625000000004 200L 11.815625000000004 144.0296875Q 24.875000000000004 130.9703125 37.934375 144.0296875L 37.934375 200L 11.815625000000004 200" pathFrom="M 11.815625000000004 200L 11.815625000000004 200L 37.934375 200L 37.934375 200L 11.815625000000004 200" cy="137.5" cx="37.934375" j="0" val="75" barHeight="62.5" barWidth="26.11875"></path><path id="SvgjsPath2393" d="M 69.85729166666667 200L 69.85729166666667 102.36302083333332Q 82.91666666666667 89.30364583333332 95.97604166666667 102.36302083333332L 95.97604166666667 200L 69.85729166666667 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 69.85729166666667 200L 69.85729166666667 102.36302083333332Q 82.91666666666667 89.30364583333332 95.97604166666667 102.36302083333332L 95.97604166666667 200L 69.85729166666667 200" pathFrom="M 69.85729166666667 200L 69.85729166666667 200L 95.97604166666667 200L 95.97604166666667 200L 69.85729166666667 200" cy="95.83333333333333" cx="95.97604166666667" j="1" val="125" barHeight="104.16666666666667" barWidth="26.11875"></path><path id="SvgjsPath2394" d="M 127.89895833333334 200L 127.89895833333334 19.0296875Q 140.95833333333334 5.970312500000002 154.01770833333333 19.0296875L 154.01770833333333 200L 127.89895833333334 200" fill="rgba(115,103,240,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 127.89895833333334 200L 127.89895833333334 19.0296875Q 140.95833333333334 5.970312500000002 154.01770833333333 19.0296875L 154.01770833333333 200L 127.89895833333334 200" pathFrom="M 127.89895833333334 200L 127.89895833333334 200L 154.01770833333333 200L 154.01770833333333 200L 127.89895833333334 200" cy="12.5" cx="154.01770833333333" j="2" val="225" barHeight="187.5" barWidth="26.11875" selected="true" filter="url(#SvgjsFilter2607)"></path><path id="SvgjsPath2395" d="M 185.94062500000004 200L 185.94062500000004 60.69635416666666Q 199.00000000000003 47.63697916666666 212.05937500000005 60.69635416666666L 212.05937500000005 200L 185.94062500000004 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 185.94062500000004 200L 185.94062500000004 60.69635416666666Q 199.00000000000003 47.63697916666666 212.05937500000005 60.69635416666666L 212.05937500000005 200L 185.94062500000004 200" pathFrom="M 185.94062500000004 200L 185.94062500000004 200L 212.05937500000005 200L 212.05937500000005 200L 185.94062500000004 200" cy="54.16666666666666" cx="212.05937500000002" j="3" val="175" barHeight="145.83333333333334" barWidth="26.11875"></path><path id="SvgjsPath2396" d="M 243.9822916666667 200L 243.9822916666667 102.36302083333332Q 257.0416666666667 89.30364583333332 270.1010416666667 102.36302083333332L 270.1010416666667 200L 243.9822916666667 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 243.9822916666667 200L 243.9822916666667 102.36302083333332Q 257.0416666666667 89.30364583333332 270.1010416666667 102.36302083333332L 270.1010416666667 200L 243.9822916666667 200" pathFrom="M 243.9822916666667 200L 243.9822916666667 200L 270.1010416666667 200L 270.1010416666667 200L 243.9822916666667 200" cy="95.83333333333333" cx="270.1010416666667" j="4" val="125" barHeight="104.16666666666667" barWidth="26.11875"></path><path id="SvgjsPath2397" d="M 302.0239583333334 200L 302.0239583333334 144.0296875Q 315.08333333333337 130.9703125 328.14270833333336 144.0296875L 328.14270833333336 200L 302.0239583333334 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 302.0239583333334 200L 302.0239583333334 144.0296875Q 315.08333333333337 130.9703125 328.14270833333336 144.0296875L 328.14270833333336 200L 302.0239583333334 200" pathFrom="M 302.0239583333334 200L 302.0239583333334 200L 328.14270833333336 200L 328.14270833333336 200L 302.0239583333334 200" cy="137.5" cx="328.14270833333336" j="5" val="75" barHeight="62.5" barWidth="26.11875"></path><path id="SvgjsPath2398" d="M 360.065625 200L 360.065625 185.69635416666665Q 373.125 172.63697916666666 386.184375 185.69635416666665L 386.184375 200L 360.065625 200" fill="rgba(231,238,247,0.85)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMask6ykh8gk3)" pathTo="M 360.065625 200L 360.065625 185.69635416666665Q 373.125 172.63697916666666 386.184375 185.69635416666665L 386.184375 200L 360.065625 200" pathFrom="M 360.065625 200L 360.065625 200L 386.184375 200L 386.184375 200L 360.065625 200" cy="179.16666666666666" cx="386.184375" j="6" val="25" barHeight="20.833333333333336" barWidth="26.11875"></path><g id="SvgjsG2391" class="apexcharts-datalabels"></g></g></g><line id="SvgjsLine2406" x1="0" y1="0" x2="398" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine2407" x1="0" y1="0" x2="398" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG2408" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG2409" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG2410" class="apexcharts-point-annotations"></g></g><g id="SvgjsG2401" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)"><g id="SvgjsG2402" class="apexcharts-yaxis-texts-g"></g></g></svg><div class="apexcharts-legend"></div><div class="apexcharts-tooltip light" style="left: 23.9406px; top: 120px;"><div class="apexcharts-tooltip-series-group active" style="display: flex;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(231, 238, 247);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label">Sessions: </span><span class="apexcharts-tooltip-text-value">75</span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>