<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsAuto */
?>
<div class="clients-auto-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
