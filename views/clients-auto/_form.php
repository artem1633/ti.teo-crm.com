<?php
use app\models\manual\Marking;
use app\models\manual\Spot;
use app\models\manual\TypeClothes;
use kartik\color\ColorInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsAuto */
/* @var $form yii\widgets\ActiveForm */


$spotData = [];

if($model->marks_id != null){
    $spotData = Spot::find()->where(['marking_id' => $model->marks_id])->all();
}

?>

<style>
    .spectrum-group .spectrum-input {
        display: none;
    }
</style>

<div class="clients-auto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(TypeClothes::find()->all(), 'id', 'name')) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'marks_id')->dropDownList(ArrayHelper::map(Marking::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'spot_id')->dropDownList(ArrayHelper::map($spotData, 'id', 'name')) ?>
        </div>
    </div>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->widget(ColorInput::class, [
        'options' => ['placeholder' => 'Выберите цвет'],
    ]) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php


$script = <<< JS

$('#clientsauto-marks_id').change(function(){
    var id = $(this).val();

    $.get("/marking/spots?id="+id, function(response){
        $("#clientsauto-spot_id").html(response);
    });
});

JS;


$this->registerJs($script, \yii\web\View::POS_READY);


?>