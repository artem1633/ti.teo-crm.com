<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsAuto */
?>
<div class="clients-auto-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'type_id',
            'marks_id',
            'spot_id',
            'number',
            'passport',
            'color',
            'year',
        ],
    ]) ?>

</div>
