<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Orders */
$this->title = '';
?>
<div class="orders-create">
	<?php if( $model->step == 1) { ?>

	    <?= $this->render('_form_base', [
	        'model' => $model,
	    ]) ?>

	<?php } if( $model->step == 2) { ?>

		<?= $this->render('_form_additional', [
	        'model' => $model,
	    ]) ?>

	<?php } ?>
</div>
