<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\jui\AutoComplete;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use kartik\range\RangeInput;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/*\johnitvn\ajaxcrud\CrudAsset::register($this);*/

?>

<div class="box box-solid box-warning">    
        <div class="box-body">
            <div class="row">

                <?php Pjax::begin(['enablePushState' => false, 'id' => 'order-pjax']) ?>
                    <div class="col-md-12">
                        <h3>
                            Информация по заказу 
                            <?php if($model->status_id != 3){ ?>
                                <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['edit-order', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                            <?php } ?> 
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('status_id')?></b></td>
                                        <td><?=Html::encode($model->status->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('cost')?></b></td>
                                        <td><?=Html::encode($model->cost == null ? '0' : \Yii::$app->formatter->asDecimal($model->cost,0))?></td>
                                        <td><b><?=$model->getAttributeLabel('order_type')?></b></td>
                                        <td><?=Html::encode($model->getOrderTypeDescription())?></td>
                                        <td><b><?=$model->getAttributeLabel('pollution_id')?></b></td>
                                        <td><?=Html::encode($model->pollution->name)?></td>
                                    </tr>

                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('clothes_type')?></b></td>
                                        <td><?=Html::encode($model->clothesType->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('prepay')?></b></td>
                                        <td><?=Html::encode($model->prepay == null ? '0' : \Yii::$app->formatter->asDecimal($model->prepay,0))?></td>
                                        <td><b><?=$model->getAttributeLabel('spot_id')?></b></td>
                                        <td><?=Html::encode($model->spot->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('lost_product')?></b></td>
                                        <td><?=Html::encode($model->lost_product)?></td>
                                    </tr>

                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('user_id')?></b></td>
                                        <td><?=Html::encode($model->user->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('sale')?></b></td>
                                        <td><?=Html::encode($model->sale)?></td>
                                        <td><b><?=$model->getAttributeLabel('marketing_id')?></b></td>
                                        <td><?=Html::encode($model->marketing->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('issue_date')?></b></td>
                                        <td><?=($model->issue_date)?></td>
                                    </tr>

                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('marks_id')?></b></td>
                                        <td><?=Html::encode($model->marks->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('quick')?></b></td>
                                        <td><?=Html::encode($model->quick == 1 ? 'Да' : 'Нет')?></td>
                                        <td><b><?=$model->getAttributeLabel('creator_id')?></b></td>
                                        <td><?=Html::encode($model->creator->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('fact_date')?></b></td>
                                        <td><?=$model->fact_date?></td>
                                    </tr>

                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('comment')?></b></td>
                                        <td colspan="7"><?=Html::encode($model->comment)?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php Pjax::end() ?>


                <?php Pjax::begin(['enablePushState' => false, 'id' => 'detail-pjax']) ?>
                    <div class="col-md-7">
                        <h3>Детальность 
                            <?php if($model->status_id != 3){ ?>
                            <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['edit-detail', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('color')?></b></td>
                                        <td><?=Html::encode($model->color)?></td>
                                        <td><b><?=$model->getAttributeLabel('fabric_structure')?></b></td>
                                        <td><?=Html::encode($model->fabric_structure)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('hidden_defect')?></b></td>
                                        <td><?=Html::encode($model->hidden_defect == 1 ? 'Да' : 'Нет')?></td>
                                        <td><b><?=$model->getAttributeLabel('symbol')?></b></td>
                                        <td><?=Html::encode($model->symbol)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('product_warranty')?></b></td>
                                        <td><?=Html::encode($model->product_warranty)?></td>
                                        <td><b><?=$model->getAttributeLabel('complekt')?></b></td>
                                        <td><?=Html::encode($model->complekt)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('defect')?></b></td>
                                        <td><?=Html::encode($model->defect)?></td>
                                        <td><b><?=$model->getAttributeLabel('damage')?></b></td>
                                        <td><?=Html::encode($model->damage)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('spots_location')?></b></td>
                                        <td><?=Html::encode($model->spots_location)?></td>
                                        <td><b><?=$model->getAttributeLabel('wear')?></b></td>
                                        <td><?=Html::encode($model->wear)?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b><?=$model->getAttributeLabel('material')?></b></td>
                                        <td colspan="2"><?=Html::encode($model->material)?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php Pjax::end() ?>

                <?php Pjax::begin(['enablePushState' => false, 'id' => 'client-pjax']) ?>
                    <div class="col-md-5">
                        <h3>Информации по клиентам 
                            <?php if($model->status_id != 3){ ?>
                                <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['edit-client', 'id' => $model->client_id])?>"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('client_id')?></b></td>
                                        <td><?=Html::encode($model->client->fio)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('telephone')?></b></td>
                                        <td><?=Html::encode($model->telephone)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('client.group_id')?></b></td>
                                        <td><?=Html::encode($model->client->group->name)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('client.sale_for_product')?></b></td>
                                        <td><?=Html::encode($model->client->sale_for_product)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('client.sale_for_work')?></b></td>
                                        <td><?=Html::encode($model->client->sale_for_work)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('client.usd_ball')?></b></td>
                                        <td><?=Html::encode($model->client->usd_ball)?></td>
                                    </tr>
                                    <!-- <tr>
                                        <td><b><?php //$model->getAttributeLabel('client.email')?></b></td>
                                        <td><?php //Html::encode($model->client->email)?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?php //$model->getAttributeLabel('client.address')?></b></td>
                                        <td><?php //Html::encode($model->client->address)?></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php Pjax::end() ?>

            </div>
        </div>
</div>

<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; //console.log(type);
        $('#comp').hide();
        if(type == 2) $('#comp').show(); 
    }
);
JS
);
?>
<?php 
$this->registerJs(<<<JS
    $('#order_type').on('change', function() 
    {  
        var type = this.value;
        $('#polis').hide();
        if(type == 7) $('#polis').show(); 
    }
);
JS
);
?>