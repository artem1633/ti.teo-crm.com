<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */



if($model->isNewRecord){
    $model->acts = [
        [
            'name' => 'Ключ зажигания',
            'count' => 1,
        ],
        [
            'name' => 'Запасное колесо',
            'count' => 1,
        ],
        [
            'name' => 'Баллонный ключ',
            'count' => 1,
        ],
        [
            'name' => 'Аварийный знак',
            'count' => 1,
        ],
        [
            'name' => 'Аптечка',
            'count' => 1,
        ],
        [
            'name' => 'Огнетушитель',
            'count' => 1,
        ],
    ];
}

?>

<style>
</style>

<div class="orders-form">
    <?php $form = ActiveForm::begin(['id' => 'additional-form']); ?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Заказы</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'acts')->widget(\unclead\multipleinput\MultipleInput::class, [
                            'id' => 'acts_id',
                            'addButtonOptions' => [
                                'class' => 'btn btn-my-square btn-primary',
                                'label' => '+',
                            ],
                            'removeButtonOptions' => [
                                'class' => 'btn btn-my-square btn-danger',
                                'label' => 'x',
                            ],
                            'min' => 0,
                            'columns' => [
                                [
                                    'name' => 'id',
                                    'options' => [
                                        'type' => 'hidden'
                                    ]
                                ],

                                [
                                    'name'  => 'name',
                                    'title' => 'Наименование',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'input-priority'
                                    ]
                                ],

                                [
                                    'name'  => 'count',
                                    'title' => 'Кол-во',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'input-priority'
                                    ],
                                    'headerOptions' => [
                                        'style' => 'width: 100px;'
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-8">
                        <div class="canvas-wrapper">
                            <div class="cont">
                                <canvas id="imageTempAct" width="750" height="400"></canvas>
                                <div class="hidden">
                                    <?= $form->field($model, 'act_draw')->hiddenInput() ?>
                                </div>
<!--                            <div></div>-->
                            </div>
<!--                            <img src="https://a.d-cd.net/ksAAAgBMiOA-1920.jpg" alt="" style="width: 100%;">-->
<!--                             https://a.d-cd.net/ksAAAgBMiOA-1920.jpg-->
                        </div>
                        <?php if($model->isNewRecord == false): ?>
                            <?= Html::a('Очистить', '#', ['id' => 'clear-btn', 'class' => 'btn btn-primary']) ?>
                            <?= Html::a('Подписать', ['orders/signature', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?php endif; ?>
                        <?= Html::a('Очистить', '#', ['id' => 'clear-btn', 'class' => 'btn btn-warning']) ?>
                        <?= Html::submitButton('Назад', ['class' => 'btn btn-primary', 'onclick' => '$("[name=\"back\"]").val(1);']) ?>
                        <div class="hidden">
                            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?= Html::submitButton('Перейти к подписанию', ['id' => 'submit-frm-btn', 'class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <div style="display: none;">
                    <input type="hidden" name="redirect" value="0">
                    <input type="hidden" name="back" value="0">
                    <?= $form->field($model, 'step')->textInput()->label('') ?>
                    <?= $form->field($model, 'type')->textInput()->label('') ?>
                    <?= $form->field($model, 'complekt')->textInput()->label('') ?>
                    <?= $form->field($model, 'clientName')->textInput()->label('') ?>
                    <?= $form->field($model, 'atelier_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'client_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'telephone')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'order_type')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'user_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'clothes_type')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'marketing_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'issue_date')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'prepay')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'sale')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'color')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'lost_product')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'spot_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'comment')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'number')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'marks_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'pollution_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'pre_date')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'fabric_structure')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'jobs')->widget(\unclead\multipleinput\MultipleInput::class, [
//                        'id' => 'my_id',
                        'addButtonOptions' => [
                            'class' => 'btn btn-sm btn-primary',
                            'label' => '+',
                        ],
                        'removeButtonOptions' => [
                            'label' => 'x',
                        ],
                        'min' => 0,
                        'columns' => [
                            [
                                'name' => 'id',
                                'options' => [
                                    'type' => 'hidden'
                                ]
                            ],
                            [
                                'name'  => 'job_id',
                                'title' => 'Услуга',
                                'options' => [
                                    'class' => 'input-priority'
                                ],
                                'type'  => 'dropDownList',
                                'enableError' => true,
                                'items' => ArrayHelper::map(\app\models\manual\ListServices::find()->all(), 'id', 'name')
                            ],

                            [
                                'name'  => 'price',
                                'title' => 'Сумма',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'prepaid',
                                'title' => 'Аванс',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],


                            [
                                'name'  => 'material_pay',
                                'title' => 'Оплата за материал',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'master_id',
                                'title' => 'Мастер',
                                'type'  => 'dropDownList',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority',
                                    'prompt' => 'Выберите'
                                ],
                                'items' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name')
                            ],

                            [
                                'name'  => 'master_price',
                                'title' => 'Сумма Мастера',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'date',
                                'title' => 'Срок',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'master_prepaid',
                                'title' => 'Аванс Мастера',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],
                            [
                                'name'  => 'cost_credit',
                                'title' => 'Сумма расход',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'description',
                                'title' => 'Примечание',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>    
</div>

<?php

$script = <<< JS
 var canvas = document.getElementById("imageTempAct"), 
                context = canvas.getContext("2d");
                
            var img = new Image();
            img.src = "/images/act-image-dist.png";
            img.onload = function() {
                context.drawImage(img, 0, 0);
            };
           
            
            
var canvas = document.querySelector("#imageTempAct");

var signaturePad = new SignaturePad(canvas);

// Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible parameters)
signaturePad.toDataURL(); // save image as PNG
signaturePad.toDataURL("image/jpeg"); // save image as JPEG
signaturePad.toDataURL("image/svg+xml"); // save image as SVG

// Draws signature image from data URL.
// NOTE: This method does not populate internal data structure that represents drawn signature. Thus, after using #fromDataURL, #toData won't work properly.
signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

// Returns signature image as an array of point groups
var data = signaturePad.toData();

// Draws signature image from an array of point groups
signaturePad.fromData(data);

// Clears the canvas
signaturePad.clear();

signaturePad.penColor = 'red';

// Returns true if canvas is empty, otherwise returns false
signaturePad.isEmpty();

// Unbinds all event handlers
signaturePad.off();

// Rebinds all event handlers
signaturePad.on();


var cancelButton = document.getElementById('clear-btn');

cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
  
  
   var canvas = document.getElementById("imageTempAct"), 
                context = canvas.getContext("2d");
                
            var img = new Image();
            img.src = "/images/act-image-dist.png";
            img.onload = function() {
                context.drawImage(img, 0, 0);
            };
           
});


$('#submit-frm-btn').click(function(e){
    e.preventDefault();
    
    $('#submit-frm-btn').html('<i class="fa fa-spinner fa-spin"></i> Перейти к подписанию');
    
    $("[name=\"redirect\"]").val(1);
    
    var data = signaturePad.toDataURL('image/png');

// Send data to server instead...
//   window.open(data);

    console.log(data);
    
    var param = $('meta[name=csrf-param]').attr("content");
    var token = $('meta[name=csrf-token]').attr("content");
    
    $.ajax({
        url: "/orders/load-img",
        method: "POST",
        data: {
            'data': data,
            '_csrf': token,
        },
        success: function(response){
            console.log(response);
            $('#orders-act_draw').val(response.path);
            
            $('#additional-form').submit();
        }
    });
    
    
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);




?>
