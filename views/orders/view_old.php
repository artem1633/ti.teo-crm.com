<?php


use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = "Заказы";
$this->params['breadcrumbs'][] = ['label' => $label,'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Заказ</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div style="position:relative; left:0px;">
                    <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab-justified" data-toggle="tab" href="#home-just" role="tab" aria-controls="home-just" aria-selected="true">Информация</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab-justified" data-toggle="tab" href="#profile-just" role="tab" aria-controls="profile-just" aria-selected="true">Ремонт</a>
                        </li>
                    </ul>
                    <div class="tab-content pt-1">
                        <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                            <?= $contentOrder ?>
                        </div>
                        <div class="tab-pane" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-justified">
                            <?= $contentJob ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    //"size" => "large",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>