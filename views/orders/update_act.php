<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */


\app\assets\CanvasSignatureAsset::register($this);

if($model->isNewRecord){
    $model->acts = [
        [
            'name' => 'Ключ зажигания',
            'count' => 1,
        ],
        [
            'name' => 'Запасное колесо',
            'count' => 2,
        ],
        [
            'name' => 'Баллонный ключ',
            'count' => 1,
        ],
        [
            'name' => 'Аварийный знак',
            'count' => 0,
        ],
        [
            'name' => 'Аптечка',
            'count' => 1,
        ],
        [
            'name' => 'Огнетушитель',
            'count' => 0,
        ],
    ];
}

?>

<style>
    .cont { position: relative; }
    #imageViewAct { border: 1px solid #000; }
    #imageTempAct { position: absolute; top: 1px; left: 1px; }
</style>

<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Заказы</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'acts')->widget(\unclead\multipleinput\MultipleInput::class, [
                            'id' => 'my_id',
                            'addButtonOptions' => [
                                'class' => 'btn btn-my-square btn-primary',
                                'label' => '+',
                            ],
                            'removeButtonOptions' => [
                                'class' => 'btn btn-my-square btn-danger',
                                'label' => 'x',
                            ],
                            'min' => 0,
                            'columns' => [
                                [
                                    'name' => 'id',
                                    'options' => [
                                        'type' => 'hidden'
                                    ]
                                ],

                                [
                                    'name'  => 'name',
                                    'title' => 'Наименование',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'input-priority'
                                    ]
                                ],

                                [
                                    'name'  => 'count',
                                    'title' => 'Кол-во',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'input-priority'
                                    ],
                                    'headerOptions' => [
                                        'style' => 'width: 100px;'
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-8">
                        <div class="canvas-wrapper">
                            <div class="cont">
                                <div class="data-act-coords" style="display: none;"><?=$model->act_draw?></div>
                                <div class="hidden">
                                    <p><label>Drawing tool: <select id="dtool"><option value="pencil" selected>Pencil</option></select></label></p>
                                </div>
                                <canvas id="imageViewAct" width="750" height="400">
                                    <p>Unfortunately, your browser is currently unsupported by our web application. We are sorry for the inconvenience. Please use one of the supported browsers listed below, or draw the image you want using an offline tool.</p>
                                    <p>Supported browsers: <a href="http://www.opera.com">Opera</a>, <a href="http://www.mozilla.com">Firefox</a>, <a href="http://www.apple.com/safari">Safari</a>, and <a href="http://www.konqueror.org">Konqueror</a>.</p>
                                </canvas>
                                <canvas id="imageTempAct" width="750" height="400"></canvas>
                            </div>
                            <input name="act_draw" type="hidden" id="act-fld" value="<?=$model->act_draw?>">
                            <!--                            <img src="https://a.d-cd.net/ksAAAgBMiOA-1920.jpg" alt="" style="width: 100%;">-->
                            <!--                             https://a.d-cd.net/ksAAAgBMiOA-1920.jpg-->
                        </div>
                        <?php if($model->isNewRecord == false): ?>
                            <?= Html::a('Очистить', '#', ['id' => 'clear-btn', 'class' => 'btn btn-primary']) ?>
                            <?= Html::a('Подписать', ['orders/signature', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?php endif; ?>
                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <div style="display: none;">
                    <?= $form->field($model, 'step')->textInput()->label('') ?>
                    <?= $form->field($model, 'type')->textInput()->label('') ?>
                    <?= $form->field($model, 'clientName')->textInput()->label('') ?>
                    <?= $form->field($model, 'atelier_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'client_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'telephone')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'order_type')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'user_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'clothes_type')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'marketing_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'issue_date')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'prepay')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'sale')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'color')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'lost_product')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'spot_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'comment')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'number')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'marks_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'pollution_id')->hiddenInput()->label('') ?>
                    <?= $form->field($model, 'jobs')->widget(\unclead\multipleinput\MultipleInput::class, [
//                        'id' => 'my_id',
                        'addButtonOptions' => [
                            'class' => 'btn btn-my-square btn-primary',
                            'label' => '+',
                        ],
                        'removeButtonOptions' => [
                            'class' => 'btn btn-my-square btn-danger',
                            'label' => 'x',
                        ],
                        'min' => 0,
                        'columns' => [
                            [
                                'name' => 'id',
                                'options' => [
                                    'type' => 'hidden'
                                ]
                            ],
                            [
                                'name'  => 'job_id',
                                'title' => 'Услуга',
                                'options' => [
                                    'class' => 'input-priority'
                                ],
                                'type'  => 'dropDownList',
                                'enableError' => true,
                                'items' => ArrayHelper::map(\app\models\manual\ListServices::find()->all(), 'id', 'name')
                            ],

                            [
                                'name'  => 'price',
                                'title' => 'Сумма',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'prepaid',
                                'title' => 'Аванс',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],


                            [
                                'name'  => 'material_pay',
                                'title' => 'Оплата за материал',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'master_id',
                                'title' => 'Мастер',
                                'type'  => 'dropDownList',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ],
                                'items' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name')
                            ],

                            [
                                'name'  => 'master_price',
                                'title' => 'Сумма Мастера',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'master_prepaid',
                                'title' => 'Аванс Мастера',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],
                            [
                                'name'  => 'cost_credit',
                                'title' => 'Сумма расход',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],

                            [
                                'name'  => 'description',
                                'title' => 'Примечание',
                                'enableError' => true,
                                'options' => [
                                    'class' => 'input-priority'
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS
$(document).ready(function(){
     var canvas = document.getElementById("imageViewAct"), 
                context = canvas.getContext("2d");
            var img = new Image();
            img.src = "/images/act-image-dist.jpg";
            img.onload = function() {
                context.drawImage(img, 0, 0);
            };
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
