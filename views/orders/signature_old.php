<?php

use yii\helpers\Html;

/** @var $model \app\models\Orders */

\app\assets\CanvasSignatureAsset::register($this);

?>

<style>
    .cont { position: relative; }
    #imageViewClient { border: 1px solid #000; }
    #imageViewAdmin { border: 1px solid #000; }
    #imageTempClient { position: absolute; top: 36px; left: 1px; }
    #imageTempAdmin { position: absolute; top: 36px; left: 1px; }
</style>


<div class="card">
    <div class="card-header">
        <h4 class="card-title">Подпись</h4>
    </div>
    <div class="card-content">
        <div class="card-body">

            <?php $form = \yii\widgets\ActiveForm::begin() ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="cont">
                        <div class="data-client-coords" style="display: none;"><?= $model->status_id == 3 ? $model->signature_client_second : $model->signature_client ?></div>
                        <p>Роспись клиента</p>
                        <div class="hidden">
                            <p><label>Drawing tool: <select id="dtool"><option value="pencil" selected>Pencil</option></select></label></p>
                        </div>
                        <canvas id="imageViewClient" width="400" height="300">
                            <p>Unfortunately, your browser is currently unsupported by our web application. We are sorry for the inconvenience. Please use one of the supported browsers listed below, or draw the image you want using an offline tool.</p>
                            <p>Supported browsers: <a href="http://www.opera.com">Opera</a>, <a href="http://www.mozilla.com">Firefox</a>, <a href="http://www.apple.com/safari">Safari</a>, and <a href="http://www.konqueror.org">Konqueror</a>.</p>
                        </canvas>
                        <canvas id="imageTempClient" width="400" height="300"></canvas>
                    </div>
                    <input name="signature_client" type="hidden" id="signature-client-fld" value="<?= $model->status_id == 3 ? $model->signature_client_second : $model->signature_client ?>">
                </div>
                <div class="col-md-6">
                    <div class="cont">
                        <div class="data-admin-coords" style="display: none;"><?= $model->status_id == 3 ? $model->signature_admin_second : $model->signature_admin ?></div>
                        <p>Роспись администратора</p>
                        <div class="hidden">
                            <p><label>Drawing tool: <select id="dtool"><option value="pencil" selected>Pencil</option></select></label></p>
                        </div>
                        <canvas id="imageViewAdmin" width="400" height="300">
                            <p>Unfortunately, your browser is currently unsupported by our web application. We are sorry for the inconvenience. Please use one of the supported browsers listed below, or draw the image you want using an offline tool.</p>
                            <p>Supported browsers: <a href="http://www.opera.com">Opera</a>, <a href="http://www.mozilla.com">Firefox</a>, <a href="http://www.apple.com/safari">Safari</a>, and <a href="http://www.konqueror.org">Konqueror</a>.</p>
                        </canvas>
                        <canvas id="imageTempAdmin" width="400" height="300"></canvas>
                    </div>
                    <input name="signature_admin" type="hidden" id="signature-admin-fld" value="<?= $model->status_id == 3 ? $model->signature_admin_second : $model->signature_admin ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <?= Html::a('Очистить', '#', ['id' => 'clear-btn-client', 'class' => 'btn btn-primary']) ?>
                </div>
                <div class="col-md-3">

                </div>
                <div class="col-md-3 text-center">
                    <?= Html::a('Очистить', '#', ['id' => 'clear-btn-admin', 'class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?= Html::a('Назад', ['orders/update-additional', 'id' => $model->id], ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px;']) ?>
                    <?= Html::submitButton('Подписать', ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px;']) ?>
                </div>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>
