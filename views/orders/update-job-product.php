<?php
use app\models\manual\Product;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\manual\ListServices */
/* @var $form yii\widgets\ActiveForm */


if($model->isNewRecord == false){
//    $model->productList = ArrayHelper::getColumn(\app\models\JobListProduct::find()->where(['job_list_id' => $model->id])->all(), 'id');
    $model->productList = \app\models\JobListProduct::find()->where(['job_list_id' => $model->id])->asArray()->all();
}

$items = [];

/** @var Product[] $products */
$products = Product::find()->all();

foreach ($products as $product)
{
    $unitLabel = '';

    if($product->unit !== null){
        $unitLabel = ' ('.ArrayHelper::getValue(Product::unitLabels(), $product->unit).')';
    }

    $items[$product->id] = $product->name.$unitLabel;
}



?>

<div class="list-services-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'productList')->widget(\unclead\multipleinput\MultipleInput::class, [
                'id' => 'my_id',
                'addButtonOptions' => [
                    'class' => 'btn btn-sm btn-primary btn-my-square',
                    'label' => '+',
                ],
                'removeButtonOptions' => [
                    'class' => 'btn btn-sm btn-danger btn-my-square',
                    'label' => 'x',
                ],
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden'
                        ]
                    ],
                    [
                        'name'  => 'product_id',
                        'title' => 'Расходники',
                        'options' => [
                            'class' => 'input-priority',
                        ],
                        'headerOptions' => [
                            'style' => 'width: 80%;',
                        ],
                        'type'  => 'dropDownList',
                        'enableError' => true,
                        'items' => $items,
                    ],
                    [
                        'name'  => 'count',
                        'title' => 'Объем',
                        'enableError' => true,
                        'options' => [
                            'class' => 'input-priority',
                        ],
                        'headerOptions' => [
                            'style' => 'width: 20%;',
                        ]
                    ],
                ],
            ])->label(false); ?>
        </div>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Cоздать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
