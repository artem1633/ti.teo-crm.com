<?php
use app\models\JobList;
use app\models\manual\ListServices;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => 'Админ',
//        'attribute'=>'creator_id',
//        'value'=>'creator.name',
//        'contentOptions' => ['style' => 'font-size: 14px;'],
//        'width' => '10%',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fact_date',
        'label' => 'Дата',
//        'format' => ['date', 'php:d.m.Y H:i'],
        'content' => function($model){
            if($model->pre_date != null){
                $model->pre_date .= '07:00';
                return Yii::$app->formatter->asDate($model->pre_date, 'php:d.m.Y H:i');
            } else {
                return Yii::$app->formatter->asDate($model->fact_date, 'php:d.m.Y H:i');
            }
        },
        'contentOptions' => ['style' => 'font-size: 14px;'],
        'width' => '10%',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_id',
//        'content' => function($data){
//            return $data->status->name;
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'content' => function($data){
            $output = '';

            $output .= $data->client->fio.'<br>';
            $output .= ArrayHelper::getValue($data, 'marks.name').' '.ArrayHelper::getValue($data, 'spot.name').', '.$data->number;

            return $output;
        },
        'contentOptions' => ['style' => 'font-size: 14px;'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_id',
//        'value' => 'status.name',
//        'content' => function($model){
//            return Html::dropDownList('status_id_'.$model->id, $model->status_id, ArrayHelper::map(\app\models\manual\OrderStatus::find()->all(), 'id', 'name'), [
//                'class' => 'form-control',
//                'data-status' => $model->id,
//            ]);
//        },
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Услуги',
        'content' => function($model){
            $services = JobList::find()->where(['order_id' => $model->id])->orderBy('id asc')->all();
//            $serviceList = ArrayHelper::getColumn(ListServices::find()->where(['id' => $services])->orderBy('id asc')->all(), 'name');

            $servicesHtml = [];

            foreach ($services as $service)
            {
                $listService = ListServices::findOne($service->job_id);
                if($listService){
                    $servicesHtml[] = "<span style='display: inline-block; padding: 5px 0; height: 30px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; width: 300px;'>{$listService->name}</span>";
                }
            }

//            array_walk($serviceList, function(&$service){
//                $service = "<span style='display: inline-block; padding: 5px 0; height: 30px;'>{$service}</span>";
//            });

            return "<div style='font-size: 14px;'>".implode('<br>', $servicesHtml)."</div>";
        },
    ],
    [
        'attribute' => 'fabric_structure',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'content' => function($model){
            $output = '';
            $services = JobList::find()->select('job_list.id, users.name as master_name')->joinWith('master')->where(['order_id' => $model->id])->orderBy('job_list.id asc')->asArray()->all();

            foreach ($services as $service)
            {
                $output .= "<span style='display: inline-block; padding: 5px 0; height: 30px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; width: 100px;'>".($service['master_name'] != '' ? $service['master_name'] :
                    Html::a('Назначить мастера', ['job-list/apply-master', 'id' => $service['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['role' => 'modal-remote']))."</span>";
                $output .= '<br>';
            }

            return "<div style='font-size: 14px;'>{$output}</div>";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
        'label' => 'Сумма/Аванс',
        'content' => function($data){
//            $price = JobList::find()->where(['order_id' => $data->id])->sum('price');
//            $debt = JobList::find()->where(['order_id' => $data->id])->sum('prepaid');

            $output = '';

            $jobLists = JobList::find()->where(['order_id' => $data->id])->orderBy('id asc')->all();

            foreach ($jobLists as $jobList)
            {
                $output .= "<span style='display: inline-block; padding: 5px 0; height: 30px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; width: 100px;'>{$jobList->price}/{$jobList->prepaid}</span><br>";
            }

            return "<div style='font-size: 14px;'>{$output}</div>";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'label' => 'Статус ',
//        'content' => function($data){
//            return $data->status->name;
//        },
        'content' => function($model){
            $services = JobList::find()->select('job_list.id as id, order_status.name as status_name')->joinWith('status')->where(['order_id' => $model->id])->orderBy('job_list.id asc')->asArray()->all();

            $output = '';

            $allCompleted = count($services) == 0 ? false : true;
            $allPre = count($services) == 0 ? false : true;

            foreach ($services as $service)
            {
                if($service['status_name'] != 'На выдачи'){
                    $allCompleted = false;
                }
                if($service['status_name'] != 'На записи'){
                    $allPre = false;
                }
            }

            if($allCompleted)
            {
                $service = '<div class="chip chip-primary">
                                  <div class="chip-body">
                                    <div class="chip-text">На выдачи</div>
                                  </div>
                                </div>';

                $output .= Html::a($service, ['job-list/apply-status', 'id' => $services[0]['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['class' => 'service-label', 'role' => 'modal-remote'])."<br>";
                return $output;
            }

            if($allPre)
            {
                $service = '<div class="chip chip-info">
                                  <div class="chip-body">
                                    <div class="chip-text">На записи</div>
                                  </div>
                                </div>';

                $output .= Html::a($service, ['job-list/apply-status', 'id' => $services[0]['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['class' => 'service-label', 'role' => 'modal-remote'])."<br>";
                return $output;
            }

            if($model->status_id == 8)
            {
                $service = '<div class="chip chip-red">
                                  <div class="chip-body">
                                    <div class="chip-text">Закрытые</div>
                                  </div>
                                </div>';

                $output .= Html::a($service, ['job-list/apply-status', 'id' => $services[0]['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['class' => 'service-label', 'role' => 'modal-remote'])."<br>";
                return $output;
            }


            array_walk($services, function(&$service) use (&$output){

                $href = true;

                if($service['status_name'] == 'Новый'){
                    $service['status_name'] = '<div class="chip chip-success">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'На записи'){
                    $service['status_name'] = '<div class="chip chip-info">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'В работе'){
                    $service['status_name'] = '<div class="chip chip-yellow">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'На проверке'){
                    $service['status_name'] = '<div class="chip chip-warning">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'Выполнено'){
                    $service['status_name'] = '<div class="chip chip-purple">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'Отмененный'){
                    $service['status_name'] = '<div class="chip chip-black">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';

                    $href = false;
                } else if($service['status_name'] == 'На выдачи'){
                    $service['status_name'] = '<div class="chip chip-primary">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';
                } else if($service['status_name'] == 'Закрытые'){
                    $service['status_name'] = '<div class="chip chip-red">
                                  <div class="chip-body">
                                    <div class="chip-text">'.$service['status_name'].'</div>
                                  </div>
                                </div>';

                    $href = false;
                }

                if($href){
                    $output .= Html::a($service['status_name'] ? $service['status_name'] : 'Установить статус', ['job-list/apply-status', 'id' => $service['id'], 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['class' => 'service-label', 'role' => 'modal-remote'])."<br>";
                } else {
                    $output .= $service['status_name']."<br>";
                }
            });

            return $output;
        },
    ],
//    [
//        'attribute' => 'status_id',
//        'label' => 'Статус заявки',
//        'content' => function($model){
//            $str = null;
//
//            $isLink = true;
//
//            if(ArrayHelper::getValue($model,'status.name') == 'Новый'){
//                $str = '<div class="chip chip-success">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            } else if(ArrayHelper::getValue($model,'status.name') == 'На записи'){
//                $str = '<div class="chip chip-info">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            } else if(ArrayHelper::getValue($model,'status.name') == 'В работе'){
//                $str = '<div class="chip chip-yellow">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            } else if(ArrayHelper::getValue($model,'status.name') == 'На проверке'){
//                $str = '<div class="chip chip-warning">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            } else if(ArrayHelper::getValue($model,'status.name') == 'Выполненный'){
//                $isLink = false;
//                $str = '<div class="chip chip-purple">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            } else if(ArrayHelper::getValue($model,'status.name') == 'Отмененный'){
//                $isLink = false;
//                $str = '<div class="chip chip-black">
//                                  <div class="chip-body">
//                                    <div class="chip-text">'.ArrayHelper::getValue($model,'status.name').'</div>
//                                  </div>
//                                </div>';
//            }
//
//            if($isLink){
//                $str = Html::a($str ? $str : 'Установить статус', ['orders/apply-status', 'id' => $model->id, 'reloadPjaxContainer' => '#crud-datatable-pjax'], ['role' => 'modal-remote'])."<br>";
//            }
//
//            return $str;
//        }
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'acceptor_id',
//        'content' => function($data){
//            return $data->acceptor->name;
//        }
//    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{signature} {add-job} {leadView} {leadDelete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons'  => [
            'signature' => function ($url, $model)
            {
                if(Yii::$app->user->identity->can('update')) {
                    $jobListsCount = JobList::find()->where(['order_id' => $model->id])->count();
                    $jobListsCompleteCount = JobList::find()->where(['order_id' => $model->id, 'status_id' => 7])->count();
                    if($jobListsCount == $jobListsCompleteCount && $jobListsCount > 0){
                        $url = Url::to(['/orders/signature', 'id' => $model->id, 'job' => 'service']);
                        return Html::a('<i class="feather icon-edit"></i>', $url, ['data-pjax' => '0', 'title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
                    }
                }
            },
            'add-job' => function ($url, $model)
            {
                if(Yii::$app->user->identity->can('update')) {
                    $url = Url::to(['/orders/add-job', 'id' => $model->id, 'job' => 'service']);
                    return Html::a('<i class="feather icon-plus-square"></i>', $url, ['role' => 'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;',
                        'onclick' => '$("#ajaxCrudModal").addClass("modal-slg");',
                    ]);
                }
            },
            'leadView' => function ($url, $model)
            {
                if(Yii::$app->user->identity->can('view')) {
                    $url = Url::to(['/orders/view', 'id' => $model->id]);
                    return Html::a('<i class="feather icon-eye"></i>', $url, ['data-pjax' => 0,'title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
                }
            },
            'leadUpdate' => function ($url, $model)
            {
                $url = Url::to(['/orders/update', 'id' => $model->id]);
                return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;']);
            },
            'leadDelete' => function ($url, $model)
            {
                if(Yii::$app->user->identity->can('delete')){
                    $url = Url::to(['/orders/delete', 'id' => $model->id]);
                    return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                        'role'=>'modal-remote','title'=>'',
                        'style' => 'font-size: 23px;',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    ]);
                }
            },
        ]
    ]

];   