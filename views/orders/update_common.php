<?php
use app\models\manual\ListServices;
use app\models\manual\Marking;
use app\models\manual\Spot;
use app\models\Orders;
use app\models\User;
use app\models\Users;
use kartik\color\ColorInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->type = 0;
}

?>

<style>
    .list-cell__price {
        /*width: 9%;*/
        padding-right: 2px !important;
    }
    .list-cell__prepaid {
        /*width: 10%;*/
        padding-left: 0 !important;
    }
    .list-cell__master_price {
        /*width: 9%;*/
        padding-right: 2px !important;
    }
    .list-cell__master_prepaid {
        /*width: 10%;*/
        padding-left: 0 !important;
    }

    #orders-type label {
        font-size: 20px;
    }

    #orders-type label:first-child {
        margin-right: 20px;
        display: inline-block;
    }

    /*table.multiple-input-list {*/
        /*width: 75%;*/
    /*}*/

    .spectrum-group .spectrum-input {
        display: none;
    }

    .input-group-sp.input-group-addon.addon-text {
        height: 37px;
    }
</style>

<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Заказы</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div style="display: flex; align-items: flex-end;">
                    <div id="clientIdInput" style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'client_id')->widget(kartik\select2\Select2::classname(), [
                            'data' => $model->getClientsList(),
                            'options' => ['placeholder' => 'Выберите'],
                            'pluginEvents' => [
                                "change" => "function() 
                                    {
                                        var value = $(this).val();
                                        
                                        var type = $('#orders-type input:checked').val();
                                        
                                        if(type == 0){
                                            if(value === '' || value === null){
//                                                $('#clientNameRow').show();
    //                                            $('#orderTypeRow').removeClass('col-md-1');
    //                                            $('#orderTypeRow').addClass('col-md-2');
                                            } else {
//                                                $('#clientNameRow').hide();
    //                                            $('#orderTypeRow').removeClass('col-md-2');
    //                                            $('#orderTypeRow').addClass('col-md-1');
                                            }
                                        }
                                    
                                        $.get('set-phone',
                                        {
                                            'id':$(this).val()
                                        },
                                            function(data) { $('#phone').val(data); }
                                        );

                                        $.get('set-atelier',
                                        {
                                            'id':$(this).val()
                                        },
                                            function(data) { $('#atelier').val(data); }
                                        );
                                        
                                        
                                        $.get('/clients/view-ajax?id='+value, function(response){
                                            $('#orders-clientname').val(response.contact_person);
                                            $('#orders-complekt').val(response.company_name);
                                            $('#orders-telephone').val(response.telephone);
                                            $('#orders-number').val(response.car.number);
                                            $('#orders-comment').val(response.car.passport);
                                            $('#orders-order_type').val(response.car.year);
                                            $('#orders-clothes_type').val(response.car.type_id);
                                            $('#orders-color-cont .sp-preview-inner').attr('style', 'background-color: '+response.car.color+';');
                                            $('#orders-color-cont .sp-preview-inner').attr('class', 'sp-preview-inner');
                                            $('#orders-marks_id').val(parseInt(response.car.marks_id));
                                            $('#orders-marks_id').trigger('change');
                                            setTimeout(function(){
                                                $('#orders-spot_id').val(parseInt(response.car.spot_id));
                                                $('#orders-spot_id').trigger('change');
                                            }, 1000);
                                            $('#orders-color').val(response.car.color);
                                        });
                                        
                                        $.get('/clients/get-autos?id='+value, function(response){
                                            $('#orders-number').html(response);
                                        });
                                    }",
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                                'allowClear' => true,
                            ],
                        ]);
                        ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'telephone')->widget(MaskedInput::class, [
                            'mask' => '+7(999) 999-99-99',
                        ])->label('Телефон') ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'number')->widget(Select2::class,  [
                            'data' => $model->client_id != null ? ArrayHelper::map(\app\models\ClientsAuto::find()->where(['client_id' => $model->client_id])->all(), 'number', 'number') : [],
                            'pluginOptions' => [
                                'tags' => true,
                                'tokenSeparators' => [','],
                            ],
                            'pluginEvents' => [
                                'change' => "function(){
                                
                                        var clientId = $('#orders-client_id').val();
                                        var number = $(this).val();
                                
                                        $.get('/clients-auto/view-ajax?client_id='+clientId+'&number='+number, function(response){
                                            if(response != null){
                                                $('#orders-number').val(response.number);
                                                $('#orders-comment').val(response.passport);
                                                $('#orders-order_type').val(response.year);
                                                $('#orders-clothes_type').val(response.type_id);
                                                $('#orders-marks_id').val(parseInt(response.marks_id));
                                                $('#orders-marks_id').trigger('change');
                                                setTimeout(function(){
                                                    $('#orders-spot_id').val(parseInt(response.spot_id));
                                                    $('#orders-spot_id').trigger('change');
                                                }, 1000);
                                                $('#orders-color').val(response.color);
                                            }
                                        });
                                
                                }",
                            ],
                        ]) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div id="orderTypeRow" style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'order_type')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'fabric_structure')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div style="display: flex; align-items: flex-end;">
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'clothes_type')->dropDownList($model->getClothesTypeList(), [ 'prompt' => 'Выберите типа']) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'marks_id')->widget(Select2::class, [
                            'data' => ArrayHelper::map(Marking::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Выберите',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                            "pluginEvents" => [
                                'change' => 'function(){
                                    var id = $(this).val();
                                    
                                    $.get("/marking/spots?id="+id, function(response){
                                        $("#orders-spot_id").html(response);
                                    });
                                }',
                            ],
                        ]) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'spot_id')->widget(Select2::class, [
                            'data' => $model->marks_id != null ? ArrayHelper::map(Spot::find()->where(['marking_id' => $model->marks_id])->all(), 'id', 'name') : [],
                            'options' => [
                                'placeholder' => 'Выберите',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <?= $form->field($model, 'lost_product')->textInput() ?>
                    </div>
                    <div style="margin-right: 15px;">
                        <?= $form->field($model, 'color')->widget(ColorInput::class, [
                            'options' => ['placeholder' => 'Выберите цвет'],
                        ]) ?>
                    </div>
                    <div style="margin-right: 15px; min-width: 10%;">
                        <div style="width: 70%;">
                            <?= $form->field($model, 'pre_date')->input('date', ['class' => 'form-control model-date']) ?>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'jobs')->widget(\unclead\multipleinput\MultipleInput::class, [
                    'id' => 'my_id',
                    'addButtonOptions' => [
                        'class' => 'btn btn-my-square btn-primary',
                        'label' => '+',
                    ],
                    'removeButtonOptions' => [
                        'class' => 'btn btn-my-square btn-danger',
                        'label' => '-',
                    ],
                    'min' => 0,
                    'columns' => [
                        [
                            'name' => 'id',
                            'options' => [
                                'type' => 'hidden'
                            ]
                        ],
                        [
                            'name'  => 'job_id',
                            'title' => 'Вид услуги',
                            'options' => [
                                'class' => 'input-priority'
                            ],
//                                'headerOptions' => [
//                                    'style' => 'width: 230px;'
//                                ],
                            'type'  => 'dropDownList',
                            'enableError' => true,
                            'items' => ArrayHelper::map(ListServices::find()->orderBy('name asc')->all(), 'id', 'name')
                        ],

                        [
                            'name'  => 'price',
                            'title' => 'Оплата клиента',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Сумма',
                                'style' => "width: 70px;",
                            ],
                            'headerOptions' => [
                                'style' => 'width: 70px;'
                            ],
                        ],

                        [
                            'name'  => 'prepaid',
                            'title' => '',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Аванс',
                                'style' => "width: 70px;",
                            ],
                            'headerOptions' => [
                                'style' => 'width: 70px;'
                            ],
                        ],


//                            [
//                                'name'  => 'material_pay',
//                                'title' => 'Оплата за материал',
//                                'enableError' => true,
//                                'options' => [
//                                    'class' => 'input-priority'
//                                ]
//                            ],

                        [
                            'name'  => 'master_id',
                            'title' => 'Мастер',
                            'type'  => 'dropDownList',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'prompt' => 'Выберите'
                            ],
                            'items' => ArrayHelper::map(Users::find()->where(['role_id' => Users::USER_ROLE_MASTER])->all(), 'id', 'name')
                        ],

                        [
                            'name'  => 'master_price',
                            'title' => 'Оплата мастеру',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Сумма',
                                'style' => "width: 70px;",
                            ],
                            'headerOptions' => [
                                'style' => 'width: 70px;'
                            ],
                        ],

                        [
                            'name'  => 'master_prepaid',
                            'title' => '',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Аванс',
                                'style' => "width: 70px;",
                            ],
                            'headerOptions' => [
                                'style' => 'width: 70px;'
                            ],
                        ],
                        [
                            'name'  => 'cost_credit',
                            'title' => 'Оплата за расходник',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ],
                            'headerOptions' => [
                                'style' => 'width: 100px;'
                            ],
                        ],

                        [
                            'name'  => 'date',
                            'title' => 'Срок',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'type' => 'date',
                            ],
                            'headerOptions' => [
                                'style' => 'width: 130px;'
                            ],
                        ],

                        [
                            'name'  => 'description',
                            'title' => 'Примечание',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ],
                            'headerOptions' => [
                                'style' => 'width: 250px;'
                            ],
                        ],
                    ],
                ])->label(false); ?>

                <div style="display: none;">
                    <?= $form->field($model, 'step')->hiddenInput()->label('') ?>
                </div>
                <?php if(Yii::$app->request->isAjax == false): ?>
                    <?= Html::submitButton($model->pre_date != null ? 'Создать' : 'Сохранить', ['id' => 'btn-form-create', 'class' => 'btn btn-success btn-xs']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS

$('#orders-type input').change(function(){
    var value = $(this).val();
    
    if(value == 0){
        $('#clientCompanyRow').hide();
        $.get('/clients/get-name-list', function(response){
            $('#orders-client_id').html(response);
            $('#orders-client_id').parent().find('label').text('Клиент');
            $('#orders-client_id').trigger('change');
        });
    } else if(value == 1){
        $('#clientCompanyRow').show();
        $.get('/clients/get-company-list', function(response){
            $('#orders-client_id').html(response);
            $('#orders-client_id').parent().find('label').text('Компания');
            $('#orders-client_id').trigger('change');
        });
    }
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
