<?php
use kop\y2sp\ScrollPager;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);


?>

<style>

    .service-label {
        margin-bottom: 4px;
        display: inline-block;
    }

    #crud-datatable-togdata-page {
        display: none;
    }

    .btn-toolbar.kv-grid-toolbar {
        display: none;
    }

</style>

<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET', 'options' => ['class' => 'form-inline']]) ?>

                    <?= $form->field($searchModel, 'id')->textInput(['placeholder' => 'ID', 'style' => 'margin-right: 10px; width: 50px;'])->label(false) ?>

                    <?= $form->field($searchModel, 'factDateStart')->input('date', ['style' => 'margin-right: 5px;', 'placeholder' => 'Выберите дату'])->label(false) ?>
                    <span>-</span>
                    <?= $form->field($searchModel, 'factDateEnd')->input('date', ['style' => 'margin-left: 5px; margin-right: 10px;', 'placeholder' => 'Выберите дату'])->label(false) ?>

                    <?= $form->field($searchModel, 'client_id')->textInput(['placeholder' => 'Искать'])->label(false) ?>

                    <?= Html::a('<i class="fa fa-times"></i>', '#', ['class' => 'btn btn-my-square btn-warning', 'onclick' => 'event.preventDefault(); $("#search-form input:not([type=\'hidden\'])").val(null); $("#search-form").submit();', 'style' => 'margin-left: 10px;']) ?>

                    <div class="hidden">
                        <input type="hidden" name="scrollTo" value="<?=$scrollTo?>">
                        <input type="hidden" name="l" value="<?=$l?>" onchange="$('#search-form').submit();">
                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
<!--                <div id="scroll-wrapper" style="height: 60vh; overflow: auto;">-->
                <div id="scroll-wrapper" style="height: 75vh; overflow: auto;">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'options' => ['class' => 'theme-table grid-view hide-resize'],
                        'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns.php'),
//                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['create'],
//                            ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 25px','class'=>'btn btn save']).'&nbsp;'.
//                        Html::a('<i class="fa fa-repeat"></i>', [''],
//                            ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px','class'=>'btn btn save', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>'',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$specificJs = null;
if($scrollTo != null){
    $specificJs = '$(\'#scroll-wrapper\').scrollTop('.$scrollTo.');';
}

$scrollJs = null;
if($enableScroll){
    $scrollJs = '
$("#scroll-wrapper").scroll(function(ev) {
    if ((ev.currentTarget.scrollTop + 2 ) >= (ev.currentTarget.scrollHeight - ev.currentTarget.clientHeight)) {
        var limit = $(\'[name="l"]\').val() * 2;
        $(\'[name="scrollTo"]\').val(ev.currentTarget.scrollHeight - ev.currentTarget.clientHeight);
        $(\'[name="l"]\').val(limit);
        $(\'[name="l"]\').trigger(\'change\');
    }
});
    ';
}

$script = <<< JS

{$specificJs}


$('[data-status]').change(function(){
    var id = $(this).data('status');
    var value = $(this).val();

    $.get('/orders/change-status?id='+id+'&value='+value, function(response){
        
    });
});

$('#ajaxCrudModal').on('hidden.bs.modal', function () {
    if($(this).hasClass("modal-slg")){
        $(this).removeClass("modal-slg");        
    }
});

$('#search-form input').change(function(){
    $('#search-form').submit();
});



{$scrollJs}



var socket = io('http://demo-sklad.loc:3002');

socket.on('update', function(data){
    $.pjax.reload('#crud-datatable-pjax');
});


JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>

