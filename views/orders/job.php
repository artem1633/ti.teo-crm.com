<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use app\models\manual\ListServices;
use app\models\manual\Product;

?>
<div class="job-list-index">

        <?php if($visualButton){?>
			<div class="row">
				<div class="col-md-2">
					<h2>
						<?=Html::encode($title)?>
					</h2>
				</div>
				<div class="col-md-2">
					<h2>
						<?=Html::activeDropDownList(
							$model, 
							'status_id', 
							$statusOrder, 
							[
								'prompt' => 'Определить статус','class' => 'form-control',
								'onchange' =>'window.location.href = "/orders/new-status?idOrder='.$id.'&newSum='.$sum.'&status_id="+$(this).val();'
							]
						)?>
					</h2>
				</div>
			</div>
		<br/>
		<?php } ?>	
		<div class="row">
			<?php	if($visualButton){ ?>
				<div class="col-xs-6 col-sm-2">
					<?=Html::a('Добавить услугу', ['/orders/add-job', 'id'=>$id, 'job' => 'service'],['role'=>'modal-remote','title'=> 'Добавить услугу','class'=>'btn btn-success'])?>
				</div>	

				<div class="col-xs-6 col-sm-2">
					<?=Html::a('Добавить товар', ['/orders/add-job', 'id'=>$id, 'job' => 'parts'],['role'=>'modal-remote','title'=> 'Добавить товар','class'=>'btn btn-success'])?>
				</div>
			<?php }	 ?>

				<div class="col-xs-6 col-sm-2">
					<?=ButtonDropdown::widget([
						'label' => 'Печатать',
						'options' => [
							'target'=>'_blank',
							'class' => 'btn-info',
							'style' => 'margin:2px',
							],
							'dropdown' => [
							'items' => [
								[
									'label' => 'БСО',
									'url' => ['print-bso','id' => $id],
									'linkOptions' => ['target'=>'_blank'],
								],
								[
									'label' => 'Квитанции-договора',
									'url' =>['print-contract','id' => $id],
									'linkOptions' => ['target'=>'_blank'],
								],
								/*[
									'label' => 'Форму заказа',
									'url' =>['print-order','id' => $id],
									'linkOptions' => ['target'=>'_blank'],
								],*/
							]
						]	
					])?>
				</div>
			 
		</div>
		<br/>
			
		<?php
			if($visualButton && $dataProvider->getCount() > 0){	?>
			<div class="row">
				<div class="col-xs-6 col-sm-2">
					<?=Html::a(' Завершить заказ', [ 'new-status', 'idOrder'=>$id , 'newSum' =>$sum, 'status_id' => 3, ], ['class' => 'btn btn-danger',])?>
				</div>			
		 	</div>	
		 <?php } ?>	
	
	<?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
		<?= GridView::widget([
	        'dataProvider' => $dataProvider,
	      	'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],

				[
					'attribute'=>'id',
					'content'=>function ($data){
						return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['/orders/update-job-list','id'=>$data->order_id, 'job' => $data->type, 'job_id' => $data->id], ['style'=>'color:#ffffff;', 'role'=>'modal-remote',]).'</span>';
					},
				],
	            //'order_id',
				[
					'attribute' => 'job_name',				
					'label' => 'Работа',	
					'value'=>'job.name', 
					'content'=>function ($data){   
						if($data->type == 'service'){
							$service = ListServices::findOne($data->job_id);
							return $service->name;
						}  
						else { 
							$product = Product::findOne($data->job_id);
							return $product->name;
						}
				 	},
				],
	            'count',
	            'price',
	            [
	            	'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons' => [
		               'delete' => function ($url, $model, $key){ 
						if($model->order->status_id != 3){
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
								['/orders/job-list-delete','id'=>$key, 'order_id' => $model->order_id],
								[
									'title'=>'Удалить',
									'aria-label'=>'Удалить',
									'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
									'data-method'=>'post',
								]
							);
						}
		              }, 
						
		            ],
				],
	        ],
	    ]); ?>	
	<h3><?php echo Html::encode('Итого : '.\Yii::$app->formatter->asDecimal($sum,0)  . "   (Сумма  с учетом скидки и предоплаты)"); ?></h3>
	
	<?php Pjax::end(); ?>
									
</div>