<?php

use app\models\JobOrderStatusSearch;
use app\models\OrderAct;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $tab string */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\johnitvn\ajaxcrud\CrudAsset::register($this);

//\app\assets\CanvasSignatureAsset::register($this);

$model->acts = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $model->id])->all(), 'id');
$model->acts = OrderAct::find()->where(['order_id' => $model->id])->asArray()->all();

?>
<style>
    .cont { position: relative; }
    #imageViewAct { border: 1px solid #000; }
    #imageTempAct { position: absolute; top: 1px; left: 1px; }
    #imageViewClient { border: 1px solid #000; }
    #imageViewAdmin { border: 1px solid #000; }
    #imageTempClient { position: absolute; top: 36px; left: 1px; }
    #imageTempAdmin { position: absolute; top: 36px; left: 1px; }
    #imageViewClientSecond { border: 1px solid #000; }
    #imageViewAdminSecond { border: 1px solid #000; }
    #imageTempClientSecond { position: absolute; top: 36px; left: 1px; }
    #imageTempAdminSecond { position: absolute; top: 36px; left: 1px; }

</style>

<div class="orders-view">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item">
                                <a class="nav-link<?= $tab == 'home-fill' ? ' active' : '' ?>" id="home-tab-fill" href="<?= Url::toRoute(['orders/view', 'id' => $model->id, 'tab' => 'home-fill']) ?>" aria-controls="home-fill" aria-selected="true"><i class="fa fa-list"></i> Основное</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link<?= $tab == 'act-fill' ? ' active' : '' ?>" id="act-tab-fill" href="<?= Url::toRoute(['orders/view', 'id' => $model->id, 'tab' => 'act-fill']) ?>" aria-controls="act-fill" aria-selected="true"><i class="fa fa-file"></i> Акт приема-передач</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link<?= $tab == 'signature-fill' ? ' active' : '' ?>" id="act-tab-fill" href="<?= Url::toRoute(['orders/view', 'id' => $model->id, 'tab' => 'signature-fill']) ?>" aria-controls="signature-fill" aria-selected="true"><i class="fa fa-pencil"></i> Подписи сторон</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link<?= $tab == 'history-fill' ? ' active' : '' ?>" id="act-tab-fill" href="<?= Url::toRoute(['orders/view', 'id' => $model->id, 'tab' => 'history-fill']) ?>" aria-controls="history-fill" aria-selected="true"><i class="fa fa-clock-o"></i> История статусов</a>
                            </li>
                        </ul>
                        <div class="tab-content pt-1">
                            <div class="tab-pane<?= $tab == 'home-fill' ? ' active' : '' ?>" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                                <?php \yii\widgets\Pjax::begin(['id' => 'detail-pjax', 'enablePushState' => false]) ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 style="margin-bottom: 15px;"><?= ($model->type == \app\models\Orders::TYPE_PERSON ? 'Физическое лицо' : 'Юридическое лицо') ?></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= DetailView::widget([
                                            'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                            'model' => $model,
                                            'attributes' => [
                                                [
                                                    'attribute' => 'client_id',
                                                    'label' => 'ФИО',
                                                    'value' => function($model){
                                                        return ArrayHelper::getValue($model, 'client.fio');
                                                    }
                                                ],
                                                'telephone',
                                                [
                                                    'attribute' => 'clothes_type',
                                                    'value' => function($data){
                                                        if($data->clothes_type != null){
                                                            $clotheType = \app\models\manual\TypeClothes::findOne($data->clothes_type);
                                                            if($clotheType){
                                                                return $clotheType->name;
                                                            }
                                                        }
                                                    }
                                                ],
                                                'comment:ntext',
                                                [
                                                    'attribute' => 'marks_id',
                                                    'value' => function($model){
                                                        return ArrayHelper::getValue($model, 'marks.name');
                                                    }
                                                ],
                                                [
                                                    'attribute' => 'spot_id',
                                                    'value' => function($model){
                                                        return ArrayHelper::getValue($model, 'spot.name');
                                                    }
                                                ],
                                            ],
                                        ]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= DetailView::widget([
                                            'options' => ['class' => 'view-info', 'style' => 'width: 100%;'],
                                            'model' => $model,
                                            'attributes' => [
                                                'order_type',
                                                'fabric_structure',
                                                'lost_product',
                                                [
                                                    'attribute' => 'color',
                                                    'format' => 'raw',
                                                    'value' => function($model){
                                                        return '<div style="height: 20px; width: 40px; border-radius: 1em; background: '.$model->color.';"></div>';
                                                    }
                                                ],
                                                'number',
                                                [
                                                    'attribute' => 'pre_date',
                                                    'value' => function($model){
                                                        if($model->pre_date != null){
                                                            $model->pre_date .= '07:00';
                                                            return Yii::$app->formatter->asDate($model->pre_date, 'php:d.m.Y H:i');
                                                        } else {
                                                            return Yii::$app->formatter->asDate($model->fact_date, 'php:d.m.Y H:i');
                                                        }
                                                    }
                                                ],

                                            ],
                                        ]) ?>
                                    </div>
                                </div>

                                <hr>
                                <h3>Услуги</h3>

                                <?= GridView::widget([
                                    'dataProvider' => $jobListDataProvider,
//                            'filterModel' => $jobListSearchModel,
                                    'summaryOptions' => ['style' => 'display: none;'],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        [
                                            'attribute' => 'job_id',
                                            'value' => 'job.name',
                                            'label' => 'Вид услуги'
                                        ],
                                        [
                                            'attribute' => 'price',
                                            'label' => "Оплата клиента",
                                            'content' => function($model){
                                                return "{$model->price}/{$model->prepaid}";
                                            }
                                        ],
                                        [
                                            'attribute' => 'master_id',
                                            'value' => 'master.name',
                                        ],
                                        [
                                            'attribute' => 'price',
                                            'label' => "Оплата мастеру",
                                            'content' => function($model){
                                                return "{$model->master_price}/{$model->master_prepaid}";
                                            }
                                        ],
                                        [
                                            'attribute' => 'cost_credit',
                                            'label' => 'Оплата за расходник'
                                        ],
                                        [
                                            'attribute' => 'date',
                                            'format' => ['date', 'php:d.m.Y'],
                                        ],
                                        [
                                            'attribute' => 'description',
                                            'label' => 'Примечание'
                                        ],

                                        [
                                            'class' => 'kartik\grid\ActionColumn',
                                            'dropdown' => false,
                                            'vAlign'=>'middle',
                                            'urlCreator' => function($action, $model, $key, $index) {
                                                return Url::to([$action,'id'=>$key]);
                                            },
                                            'template' => '{update-job-product}',
                                            'buttons' => [
                                                'update-job-product' => function($url, $model){
                                                    return Html::a('<i class="feather icon-package"></i>', $url, [
                                                        'role'=>'modal-remote','title'=>'Изменить кол-во расходников', 'data-toggle'=>'tooltip', 'style' => 'font-size: 23px;',
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php if(Yii::$app->user->identity->can('update')): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>
                                                <?= Html::a('<i class="feather icon-edit"></i> Изменить', ['orders/update-common', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => '0']) ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php Pjax::end() ?>
                            </div>
                            <div class="tab-pane<?= $tab == 'act-fill' ? ' active' : '' ?>" id="act-fill" role="tabpanel" aria-labelledby="act-tab-fill">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?php $form = ActiveForm::begin(); ?>
                                        <?= $form->field($model, 'acts')->widget(\unclead\multipleinput\MultipleInput::class, [
                                            'id' => 'acts_id',
                                            'addButtonOptions' => [
                                                'class' => 'btn btn-sm btn-primary hidden',
                                                'label' => '+',
                                            ],
                                            'removeButtonOptions' => [
                                                'label' => 'x',
                                                'class' => 'hidden',
                                            ],
                                            'min' => 0,
                                            'columns' => [
                                                [
                                                    'name' => 'id',
                                                    'options' => [
                                                        'type' => 'hidden',
                                                        'disabled' => true,
                                                    ]
                                                ],

                                                [
                                                    'name'  => 'name',
                                                    'title' => 'Наименование',
                                                    'enableError' => true,
                                                    'options' => [
                                                        'class' => 'input-priority',
                                                        'disabled' => true,
                                                    ]
                                                ],

                                                [
                                                    'name'  => 'count',
                                                    'title' => 'Кол-во',
                                                    'enableError' => true,
                                                    'options' => [
                                                        'class' => 'input-priority',
                                                        'disabled' => true,
                                                    ],
                                                    'headerOptions' => [
                                                        'style' => 'width: 100px;'
                                                    ],
                                                ],
                                            ],
                                        ])->label(false); ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="canvas-wrapper">
                                            <div class="cont">
                                                <img src="/<?=$model->act_draw?>" style="width: 750px; height: 400px; border: 1px solid #000;">
                                            </div>
                                            <input name="act_draw" type="hidden" id="act-fld" value="<?=$model->act_draw?>">
                                            <!--                            <img src="https://a.d-cd.net/ksAAAgBMiOA-1920.jpg" alt="" style="width: 100%;">-->
                                            <!--                             https://a.d-cd.net/ksAAAgBMiOA-1920.jpg-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="hidden">
                                                    <?= Html::a('<i class="feather icon-edit"></i> Изменить', ['orders/update-act', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => '0']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane<?= $tab == 'signature-fill' ? ' active' : '' ?>" id="signature-fill" role="tabpanel" aria-labelledby="signature-tab-fill">
                                <?php $form = \yii\widgets\ActiveForm::begin() ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-success">Прием</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="cont">
                                            <p>Роспись клиента</p>
                                            <img src="/<?=$model->signature_client?>" style="width: 400px; height: 300px; border: 1px solid #000;">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="cont">
                                            <p>Роспись администратора</p>
                                            <img src="/<?=$model->signature_admin?>" style="width: 400px; height: 300px; border: 1px solid #000;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <h4 class="text-danger">Выдача</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="cont">
                                            <p>Роспись клиента</p>
                                            <img src="/<?=$model->signature_client_second?>" style="width: 400px; height: 300px; border: 1px solid #000;">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="cont">
                                            <p>Роспись администратора</p>
                                            <img src="/<?=$model->signature_admin_second?>" style="width: 400px; height: 300px; border: 1px solid #000;">
                                        </div>
                                    </div>
                                </div>
                                <?php \yii\widgets\ActiveForm::end() ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="hidden">
                                            <?= Html::a('<i class="feather icon-edit"></i> Изменить', ['signature', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => 0]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane<?= $tab == 'history-fill' ? ' active' : '' ?>" id="history-fill" role="tabpanel" aria-labelledby="history-tab-fill">
                                <h3>Услуги</h3>
                                <div class="default-collapse collapse-bordered">
                                    <?php foreach ($jobListDataProvider->models as $jobList): ?>
                                        <div class="card collapse-header">
                                            <div id="headingCollapse1" class="card-header collapsed">
                                          <span class="lead collapse-title" onclick="$(this).parent().parent().find('.collapse-wrapper').toggleClass('hidden');">
                                            <?= $jobList->job->name ?>
                                          </span>
                                            </div>
                                            <div id="collapse1" class="collapse-wrapper hidden">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <?php

                                                                $statusSearchModel = new JobOrderStatusSearch();
                                                                $statusDataProvider = $statusSearchModel->search(Yii::$app->request->queryParams);
                                                                $statusDataProvider->query->andWhere(['job_list_id' => $jobList->id]);
                                                                $statusDataProvider->pagination = false;

                                                                ?>

                                                                <?= $this->render('@app/views/job-order-status/index', [
                                                                    'searchModel' => $statusSearchModel,
                                                                    'dataProvider' => $statusDataProvider
                                                                ]) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Подписи</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="cont">
                                <div class="data-client-coords" style="display: none;"><?=$model->signature_client?></div>
                                <p>Роспись клиента</p>
                                <canvas id="imageViewClient" width="400" height="300">
                                    <p>Unfortunately, your browser is currently unsupported by our web application. We are sorry for the inconvenience. Please use one of the supported browsers listed below, or draw the image you want using an offline tool.</p>
                                    <p>Supported browsers: <a href="http://www.opera.com">Opera</a>, <a href="http://www.mozilla.com">Firefox</a>, <a href="http://www.apple.com/safari">Safari</a>, and <a href="http://www.konqueror.org">Konqueror</a>.</p>
                                </canvas>
                                <canvas id="imageTempClient" width="400" height="300"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'class' => 'modal-big-md',
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
 var canvas = document.getElementById("imageViewAct"), 
                context = canvas.getContext("2d");
                 
            var img = new Image();
            img.src = "/images/act-image-dist.png";
            img.onload = function() {
                context.drawImage(img, 0, 0);
            };
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>

