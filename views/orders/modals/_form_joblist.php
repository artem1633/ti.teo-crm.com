<?php

use app\models\manual\ListServices;
use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>


<style>
    .list-cell__price {
        /*width: 9%;*/
        padding-right: 2px !important;
    }
    .list-cell__prepaid {
        /*width: 10%;*/
        padding-left: 0 !important;
    }
    .list-cell__master_price {
        /*width: 9%;*/
        padding-right: 2px !important;
    }
    .list-cell__master_prepaid {
        /*width: 10%;*/
        padding-left: 0 !important;
    }

    .list-cell__job_id {
        width: 139px !important;
    }

    .list-cell__master_id {
        width: 139px !important;
    }

    #orders-type label {
        font-size: 20px;
    }

    #orders-type label:first-child {
        margin-right: 20px;
        display: inline-block;
    }
</style>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'jobs')->widget(\unclead\multipleinput\MultipleInput::class, [
    'id' => 'my_id',
    'addButtonOptions' => [
        'class' => 'btn btn-my-square btn-primary',
        'label' => '+',
    ],
    'removeButtonOptions' => [
        'class' => 'btn btn-my-square btn-danger',
        'label' => '-',
    ],
    'min' => 0,
    'columns' => [
        [
            'name' => 'id',
            'options' => [
                'type' => 'hidden'
            ]
        ],
        [
            'name'  => 'job_id',
            'title' => 'Вид услуги',
            'options' => [
                'class' => 'input-priority'
            ],
//                                'headerOptions' => [
//                                    'style' => 'width: 230px;'
//                                ],
            'type'  => 'dropDownList',
            'enableError' => true,
            'items' => ArrayHelper::map(ListServices::find()->orderBy('name asc')->all(), 'id', 'name')
        ],

        [
            'name'  => 'price',
            'title' => 'Оплата клиента',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'placeholder' => 'Сумма',
                'style' => "width: 70px;",
            ],
            'headerOptions' => [
                'style' => 'width: 70px;'
            ],
        ],

        [
            'name'  => 'prepaid',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'placeholder' => 'Аванс',
                'style' => "width: 70px;",
            ],
            'headerOptions' => [
                'style' => 'width: 70px;'
            ],
        ],


//                            [
//                                'name'  => 'material_pay',
//                                'title' => 'Оплата за материал',
//                                'enableError' => true,
//                                'options' => [
//                                    'class' => 'input-priority'
//                                ]
//                            ],

        [
            'name'  => 'master_id',
            'title' => 'Мастер',
            'type'  => 'dropDownList',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'prompt' => 'Выберите'
            ],
            'items' => ArrayHelper::map(Users::find()->where(['role_id' => Users::USER_ROLE_MASTER])->andWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->all(), 'id', 'name')
        ],

        [
            'name'  => 'master_price',
            'title' => 'Оплата мастеру',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'placeholder' => 'Сумма',
                'style' => "width: 70px;",
            ],
            'headerOptions' => [
                'style' => 'width: 70px;'
            ],
        ],

        [
            'name'  => 'master_prepaid',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'placeholder' => 'Аванс',
                'style' => "width: 70px;",
            ],
            'headerOptions' => [
                'style' => 'width: 70px;'
            ],
        ],
        [
            'name'  => 'cost_credit',
            'title' => 'Оплата за расходник',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ],
            'headerOptions' => [
                'style' => 'width: 100px;'
            ],
        ],

        [
            'name'  => 'date',
            'title' => 'Срок',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority',
                'type' => 'date',
            ],
            'headerOptions' => [
                'style' => 'width: 130px;'
            ],
        ],

        [
            'name'  => 'description',
            'title' => 'Примечание',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ],
            'headerOptions' => [
                'style' => 'width: 250px;'
            ],
        ],
    ],
])->label(false); ?>

<div class="hidden">
    <?= $form->field($model, 'acts')->widget(\unclead\multipleinput\MultipleInput::class, [
        'id' => 'acts_id',
        'addButtonOptions' => [
            'class' => 'btn btn-my-square btn-primary',
            'label' => '+',
        ],
        'removeButtonOptions' => [
            'class' => 'btn btn-my-square btn-danger',
            'label' => 'x',
        ],
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ]
            ],

            [
                'name'  => 'name',
                'title' => 'Наименование',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],

            [
                'name'  => 'count',
                'title' => 'Кол-во',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ],
                'headerOptions' => [
                    'style' => 'width: 100px;'
                ],
            ],
        ],
    ]); ?>
</div>

<?php ActiveForm::end(); ?>
