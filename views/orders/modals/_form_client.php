<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\range\RangeInput;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <?php $form = ActiveForm::begin(['id' => 'client-form']); ?>
        
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>            
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>     
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'group_id')->dropDownList($model->getGroupList(), ['prompt' => 'Выберите']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'atelier_id')->dropDownList($model->getAtelierList(), ['prompt' => 'Выберите']) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'sale_for_work')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'sale_for_product')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'usd_ball')->textInput(['type' => 'number']) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'address')->textarea(['rows' => 3]) ?>
                </div>
            </div>
   
        <?php ActiveForm::end(); ?>
    </div>
</div>
