<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\range\RangeInput;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <?php $form = ActiveForm::begin(['id' => 'order-form']); ?>
        
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'fabric_structure')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'damage')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'product_warranty')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'complekt')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'defect')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'wear')->textInput(['type' => 'number']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'spots_location')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'hidden_defect')->dropDownList($model->getHiddenList(), [ 'prompt' => 'Выберите типа']) ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'material')->textInput(['maxlength' => true]) ?>
                </div>
            </div>    
   
        <?php ActiveForm::end(); ?>
    </div>
</div>
