<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\range\RangeInput;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <?php $form = ActiveForm::begin(['id' => 'order-form']); ?>
        
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'order_type')->dropDownList($model->getTypeList(), [ /*'prompt' => 'Выберите типа'*/]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'pollution_id')->dropDownList($model->getPollutionList(), [ 'prompt' => 'Выберите']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'clothes_type')->dropDownList($model->getClothesTypeList(), [ 'prompt' => 'Выберите типа']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'prepay')->textInput(['type' => 'number']) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'spot_id')->dropDownList($model->getSpotList(), [ 'prompt' => 'Выберите']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'lost_product')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'user_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getUsersList(),
                        'options' => ['placeholder' => 'Выберите мастера'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'sale')->textInput(['type' => 'number']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'issue_date')->widget(kartik\datetime\DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'marketing_id')->dropDownList($model->getMarketingList(), [ 'prompt' => 'Выберите']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'marks_id')->dropDownList($model->getMarksList(), [ 'prompt' => 'Выберите']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'quick_value')->checkbox(['style' => 'margin-top:30px;']) ?>
                </div>
            </div>    

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>  
                </div>
            </div>    
        <?php ActiveForm::end(); ?>
    </div>
</div>
