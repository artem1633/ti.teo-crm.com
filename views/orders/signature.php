<?php

use yii\helpers\Html;

/** @var $model \app\models\Orders */


?>


<div class="card">
    <div class="card-header">
        <h4 class="card-title">Подпись</h4>
    </div>
    <div class="card-content">
        <div class="card-body">

            <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'signature-form']) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="cont">
                        <p>Роспись клиента</p>
                        <div class="hidden">
                            <p><label>Drawing tool: <select id="dtool"><option value="pencil" selected>Pencil</option></select></label></p>
                        </div>
                        <canvas id="imageTempClient" width="400" height="300"></canvas>
                    </div>
                    <input name="signature_client" type="hidden" id="signature-client-fld" value="">
                </div>
                <div class="col-md-6">
                    <div class="cont">
                        <p>Роспись администратора</p>
                        <canvas id="imageTempAdmin" width="400" height="300"></canvas>
                    </div>
                    <input name="signature_admin" type="hidden" id="signature-admin-fld">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <?= Html::a('Очистить', '#', ['id' => 'clear-btn-first', 'class' => 'btn btn-primary']) ?>
                </div>
                <div class="col-md-3">

                </div>
                <div class="col-md-3 text-center">
                    <?= Html::a('Очистить', '#', ['id' => 'clear-btn-admin', 'class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?= Html::a('Назад', ['orders/update-additional', 'id' => $model->id], ['class' => 'btn btn-primary', 'style' => 'margin-top: 20px;']) ?>
                    <?= Html::submitButton('Подписать', ['id' => 'submit-signature-btn', 'class' => 'btn btn-primary', 'style' => 'margin-top: 20px;']) ?>
                </div>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>
<?php

$script = <<< JS
var canvas = document.querySelector("#imageTempClient");

var signaturePad = new SignaturePad(canvas);

// Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible parameters)
signaturePad.toDataURL(); // save image as PNG
signaturePad.toDataURL("image/jpeg"); // save image as JPEG
signaturePad.toDataURL("image/svg+xml"); // save image as SVG

// Draws signature image from data URL.
// NOTE: This method does not populate internal data structure that represents drawn signature. Thus, after using #fromDataURL, #toData won't work properly.
signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

// Returns signature image as an array of point groups
var data = signaturePad.toData();

// Draws signature image from an array of point groups
signaturePad.fromData(data);

// Clears the canvas
signaturePad.clear();

// Returns true if canvas is empty, otherwise returns false
signaturePad.isEmpty();

// Unbinds all event handlers
signaturePad.off();

// Rebinds all event handlers
signaturePad.on();

var saveButton = document.getElementById('load-btn-first');
var cancelButton = document.getElementById('clear-btn-first');


cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
});


var canvas = document.querySelector("#imageTempAdmin");

var signaturePad2 = new SignaturePad(canvas);

// Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible parameters)
signaturePad2.toDataURL(); // save image as PNG
signaturePad2.toDataURL("image/jpeg"); // save image as JPEG
signaturePad2.toDataURL("image/svg+xml"); // save image as SVG

// Draws signature image from data URL.
// NOTE: This method does not populate internal data structure that represents drawn signature. Thus, after using #fromDataURL, #toData won't work properly.
signaturePad2.fromDataURL("data:image/png;base64,iVBORw0K...");

// Returns signature image as an array of point groups
var data = signaturePad2.toData();

// Draws signature image from an array of point groups
signaturePad2.fromData(data);

// Clears the canvas
signaturePad2.clear();

// Returns true if canvas is empty, otherwise returns false
signaturePad2.isEmpty();

// signaturePad2 all event handlers
signaturePad2.off();

// Rebinds all event handlers
signaturePad2.on();

var saveButton = document.getElementById('load-btn-first');
var cancelButton2 = document.getElementById('clear-btn-admin');



cancelButton2.addEventListener('click', function (event) {
  signaturePad2.clear();
});



$('#submit-signature-btn').click(function(e){
    e.preventDefault();
    
    $('#submit-signature-btn').html('<i class="fa fa-spinner fa-spin"></i> Подписать');
    
var data = signaturePad.toDataURL('image/png');

// Send data to server instead...
//   window.open(data);

    console.log(data);
    
    var param = $('meta[name=csrf-param]').attr("content");
    var token = $('meta[name=csrf-token]').attr("content");
    
    $.ajax({
        url: "/orders/load-img",
        method: "POST",
        data: {
            'data': data,
            '_csrf': token,
        },
        success: function(response){
            console.log(response);
            $('#signature-client-fld').val(response.path);
            
            var data = signaturePad2.toDataURL('image/png');

        // Send data to server instead...
        //   window.open(data);
        
            console.log(data);
            
            var param = $('meta[name=csrf-param]').attr("content");
            var token = $('meta[name=csrf-token]').attr("content");
            
            $.ajax({
                url: "/orders/load-img",
                method: "POST",
                data: {
                    'data': data,
                    '_csrf': token,
                },
                success: function(response){
                    console.log(response);
                    $('#signature-admin-fld').val(response.path);
                    
                    $('#signature-form').submit();
                }
            });
            
        }
    });
    
    

});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>