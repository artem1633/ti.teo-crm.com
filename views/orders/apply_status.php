<?php

use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(\app\models\manual\OrderStatus::find()->orderBy('sort')->all(), 'id', 'name'),
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
