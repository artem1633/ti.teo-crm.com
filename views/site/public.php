<?php

/** @var $this \yii\web\View */

$this->title = "Добро пожаловать!";

?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title"></h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <h3>Добро пожаловать в систему, <?= Yii::$app->user->identity->name ?>!</h3>
        </div>
    </div>
</div>

