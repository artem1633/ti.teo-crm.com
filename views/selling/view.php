<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Selling */
?>
<div class="selling-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'storage_id',
                'value' => function($data){
                    return $data->storage->name;
                }
            ],
            [
                'attribute' => 'client_id',
                'value' => function($data){
                    return $data->client->fio;
                }
            ],
            'summa',
            [
            'attribute'=>'comment',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
                ],
            ],
            [
                'attribute' => 'creater_id',
                'value' => function($data){
                    return $data->creater->name;
                }
            ], 
            [
                'attribute' => 'changed_id',
                'value' => function($data){
                    return $data->changed->name;
                }
            ],
            [
                    'attribute' => 'date_cr',
                    'value' => function ($data) {
                     if($data->date_cr != null )  return \Yii::$app->formatter->asDate($data->date_cr, 'php:H:i d.m.Y');
                    },
            ],
            [
                    'attribute' => 'date_up',
                    'value' => function ($data) {
                     if($data->date_up != null )  return \Yii::$app->formatter->asDate($data->date_up, 'php:H:i d.m.Y');
                    },
            ],
        ],
    ]) ?>

</div>
