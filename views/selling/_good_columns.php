<?php
use yii\helpers\Url;
use yii\helpers\Html;
return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    //],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'selling_id',
    //     'content' => function($data){
    //         return $data->selling->id;
    //     }
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'product_id',
        'content' => function($data){
            return $data->product->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
    ],
    
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/sellings-good/update-good', 'id' => $model->id]);
                return Html::a('<i class="feather icon-edit"></i>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/sellings-good/delete-good', 'id' => $model->id]);
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    'style' => 'font-size: 16px;'
                ]);
            },
        ]
    ]

];   