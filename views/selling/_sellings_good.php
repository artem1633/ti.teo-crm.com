<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<?=GridView::widget([ 
    'id'=>'crud-datatable',
    'dataProvider' => $model->SellingsGood,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
                'attribute' => 'product_id',
                'value' => function($data){
                    return $data->product->name;
                }
        ],
        'price',
        'count',
        [
            'class'    => 'kartik\grid\ActionColumn',
            'template' => '{leadDelete}',
            'buttons'  => [
                'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/sellings-good/delete-sellins-goods', 'id' => $model->id]);
                    return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                        'style' => 'font-size: 16px;',
                    ]);
                },
            ]
        ]
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
