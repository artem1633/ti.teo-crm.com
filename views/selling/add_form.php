<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */ 
/* @var $model app\models\Selling */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Создать';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selling-form">

<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'storage_id')->dropDownList($model->getStorages(), ['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'client_id')->dropDownList($model->getClients(), ['prompt' => 'Выберите']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'date_cr')->textInput() ?>  
        <?= $form->field($model, 'date_up')->textInput() ?>
        <?= $form->field($model, 'summa')->textInput() ?> 
        <?= $form->field($model, 'creater_id')->textInput() ?>
        <?= $form->field($model, 'changed_id')->textInput() ?>      
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>