<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Selling */
?>
<div class="selling-update">

    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>

</div>
