<?php

use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Selling */
$this->title = 'Продажи';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Документ</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'selling-pjax']) ?>   
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">                                    
                        <table class="table table-bordered">
                            <h3>Изменить <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['selling/edit', 'id' => $model->id])?>"><i class="feather icon-edit"></i></a></h3>
                            <tbody>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('storage_id')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('summa')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('client_id')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('creater_id')?></b></th>
                                </tr>
                                <tr>
                                    <td><?=Html::encode($model->storage->name)?></td>
                                    <td><?=Html::encode($model->summa)?></td>
                                    <td><?=Html::encode($model->client->fio)?></td>      
                                    <td><?=Html::encode($model->creater->name)?></td>       
                                </tr>
                                <tr>
                                    <th><b><?=$model->getAttributeLabel('changed_id')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('date_cr')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('date_up')?></b></th>
                                    <th><b><?=$model->getAttributeLabel('comment')?></b></th>
                                </tr>
                                <tr>
                                    <td><?=Html::encode($model->changed->name)?></td>      
                                    <td><?= $model->date_cr != null ? Html::encode(\Yii::$app->formatter->asDate($model->date_cr, 'php:H:i d.m.Y')) : '' ?></td> 
                                    <td><?= $model->date_up != null ? Html::encode(\Yii::$app->formatter->asDate($model->date_up, 'php:H:i d.m.Y')) : '' ?></td>
                                    <td><?=Html::encode($model->comment)?></td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            <?php Pjax::end() ?> 
        </div>
    </div>
</div>


    <div class="card">
    <div class="card-header">
        <h4 class="card-title">Товары</h4>
    </div>
    <div class="card-content">
    <div class="card-body">
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'good-pjax']) ?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'good-datatable',
                'dataProvider' => $goosdataProvider,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_good_columns.php'),
                'panelBeforeTemplate' =>    Html::a('Добавить <i class=""></i>', ['/sellings-good/add', 'selling_id' => $model->id], ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success'])." ".Html::a('Закрыть документ<i class=""></i>', ['/selling/index'], ['role'=>'','title'=> 'Закрыть документ','class'=>'btn btn-warning']),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>'',
                ]
            ])?>
        </div>
        <?php Pjax::end() ?>
    </div>
    </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('1'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>