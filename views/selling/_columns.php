<?php
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ], 
    [
        'class'=>'kartik\grid\ExpandRowColumn', 
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_sellings_good', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'collapseIcon' => '<i class="text-primary feather icon-corner-right-up"></i>',
        'expandIcon' => '<i class="text-primary feather icon-corner-right-down"></i>',
        'expandOneOnly'=>true
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'storage_id',
        'content' => function($data){
            return $data->storage->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'content' => function($data){
            return $data->client->fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'comment',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'creater_id',
        'content' => function($data){
            return $data->creater->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'changed_id',
        'content' => function($data){
            return $data->changed->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_cr',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_cr, 'php:H:i d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_up',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_up, 'php:H:i d.m.Y');
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/selling/next', 'id' => $model->id]);
                return Html::a('<i class="feather icon-edit"></i>', $url, ['title'=>'', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',]);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/selling/delete', 'id' => $model->id]);
                return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                            'style' => 'font-size: 16px;',
                ]);
            },
        ]
    ]

];   