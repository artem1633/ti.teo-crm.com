<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Selling */

?>
<div class="selling-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

