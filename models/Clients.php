<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\manual\ClientsGroup;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $fio
 * @property integer $group_id
 * @property string $address
 * @property string $telephone
 * @property string $email
 * @property double $sale_for_work
 * @property double $sale_for_product
 * @property double $usd_ball
 * @property integer $atelier_id
 *
 * @property Atelier $atelier
 * @property ClientsGroup $group
 */
class Clients extends \yii\db\ActiveRecord
{
    /** @var array */
    public $cars;

    const TYPE_PERSON = 0;
    const TYPE_COMPANY = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    /*public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
    }  */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'atelier_id', 'company_id', 'type'], 'integer'],
            [['address', 'description'], 'string'],
//            [['fio', 'atelier_id'], 'required'],
            [['fio'], 'required', 'when' => function($model){
                return $this->type == self::TYPE_PERSON;
            }],
            [['company_name'], 'required', 'when' => function($model){
                return $this->type == self::TYPE_COMPANY;
            }],
            [['email'], 'email'],
            [['sale_for_work', 'sale_for_product', 'usd_ball'], 'number'],
            [['fio', 'telephone', 'email', 'company_name', 'company_full_name', 'bank_name', 'bin', 'iban', 'bik', 'contact_person'], 'string', 'max' => 255],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\ClientsGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['cars'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'type' => 'Тип',
            'group_id' => 'Группа',
            'address' => 'Адрес',
            'telephone' => 'Контакты',
            'email' => 'Email',
            'sale_for_work' => 'Скидка-кэшбэк на работу',
            'sale_for_product' => 'Скидка-кэшбэк на товар',
            'company_name' => 'Короткое наименование компании',
            'company_full_name' => 'Юридическое наименование компании',
            'bank_name' => 'Банк',
            'bin' => 'БИН',
            'iban' => 'IBAN',
            'bik' => 'БИК',
            'usd_ball' => 'Балл',
            'description' => 'Примечание',
            'atelier_id' => 'Магазин',
            'company_id' => 'Company ID',
            'contact_person' => 'Контактное лицо',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->atelier_id = Yii::$app->user->identity->atelier_id;
            if($this->sale_for_work == null) $this->sale_for_work = 0;
            if($this->sale_for_product == null) $this->sale_for_product = 0;
        }
        
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $orders = Orders::find()->where(['client_id' => $this->id])->all();
        foreach ($orders as $order) 
        {
            $order->delete();
        }

        $sellings = Selling::find()->where(['client_id' => $this->id])->all();
        foreach ($sellings as $selling) 
        {
            $selling->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert == false){
//            $allSubs = ArrayHelper::getColumn(ClientsAuto::find()->where(['client_id' => $this->id])->all(), 'id');
//            $cars = $this->cars ? ArrayHelper::getColumn($this->cars, 'id') : [];
//            \Yii::warning($this->jobs);
//            \Yii::warning($cars);
//            foreach ($allSubs as $sub) {
//                $delete = true;
//                foreach($cars as $car){
//                    if($car == $sub){
//                        $delete = false;
//                    }
//                }
//                if($delete){
//                    ClientsAuto::deleteAll(['id' => $sub]);
//                }
//            }
        }

        if ($this->cars != null) {
            foreach ($this->cars as $item) {
                $contact = ClientsAuto::find()->where(['id' => $item['id']])->one();

                if (!$contact) {
                    (new ClientsAuto([
                        'client_id' => $this->id,
                        'type_id' => $item['type_id'],
                        'marks_id' => $item['marks_id'],
                        'spot_id' => $item['spot_id'],
                        'number' => $item['number'],
                        'passport' => $item['passport'],
                        'color' => $item['color'],
                        'year' => $item['year'],
//                        'customer_id' => $this->id,
                    ]))->save(false);
                }else{
                    $contact->client_id = $this->id;
                    $contact->type_id = $item['type_id'];
                    $contact->marks_id = $item['marks_id'];
                    $contact->spot_id = $item['spot_id'];
                    $contact->number = $item['number'];
                    $contact->passport = $item['passport'];
                    $contact->color = $item['color'];
                    $contact->year = $item['year'];
                    $contact->save(false);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_PERSON => 'Физ. лицо',
            self::TYPE_COMPANY => 'Юр. лицо'
         ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ClientsGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellings()
    {
        return $this->hasMany(Selling::className(), ['client_id' => 'id']);
    }

    public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }

    public function getGroupList()
    {
        $groups = manual\ClientsGroup::find()->all();
        return ArrayHelper::map($groups, 'id', 'name');
    }
}
