<?php

namespace app\models;
 
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html; 
use app\models\manual\Product;
/**
 * This is the model class for table "sellings_good".
 *
 * @property int $id
 * @property int $selling_id
 * @property int $product_id
 * @property double $price
 * @property int $count
 *
 * @property Selling $selling
 */
class SellingsGood extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sellings_good';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['selling_id', 'product_id', 'count'], 'integer'],
            [['price'], 'number'],
            [['price','count'], 'integer','min'=>0],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['selling_id'], 'exist', 'skipOnError' => true, 'targetClass' => Selling::className(), 'targetAttribute' => ['selling_id' => 'id']],
            [['product_id','count','price'], 'required'],
        ];
    }
 
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'selling_id' => '#',
            'product_id' => 'Название продукта',
            'price' => 'Цена',
            'count' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSelling()
    {
        return $this->hasOne(Selling::className(), ['id' => 'selling_id']);
    }
    public function getSellings()
    {
        $selling = Selling::find()->all();
        return ArrayHelper::map($selling, 'id', 'id');
    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getPro()
    {
        $pro = Product::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }
}
