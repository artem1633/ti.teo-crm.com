<?php

namespace app\models;

use app\models\manual\Marking;
use app\models\manual\Spot;
use app\models\manual\TypeClothes;
use Yii;

/**
 * This is the model class for table "clients_auto".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $type_id
 * @property integer $marks_id
 * @property integer $spot_id
 * @property string $number
 * @property string $passport
 * @property string $color
 * @property string $year
 *
 * @property Clients $client
 * @property TypeClothes $type
 * @property Marking $marks
 * @property Spot $spot
 */
class ClientsAuto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients_auto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'type_id', 'marks_id', 'spot_id'], 'integer'],
            [['number', 'passport', 'color', 'year'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeClothes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['marks_id'], 'exist', 'skipOnError' => true, 'targetClass' => Marking::className(), 'targetAttribute' => ['marks_id' => 'id']],
            [['spot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spot::className(), 'targetAttribute' => ['spot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'type_id' => 'Тип',
            'marks_id' => 'Марка',
            'spot_id' => 'Модель',
            'number' => 'Гос. номер',
            'passport' => 'Тех. паспорт',
            'color' => 'Цвет',
            'year' => 'Год выпуска',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TypeClothes::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasOne(Marking::className(), ['id' => 'marks_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpot()
    {
        return $this->hasOne(Spot::className(), ['id' => 'spot_id']);
    }
}
