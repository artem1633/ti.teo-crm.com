<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use yii\web\UploadedFile;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property integer $access_for_moving
 * @property string $telephone
 * @property string $role_id
 * @property string $status
 * @property integer $atelier_id
 * @property double $sales_percent
 * @property double $cleaner_percent
 * @property double $sewing_percent
 * @property double $repairs_percent
 * @property string $auth_key
 * @property string $data_cr
 * @property integer $can_dashboard
 * @property integer $can_orders
 * @property integer $can_clients
 * @property integer $can_masters
 * @property integer $can_reports
 * @property integer $can_users
 * @property integer $can_storage
 * @property integer $can_book
 * @property integer $can_view
 * @property integer $can_update
 * @property integer $can_delete
 * @property integer $accept_notifications
 * @property string $files
 *
 * @property boolean $isOnline
 *
 * @property Atelier $atelier
 */
class Users extends \yii\db\ActiveRecord
{
    const SCENARIO_CREDENTIALS = 'scenario_credentials';
    const SCENARIO_COMMON = 'scenario_common';

    const USER_ROLE_ADMIN = 'administrator';
    const USER_TYPE_SUPER_ADMIN = 'super_admin';
    const USER_ROLE_WAREHOUSE_MANAGER = 'warehouse_manager';
    const USER_ROLE_MASTER = 'master';
    const ACCESS_MOVING_YES = 1;
    const ACCESS_MOVING_NO = 0;
    const USER_STATUS_WORKING = 'working';
    const USER_STATUS_OUT_OF_SERVICE = 'out_of_service';

    const STATE_IN = 0;
    const STATE_OUT = 1;

    public $new_password;
    public $repeatPassword;

    public $groups;

    public $files;

    public $file;

    public $isMaster;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
//    public static function find()
//    {
//        if (isset(Yii::$app->user->identity->id)){
//            $companyId = Yii::$app->user->identity->company_id;
//        } else {
//            $companyId = null;
//        }
//
//        return new AppActiveQuery(get_called_class(), [
//            'companyId' => $companyId,
//        ]);
//    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_CREDENTIALS => ['login', 'new_password', 'repeatPassword', 'vk_id'],
            self::SCENARIO_COMMON => ['name', 'groups', 'state', 'telephone', 'address', 'file', 'role_id'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['name', 'password', 'telephone', 'login'], 'required'],
//            [['login', 'new_password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREDENTIALS],
            [['login', 'new_password', 'repeatPassword'], 'required'],
            [['access_for_moving', 'atelier_id', 'company_id', 'state', 'can_dashboard', 'can_orders', 'can_clients', 'can_masters', 'can_order', 'can_reports', 'can_users', 'can_storage', 'can_book', 'can_view', 'can_update', 'can_delete', 'accept_notifications', 'isMaster'], 'integer'],
            [['sales_percent', 'cleaner_percent', 'sewing_percent', 'repairs_percent'], 'number', 'min' => 0, 'max' => 100],
            [['data_cr', 'last_activity_datetime', 'groups', 'files'], 'safe'],
            [['login'], 'email'],
            [['password', 'new_password', 'repeatPassword'], 'string', 'max' => 255, 'min' => 6],
//            ['repeatPassword', function(){
//                if($this->new_password != $this->repeatPassword){
//                    $this->addError('repeatPassword', 'Пароли не совпадают');
//                }
//            }, 'on' => self::SCENARIO_CREDENTIALS],
            ['repeatPassword', function(){
                if($this->new_password != $this->repeatPassword){
                    $this->addError('repeatPassword', 'Пароли не совпадают');
                }
            }],
            [['documents'], 'string'],
            [['name', 'login', 'telephone', 'role_id', 'status', 'auth_key', 'vk_id', 'address'], 'string', 'max' => 255],
            [['login', 'atelier_id'], 'unique'],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['file'], 'file'],
//            ['atelier_id', 'required', 'when' => function($model) { return $model->role_id != self::USER_ROLE_ADMIN; }, 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Email',
            'password' => 'Пароль',
            'access_for_moving' => 'Доступ для перемешения',
            'telephone' => 'Телефон',
            'role_id' => 'Должность',
            'status' => 'Статус',
            'atelier_id' => 'Филиал',
            'address' => 'Адрес',
            'state' => 'Положение',
            'sales_percent' => 'За продажу (%)',
            'cleaner_percent' => 'За химчитску (%)',
            'sewing_percent' => 'За пошива (%)',
            'repairs_percent' => 'За ремонта (%)',
            'documents' => 'Документы',
            'auth_key' => 'Пароль',
            'data_cr' => 'Дата регистрации',
            'company_id' => 'Компания, к которой принадлежит пользователь',
            'vk_id' => 'Telegram ID',
            'last_activity_datetime' => 'Дата и время последней активности',
            'new_password' => 'Новый пароль',
            'groups' => 'Группы',
            'can_dashboard' => 'Dashboard',
            'can_orders' => 'Заявки',
            'can_clients' => 'Клиенты',
            'can_masters' => 'Мастера',
            'can_reports' => 'Отчеты',
            'can_users' => 'Пользователи',
            'isMaster' => 'Мастер',
            'files' => 'Файлы',
            'file' => 'Файл',
            'repeatPassword' => 'Повторите пароль',
            'can_storage' => 'Склад',
            'can_book' => 'Справочники',
            'can_order' => 'Заявки',
            'can_view' => 'Просмотр',
            'can_update' => 'Изменение',
            'accept_notifications' => 'Принимать уведомления',
            'can_delete' => 'Удаление',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->auth_key = $this->password;
            $this->password = md5($this->password);
            $this->data_cr = date('Y-m-d');

            if (isset(Yii::$app->user->identity->id)) {
                $log = new Logs([
                    'user_id' => Yii::$app->user->identity->id,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            }
            else {
                $log = new Logs([
                    'user_id' => null,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            }
        }

        if($this->isMaster){
            $this->role_id = self::USER_ROLE_MASTER;
        }

        if($this->files){ // Сохранение после Dropzone
            $this->files = explode(',', $this->files);
            $newFiles = [];
            foreach ($this->files as $file)
            {
                if($file != ''){
                    $newFiles[] = $file;
                }
            }
            $this->documents = json_encode($newFiles);
        }

        $file = \yii\web\UploadedFile::getInstance($this,'file');
        \Yii::warning($file);

        if($file){ // Сохранение по кнопке
            $fileName = Yii::$app->security->generateRandomString();

            if (is_dir('uploads') == false) {
                mkdir('uploads');
            }
            if (is_dir('uploads/data') == false) {
                mkdir('uploads/data');
            }
            $uploadPath = 'uploads/data';

            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;

            if ($file->saveAs($path)) {
                $docs = json_decode($this->documents, true);
                \Yii::warning($docs);
                if(is_array($docs)){
                    $docs[] = $path;
                    \Yii::warning($docs);
                    $this->documents = json_encode($docs);
                } else {
                    $this->documents = json_encode([$path]);
                }
            }
        }

        if($this->new_password != null)
        {
            $this->auth_key = $this->new_password;
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->scenario != self::SCENARIO_CREDENTIALS){
            UsersGroup::deleteAll(['users_id' => $this->id]);
            if($this->groups != null){
                foreach ($this->groups as $group)
                {
                    (new UsersGroup([
                        'users_id' => $this->id,
                        'group_id' => $group,
                    ]))->save(false);
                }
            }
        }
    }

    /**
     * @return array
     */
    public static function stateLabels()
    {
        return [
            self::STATE_IN => 'Штатный',
            self::STATE_OUT => 'Внештатный',
        ];
    }

    /**
     * @return boolean
     */
    public function getIsOnline()
    {
        $online = strtotime($this->last_activity_datetime);

        return (time() - $online) < 300;
    }

    /**
     * @param string $action
     * @return boolean
     */
    public function can($action)
    {
        if(Yii::$app->user->identity->id == 1){
            return true;
        }


        $attribute = "can_{$action}";
        if(isset($this->$attribute)){
            return $this->$attribute == 1 ? true : false;
        }

        return false;
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_WAREHOUSE_MANAGER, 'name' => 'Завсклад',],
            ['id' => self::USER_ROLE_MASTER, 'name' => 'Мастер',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if(self::USER_ROLE_ADMIN == $this->role_id) return 'Администратор';
        if(self::USER_ROLE_WAREHOUSE_MANAGER == $this->role_id) return 'Завсклад';
        if(self::USER_ROLE_MASTER == $this->role_id) return 'Мастер';
    }

    public function getMovingList()
    {
        return ArrayHelper::map([
            ['id' => self::ACCESS_MOVING_YES, 'name' => 'Да',],
            ['id' => self::ACCESS_MOVING_NO, 'name' => 'Нет',],
        ], 'id', 'name');
    }

    public function getMovingDescription()
    {
        if(self::ACCESS_MOVING_YES == $this->access_for_moving) return 'Да';
        if(self::ACCESS_MOVING_NO == $this->access_for_moving) return 'Нет';
    }

    public function getStatusList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_STATUS_WORKING, 'name' => 'Работает',],
            ['id' => self::USER_STATUS_OUT_OF_SERVICE, 'name' => 'Не работает',],
        ], 'id', 'name');
    }

    public function getStatusDescription()
    {
        if(self::USER_STATUS_WORKING == $this->status) return 'Работает';
        if(self::USER_STATUS_OUT_OF_SERVICE == $this->status) return 'Не работает';
    }

    public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Logs::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['acceptor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0()
    {
        return $this->hasMany(Orders::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders1()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellings()
    {
        return $this->hasMany(Selling::className(), ['changed_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellings0()
    {
        return $this->hasMany(Selling::className(), ['creater_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
