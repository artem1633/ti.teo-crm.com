<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "job_list_product".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $job_list_id
 * @property integer $product_id
 * @property double $count
 *
 * @property JobList $jobList
 * @property Orders $order
 * @property Product $product
 */
class JobListProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_list_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'job_list_id', 'product_id'], 'integer'],
            [['count'], 'number'],
            [['job_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobList::className(), 'targetAttribute' => ['job_list_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'job_list_id' => 'Услуга',
            'product_id' => 'Расходник',
            'count' => 'Кол-во',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobList()
    {
        return $this->hasOne(JobList::className(), ['id' => 'job_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
