<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\manual\AboutCompany;
use app\models\manual\Advertising;
use app\models\manual\Cashbox;
use app\models\manual\ClientsGroup;
use app\models\manual\Goods;
use app\models\manual\ListServices;
use app\models\manual\Marking;
use app\models\manual\OrderStatus;
use app\models\manual\OrdersColor;
use app\models\manual\Product;
use app\models\manual\ProductStatus;
use app\models\manual\QuickOrder;
use app\models\manual\Spot;
use app\models\manual\Suppliers;
use app\models\manual\TypeClothes;
use app\models\manual\TypePollution;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property int $admin_id Id администратора компании
 * @property int $rate_id Тариф
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $access_end_datetime Дата и время потери доступа к системе
 * @property int $is_super Супер компания
 *
 * @property Users $admin
 * @property Rates $rate
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id'], 'integer'],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['access_end_datetime', 'created_date'], 'safe'],
            [['company_name'], 'string', 'max' => 255],
            [['company_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_end_datetime' => 'Дата и время окончания доступа',
            'admin_id' => 'Администратор компании',
            'rate_id' => 'Тариф',
            'last_activity_datetime' => 'Последняя активность',
            'company_name' => 'Название компании',
            'created_date' => 'Дата регистрации',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_date = date("Y-m-d");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function isSuperCompany()
    {
        return $this->is_super == 1 ? true : false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->isSuperCompany() === false)
        {
            $this->deleteAllObjects($this->id);
            return parent::beforeDelete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCompanies()
    {
        return $this->hasMany(AboutCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisings()
    {
        return $this->hasMany(Advertising::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAteliers()
    {
        return $this->hasMany(Atelier::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashboxes()
    {
        return $this->hasMany(Cashbox::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsGroups()
    {
        return $this->hasMany(ClientsGroup::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Users::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListServices()
    {
        return $this->hasMany(ListServices::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkings()
    {
        return $this->hasMany(Marking::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoves()
    {
        return $this->hasMany(Move::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatuses()
    {
        return $this->hasMany(OrderStatus::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersColors()
    {
        return $this->hasMany(OrdersColor::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuickOrders()
    {
        return $this->hasMany(QuickOrder::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellings()
    {
        return $this->hasMany(Selling::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpots()
    {
        return $this->hasMany(Spot::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Suppliers::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeClothes()
    {
        return $this->hasMany(TypeClothes::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePollutions()
    {
        return $this->hasMany(TypePollution::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['company_id' => 'id']);
    }

    public function deleteAllObjects($company_id)
    {
        $moving = Move::find()->where(['company_id' => $company_id])->all();
        foreach ($moving as $value) 
        {
            $value->delete();
        }

        $orders = Orders::find()->where(['company_id' => $company_id])->all();
        foreach ($orders as $order) 
        {
            $job_list = JobList::find()->where(['order_id' => $order->id])->all();
            foreach ($job_list as $value) 
            {
                $value->delete();
            }
            $order->delete();
        }

        $available = Available::find()->where(['company_id' => $company_id])->all();
        foreach ($available as $value) 
        {
            $value->delete();
        }

        $resource = Resource::find()->where(['company_id' => $company_id])->all();
        foreach ($resource as $value) 
        {
            $value->delete();
        }

        $storage = Storage::find()->where(['company_id' => $company_id])->all();
        foreach ($storage as $value) 
        {
            $value->delete();
        }

        $client = Clients::find()->where(['company_id' => $company_id])->all();
        foreach ($client as $value) 
        {
            $value->delete();
        }

        $cashbox = manual\Cashbox::find()->where(['company_id' => $company_id])->all();
        foreach ($cashbox as $value) 
        {
            $value->delete();
        }

        $product = manual\Product::find()->where(['company_id' => $company_id])->all();
        foreach ($product as $value) 
        {
            $value->delete();
        }

        $service = manual\ListServices::find()->where(['company_id' => $company_id])->all();
        foreach ($service as $value) 
        {
            $value->delete();
        }

        $supliers = manual\Suppliers::find()->where(['company_id' => $company_id])->all();
        foreach ($supliers as $value) 
        {
            $value->delete();
        }

        $user = Users::find()->where(['company_id' => $company_id])->all();
        foreach ($user as $value) 
        {
            $value->delete();
        }

        $atelier = Atelier::find()->where(['company_id' => $company_id])->all();
        foreach ($atelier as $value) 
        {
            $value->delete();
        }

    }
}
