<?php

namespace app\models;

use app\modules\api\controllers\TelegramNotifyController;
use Yii;
use yii\base\Model;

class ResetPasswordForm extends Model
{
    public $login;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login'], 'exist', 'targetClass' => '\app\models\User'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
        ];
    }

    /**
     * @return Users|null
     */
    public function reset()
    {
       if($this->validate())
       {
           /** @var \app\models\Users $user */
           $user = Users::find()->where(['login' => $this->login])->one();

           if($user){
               if($user->vk_id != null){
                   TelegramNotifyController::sendTelMessage($user->vk_id, "Ваш пароль: {$user->auth_key}");
               }
           }

           return $user;
       }

       return null;
    }
}