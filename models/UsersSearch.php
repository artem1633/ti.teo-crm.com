<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'access_for_moving', 'atelier_id', 'company_id'], 'integer'],
            [['name', 'login', 'password', 'telephone', 'role_id', 'status', 'auth_key', 'data_cr', 'vk_id', 'last_activity_datetime'], 'safe'],
            [['sales_percent', 'cleaner_percent', 'sewing_percent', 'repairs_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'access_for_moving' => $this->access_for_moving,
            'atelier_id' => $this->atelier_id,
            'sales_percent' => $this->sales_percent,
            'cleaner_percent' => $this->cleaner_percent,
            'sewing_percent' => $this->sewing_percent,
            'repairs_percent' => $this->repairs_percent,
            'data_cr' => $this->data_cr,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'role_id', $this->role_id])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }
}
