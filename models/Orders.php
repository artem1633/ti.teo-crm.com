<?php

namespace app\models;

use app\helpers\ChatAPI;
use app\models\manual\ListServiceProduct;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\JobList;
use app\models\manual\ListServices;
use app\helpers\TelegramRequester;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $issue_date
 * @property integer $order_type
 * @property integer $client_id
 * @property string $telephone
 * @property integer $user_id
 * @property integer $clothes_type
 * @property double $prepay
 * @property double $sale
 * @property string $comment
 * @property integer $marketing_id
 * @property integer $quick
 * @property string $color
 * @property string $fabric_structure
 * @property string $material
 * @property string $symbol
 * @property string $product_warranty
 * @property string $complekt
 * @property string $defect
 * @property integer $pollution_id
 * @property string $damage
 * @property integer $spot_id
 * @property string $spots_location
 * @property integer $marks_id
 * @property integer $wear
 * @property integer $lost_product
 * @property integer $hidden_defect
 * @property string $signature_client
 * @property string $signature_admin
 * @property string $signature_client_second
 * @property string $signature_admin_second
 * @property string $act_draw
 * @property string $pre_date
 *
 * @property Clients $client
 * @property TypeClothes $clothesType
 * @property Advertising $marketing
 * @property Marking $marks
 * @property TypePollution $pollution
 * @property QuickOrder $quick0
 * @property Spot $spot
 * @property Users $user
 */
class Orders extends \yii\db\ActiveRecord
{

    const TYPE_PERSON = 0;
    const TYPE_COMPANY = 1;

    public $jobs;
    public $acts;


    public $clientName;

    private $after;
    private $telegramNotify;

    private $_oldPreDate;

    /**
     * @inheritdoc
     */
    public $step;
    public $quick_value;
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['fact_date', 'issue_date'], 'safe'],
            [['order_type'], 'required'],
            [['creator_id', 'acceptor_id', 'order_type', 'status_id', 'user_id', 'clothes_type', 'marketing_id', 'quick', 'pollution_id', 'spot_id', 'marks_id', 'wear', 'lost_product', 'hidden_defect', 'quick_value', 'atelier_id', 'company_id', 'type'], 'integer'],
            [['prepay', 'sale', 'cost'], 'number'],
            [['comment', 'clientName'], 'string'],
            [['telephone', 'color', 'fabric_structure', 'material', 'symbol', 'product_warranty', 'complekt', 'defect', 'damage', 'spots_location', 'number'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\OrderStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['clothes_type'], 'exist', 'skipOnError' => true, 'targetClass' => manual\TypeClothes::className(), 'targetAttribute' => ['clothes_type' => 'id']],
            [['marketing_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Advertising::className(), 'targetAttribute' => ['marketing_id' => 'id']],
            [['marks_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Marking::className(), 'targetAttribute' => ['marks_id' => 'id']],
            [['pollution_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\TypePollution::className(), 'targetAttribute' => ['pollution_id' => 'id']],
            [['quick'], 'exist', 'skipOnError' => true, 'targetClass' => manual\QuickOrder::className(), 'targetAttribute' => ['quick' => 'id']],
            [['spot_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Spot::className(), 'targetAttribute' => ['spot_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['acceptor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['acceptor_id' => 'id']],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            ['client_id', 'validateClient'],
            [['jobs', 'acts', 'signature_admin', 'signature_client', 'signature_admin_second', 'signature_client_second', 'act_draw', 'pre_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Статус',//
            'type' => 'Тип',
            'number' => 'Гос. Номер',
            'fact_date' => 'Фактическая дата',//
            'pre_date' => 'Дата приезда',
            'issue_date' => 'Выдать',//
            'order_type' => 'Год выпуска',//
            'client_id' => 'Клиент',//k
            'clientName' => 'ФИО клиента',
            'telephone' => 'Контакты ',//k
            'user_id' => 'Мастер',//
            'clothes_type' => 'Тип транспорта',//
            'prepay' => 'Предоплата',//
            'sale' => 'Скидка',//
            'comment' => 'Тех паспорт',//
            'marketing_id' => 'Реклама',//
            'quick' => 'Срочность',//
            'color' => 'Цвет ',//1
            'fabric_structure' => 'Примечание',//1
            'material' => 'Детали из искусственных материалов',//1
            'symbol' => 'Символика',//
            'product_warranty' => 'Фурнитура без гарантии',//1
            'complekt' => 'Компания',//1
            'defect' => 'Особые свойства (дефекты)',//1
            'pollution_id' => 'Степень и тип загрязнения',//
            'damage' => 'Повреждения',//1
            'spot_id' => 'Модель',//
            'spots_location' => 'Местоположение пятен',//1
            'marks_id' => 'Марка',//
            'wear' => 'Износ',//1
            'lost_product' => 'Пробег',//
            'hidden_defect' => 'Проявление скрытых дефектов',//1
            'quick_value' => 'Срочность',
            'acceptor_id' => 'Принял',//
            'creator_id' => 'Создатель',//
            'cost' => 'Цена',//
            'acts' => 'Акт',
            'atelier_id' => 'Магазин',
            'company_id' => 'Company ID',
            'act_draw' => 'Акт',
            'signature_admin' => 'Подпись Админа',
            'signature_client' => 'Подпись Клиента',
            'signature_admin_second' => 'Подпись Админа',
            'signature_client_second' => 'Подпись Клиента',
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->_oldPreDate = $this->pre_date;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->atelier_id = Yii::$app->user->identity->atelier_id;
            $status = manual\OrderStatus::find()->where(['name' => 'Принят', 'company_id' => Yii::$app->user->identity->company_id])->one();
            if($status != null) $this->status_id = $status->id;
            else
            {
//                $status = manual\OrderStatus::find()->one();
                if($this->pre_date != null){
                    $this->status_id = 1;
                } else {
                    $this->status_id = 2;
                }
            }
            $this->creator_id = Yii::$app->user->identity->id;
            $this->fact_date = date('Y-m-d H:i:s');
        }

        if($this->_oldPreDate != $this->pre_date){
            $this->status_id = 1; // Статус "Предварительный"
        }




        if($this->quick_value == 1) $this->quick = manual\QuickOrder::findOne(1)->id;
        else $this->quick = null;

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null, $after = true, $telegramNotify = true)
    {
        $this->after = $after;
        $this->telegramNotify = $telegramNotify;
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


//        $contactAll = CustomerContact::find()->where(['customer_id' => $this->id,])->all();
//        foreach ($contactAll as $item1) {
//            $a = false;
//            if ($this->contacts == null) {
//                $item1->delete();
//                continue;
//            }
//            foreach ($this->contacts as $item3) {
//                if ($item3['id'] == $item1->id) {
//                    $a = true;
//                }
//                if (!$a) {
//                    $item1->delete();
//                    break;
//                }
//            }
//        }

        if($this->after){
            if($insert == false){
//            if ($this->jobs != null) {
                $allSubs = ArrayHelper::getColumn(JobList::find()->where(['order_id' => $this->id])->all(), 'id');
                $jobs = $this->jobs ? ArrayHelper::getColumn($this->jobs, 'id') : [];
//                \Yii::warning($this->jobs);
//                \Yii::warning($jobs);
                foreach ($allSubs as $sub) {
                    $delete = true;
                    foreach($jobs as $job){
                        if($job == $sub){
                            $delete = false;
                        }
                    }
                    if($delete){
                        JobList::deleteAll(['id' => $sub]);
                    }
                }

                $allSubs = ArrayHelper::getColumn(OrderAct::find()->where(['order_id' => $this->id])->all(), 'id');
                $acts = $this->acts ? ArrayHelper::getColumn($this->acts, 'id') : [];
//                \Yii::warning($this->acts);
////                \Yii::warning($acts);
//                foreach ($allSubs as $sub) {
//                    $delete = true;
//                    foreach($acts as $act){
//                        if($act == $sub){
//                            $delete = false;
//                        }
//                    }
//                    if($delete){
//                        OrderAct::deleteAll(['id' => $sub]);
//                    }
//                }
//            }
            }

            if ($this->jobs != null) {
                foreach ($this->jobs as $item) {
                    $contact = JobList::find()->where(['id' => $item['id']])->one();

                    if (!$contact) {
                        $jobList = new JobList([
                            'job_id' => $item['job_id'],
                            'order_id' => $this->id,
                            'type' => $item['type'],
                            'price' => $item['price'],
                            'count' => $item['count'],
                            'material_pay' => $item['material_pay'],
                            'prepaid' => $item['prepaid'],
                            'master_price' => $item['master_price'],
                            'status_id' => $this->pre_date != null ? 1 : 2,
                            'master_prepaid' => $item['master_prepaid'],
                            'cost_credit' => $item['cost_credit'],
                            'master_id' => $item['master_id'],
                            'description' => $item['description'],
                            'date' => $item['date'],
//                        'customer_id' => $this->id,
                        ]);
                        $jobList->save(false, null, ($this->telegramNotify ? true : false));

                        // Отправка WhatsApp сообшения
                        /** @var StatusMessage $message */
                        // $message = StatusMessage::find()->where(['status_id' => $jobList->status_id, 'is_active' => true])->one();
                        // \Yii::warning($message, 'Status Message');
                        // if($message)
                        // {
                        //         if($this->telephone != null){
                        //             if($message->files == null){
                        //                 ChatAPI::sendMessage($this->telephone, $message->content);
                        //             } else {
                        //                 $files = explode(',', $message->files);
                        //                 if(count($files) > 0){
                        //                     $counter = 1;
                        //                     foreach ($files as $file)
                        //                     {
                        //                         if($file == '')
                        //                             continue;

                        //                         ChatAPI::sendFile($this->telephone, $file, ($counter == 1 ? $message->content : null));
                        //                         $counter++;
                        //                     }
                        //                 } else {
                        //                     ChatAPI::sendMessage($this->telephone, $message->content);
                        //                 }
                        //             }
                        //         }
                        // }

                    }else{
                        $contact->job_id = $item['job_id'];
                        $contact->order_id = $this->id;
                        $contact->type = $item['type'];
                        $contact->price = $item['price'];
                        $contact->count = $item['count'];
                        $contact->material_pay = $item['material_pay'];
                        $contact->prepaid = $item['prepaid'];
                        $contact->master_price = $item['master_price'];
                        $contact->master_prepaid = $item['master_prepaid'];
                        $contact->cost_credit = $item['cost_credit'];
                        $contact->master_id = $item['master_id'];
                        $contact->description = $item['description'];
                        $contact->date = $item['date'];
                        $contact->save(false);
                    }
                }
            }


            if ($this->acts != null) {
                foreach ($this->acts as $item) {
                    $contact = OrderAct::find()->where(['id' => $item['id']])->one();

                    if (!$contact) {
                        (new OrderAct([
                            'order_id' => $this->id,
                            'name' => $item['name'],
                            'count' => $item['count'],
                        ]))->save(false);
                    }else{
                        $contact->order_id = $this->id;
                        $contact->name = $item['name'];
                        $contact->count = $item['count'];
                        $contact->save(false);
                    }
                }
            }

            if($this->client_id == null){
                $client = new Clients([
//                    'fio' => $this->clientName,
                    'telephone' => $this->telephone,
                    'type' => $this->type
                ]);

                if($this->type == 0){
                    $client->fio = $this->clientName;
                } else {
                    $client->contact_person = $this->clientName;
                    $client->company_name = $this->complekt;
                }


                $client->save(false);
                $this->client_id = $client->id;
                $this->save(false, null, false);

                $auto = new ClientsAuto([
                    'client_id' => $client->id,
                    'marks_id' => $this->marks_id,
                    'type_id' => $this->clothes_type,
                    'spot_id' => $this->spot_id,
                    'number' => $this->number,
                    'passport' => $this->comment,
                    'color' => $this->color,
                    'year' => $this->order_type,
                ]);

                $auto->save(false);

            } else {
                $client = Clients::findOne($this->client_id);
                if($client != null){
                    $client->telephone = $this->telephone;
                    $client->type = $this->type;

                    if($this->type == 0){
                        $client->fio = $this->clientName;
                    } else {
                        $client->contact_person = $this->clientName;
                        $client->company_name = $this->complekt;
                    }

                    $client->save();


                    $auto = ClientsAuto::find()->where(['client_id' => $client->id, 'number' => $this->number])->one();
                    if($auto){
                        $attributes = [
                            'client_id' => $client->id,
                            'marks_id' => $this->marks_id,
                            'type_id' => $this->clothes_type,
                            'spot_id' => $this->spot_id,
                            'number' => $this->number,
                            'passport' => $this->comment,
                            'number' => $this->number,
                            'color' => $this->color,
                            'year' => $this->order_type,
                        ];

                        $auto->attributes = $attributes;
                        $auto->save(false);
                    } else {
                        $auto = new ClientsAuto();

                        $attributes = [
                            'client_id' => $client->id,
                            'marks_id' => $this->marks_id,
                            'type_id' => $this->clothes_type,
                            'spot_id' => $this->spot_id,
                            'number' => $this->number,
                            'passport' => $this->comment,
                            'number' => $this->number,
                            'color' => $this->color,
                            'year' => $this->order_type,
                        ];

                        $auto->attributes = $attributes;
                        $auto->save(false);
                    }
                }
            }
        }

        if($this->status_id == 2 && $this->telegramNotify == true){
            // Телеграм уведомление
            /** @var JobList[] $jobLists */
            $jobLists = JobList::find()->where(['order_id' => $this->id, 'telegram_notified' => false])->all();
            foreach ($jobLists as $joblist) {
                $master = Users::findOne($joblist->master_id);
                if($master){
                    if($master->vk_id){

                        $keyboard = [];

                        $listService = ListServices::findOne($joblist->job_id);
                        $carName = ArrayHelper::getValue($this, 'marks.name').' '.ArrayHelper::getValue($this, 'spot.name').' '.$this->number;
//                        $keyboard[] = [['text' => 'Услуга "'.$listService->name.'" (Заказ #'.$this->id.'), '.$carName.' | Начать работу', 'callback_data' => 'job_'.$joblist->id]];


                        $result = TelegramRequester::send('sendMessage', [
                            'chat_id' => $master->vk_id,
                            'text' => 'Вам назначены следующие услуги: Услуга "'.$listService->name.'" (Заказ #'.$this->id.'), '.$carName,
                            'parse_mode' => 'HTML',
                            'reply_markup' => json_encode([
                                'inline_keyboard' => [[['text' => 'Начать работу', 'callback_data' => 'job_'.$joblist->id]]],
                            ])
                        ]);

                        $jobLists->telegram_notified = true;
                        $joblist->save(false);

                        Yii::warning($result, 'Telegram API Response');
                    }
                }
            }
        }




        if($insert){

            $users = Users::find()->where(['accept_notifications' => true])->all();

            foreach ($users as $user){
                if($user->vk_id != null){
                    $result = TelegramRequester::send('sendMessage', [
                        'chat_id' => $user->vk_id,
                        'text' => "Создана новая заявка (ID {$this->id})",
                        'parse_mode' => 'HTML',
                    ]);
                }
            }
        }

    }

    public function beforeDelete()
    {
        $joblist = JobList::find()->where(['order_id' => $this->id])->all();
        foreach ($joblist as $job)
        {
            $job->delete();
        }

        return parent::beforeDelete();
    }

    //Новый клиент
    public function validateClient($attribute, $params)
    {
        $client = Clients::find()->where(['id' => $this->client_id])->one();
        if (!isset($client))
        {
            $client = new Clients();
            $client->fio = $this->client_id;
            $client->telephone = $this->telephone;
            $client->atelier_id = $this->atelier_id;
            $error = $client->errors;

            if ($client->save())
            {
                $this->client_id = $client->id;
            }
            else
            {
                $this->addError($attribute,"Не создан новый клиент");
            }
        }
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_PERSON => 'Физическое лицо',
            self::TYPE_COMPANY => 'Юридическое лицо',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptor()
    {
        return $this->hasOne(Users::className(), ['id' => 'acceptor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClothesType()
    {
        return $this->hasOne(manual\TypeClothes::className(), ['id' => 'clothes_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketing()
    {
        return $this->hasOne(manual\Advertising::className(), ['id' => 'marketing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(manual\OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasOne(manual\Marking::className(), ['id' => 'marks_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollution()
    {
        return $this->hasOne(manual\TypePollution::className(), ['id' => 'pollution_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuick0()
    {
        return $this->hasOne(manual\QuickOrder::className(), ['id' => 'quick']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpot()
    {
        return $this->hasOne(manual\Spot::className(), ['id' => 'spot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobLists()
    {
        return $this->hasMany(JobList::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    public function getAtelierList()
    {
        $atelier = Atelier::find()->all();
        return ArrayHelper::map($atelier, 'id', 'name');
    }
    public function getClientsList()
    {
        $client = Clients::find()->andWhere(['atelier_id' => Yii::$app->user->identity->atelier_id])->all();
        return ArrayHelper::map($client, 'id', 'fio');
    }

    public function getUsersList()
    {
        $users = Users::find()->where(['role_id' => Users::USER_ROLE_MASTER])->all();
        return ArrayHelper::map($users, 'id', 'name');
    }

    public function getTypeList()
    {
        return ArrayHelper::map([
            ['id' => 1, 'name' => 'Ремонт',],
            ['id' => 2, 'name' => 'Пошив',],
            ['id' => 3, 'name' => 'Переделка',],
        ], 'id', 'name');
    }

    public function getOrderTypeDescription()
    {
        if(1 == $this->order_type) return 'Ремонт';
        if(2 == $this->order_type) return 'Пошив';
        if(3 == $this->order_type) return 'Переделка';
    }

    public function getHiddenList()
    {
        return ArrayHelper::map([
            ['id' => 1, 'name' => 'Да',],
            ['id' => 2, 'name' => 'Нет',],
        ], 'id', 'name');
    }

    public function getClothesTypeList()
    {
        $type = manual\TypeClothes::find()->all();
        return ArrayHelper::map($type, 'id', 'name');
    }

    public function getMarketingList()
    {
        $advertising = manual\Advertising::find()->all();
        return ArrayHelper::map($advertising, 'id', 'name');
    }

    public function getSpotList()
    {
        $spot = manual\Spot::find()->all();
        return ArrayHelper::map($spot, 'id', 'name');
    }

    public function getMarksList()
    {
        $marks = manual\Marking::find()->all();
        return ArrayHelper::map($marks, 'id', 'name');
    }

    public function getPollutionList()
    {
        $pollution = manual\TypePollution::find()->all();
        return ArrayHelper::map($pollution, 'id', 'name');
    }
}
