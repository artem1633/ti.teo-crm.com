<?php

namespace app\models\manual;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\manual\AboutCompany;

/**
 * AboutCompanySearch represents the model behind the search form about `app\models\manual\AboutCompany`.
 */
class AboutCompanySearch extends AboutCompany
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'director', 'address', 'bank', 'inn', 'kpp', 'okpo', 'r_s', 'kor_schot', 'ogrn', 'bik', 'telephone', 'site', 'email', 'service_login', 'service_parol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AboutCompany::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'director', $this->director])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'okpo', $this->okpo])
            ->andFilterWhere(['like', 'r_s', $this->r_s])
            ->andFilterWhere(['like', 'kor_schot', $this->kor_schot])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'bik', $this->bik])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'service_login', $this->service_login])
            ->andFilterWhere(['like', 'service_parol', $this->service_parol]);

        return $dataProvider;
    }
}
