<?php

namespace app\models\manual;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\manual\ListServiceProduct;

/**
 * This is the model class for table "list_services".
 *
 * @property int $id
 * @property string $name
 */
class ListServices extends \yii\db\ActiveRecord
{
    public $productList;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_services';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            //[['name'], 'unique'],
            [['company_id'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['productList'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'company_id' => 'Company ID',
            'productList' => 'Товары',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->productList != null)
        {
            $contacts = ListServiceProduct::find()->where(['list_service_id' => $this->id])->all();

            foreach ($contacts as $contact){
                $delete = true;

                foreach ($this->productList as $item){
                    if($item['id'] == $contact->id){
                        $delete = false;
                    }
                }

                if($delete){
                    $contact->delete();
                }
            }


            foreach ($this->productList as $item) {
                $contact = ListServiceProduct::find()->where(['id' => $item['id']])->one();

                if (!$contact) {
                    (new ListServiceProduct([
                        'list_service_id' => $this->id,
                        'product_id' => $item['product_id'],
                        'count' => $item['count'],
                    ]))->save(false);
                }else{
                    $contact->list_service_id = $this->id;
                    $contact->product_id = $item['product_id'];
                    $contact->count = $item['count'];
                    $contact->save(false);
                }
            }
        } else {
            ListServiceProduct::deleteAll(['list_service_id' => $this->id]);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
