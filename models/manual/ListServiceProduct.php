<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "list_service_product".
 *
 * @property integer $id
 * @property integer $list_service_id
 * @property integer $product_id
 * @property integer $count
 *
 * @property ListServices $listService
 * @property Product $product
 */
class ListServiceProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_service_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_service_id', 'product_id', 'count'], 'integer'],
            [['list_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => ListServices::className(), 'targetAttribute' => ['list_service_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'list_service_id' => 'Услуга',
            'product_id' => 'Товар',
            'count' => 'Кол-во',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListService()
    {
        return $this->hasOne(ListServices::className(), ['id' => 'list_service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
