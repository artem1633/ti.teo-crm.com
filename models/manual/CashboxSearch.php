<?php

namespace app\models\manual;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\manual\Cashbox;

/**
 * CashboxSearch represents the model behind the search form about `app\models\manual\Cashbox`.
 */
class CashboxSearch extends Cashbox
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'atelier_id', 'online_cash'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cashbox::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'atelier_id' => $this->atelier_id,
            'online_cash' => $this->online_cash,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
