<?php

namespace app\models\manual;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\Atelier;


/**
 * This is the model class for table "cashbox".
 *
 * @property integer $id
 * @property integer $atelier_id
 * @property string $name
 * @property integer $online_cash
 *
 * @property Atelier $atelier
 */
class Cashbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const CONNECTING_TYPE_YES = 1;
    const CONNECTING_TYPE_NO = 0;
    public static function tableName()
    {
        return 'cashbox';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['atelier_id', 'online_cash'], 'integer'],
            [['name'], 'string', 'max' => 255],
            //[['name'], 'unique'],
            [['atelier_id', 'name'], 'required'],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['company_id'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'atelier_id' => 'Ателье',
            'name' => 'Наименование',
            'online_cash' => 'Онлайн касса',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(\app\models\Atelier::className(), ['id' => 'atelier_id']);
    }

    public function getAtelierList()
    {
        $ateliers = \app\models\Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }

    public function getTypeList()
    {
        return ArrayHelper::map([
            ['id' => self::CONNECTING_TYPE_YES, 'name' => 'Да',],
            ['id' => self::CONNECTING_TYPE_NO, 'name' => 'Нет',],
        ], 'id', 'name');
    }

    public function getTypeDescription()
    {        
        if(self::CONNECTING_TYPE_YES == $this->online_cash) return 'Да';
        if(self::CONNECTING_TYPE_NO == $this->online_cash) return 'Нет';       
    }
}
