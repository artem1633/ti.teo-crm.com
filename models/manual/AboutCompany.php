<?php

namespace app\models\manual;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;

/**
 * This is the model class for table "about_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $director
 * @property string $address
 * @property string $bank
 * @property string $inn
 * @property string $kpp
 * @property string $okpo
 * @property string $r_s
 * @property string $kor_schot
 * @property string $ogrn
 * @property string $bik
 * @property string $telephone
 * @property string $site
 * @property string $email
 * @property string $service_login
 * @property string $service_parol
 */
class AboutCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_company';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['address'], 'string'],
            [['name', 'director', 'bank', 'inn', 'kpp', 'okpo', 'r_s', 'kor_schot', 'ogrn', 'bik', 'telephone', 'site', 'email', 'service_login', 'service_parol'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название юр. лица ',
            'director' => 'Директор ',
            'address' => 'Адрес',
            'bank' => 'Банк ',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'okpo' => 'ОКПО',
            'r_s' => 'Расчетный счет',
            'kor_schot' => 'Корреспондентский счет',
            'ogrn' => 'ОГРН',
            'bik' => 'БИК',
            'telephone' => 'Телефон',
            'site' => 'Сайт',
            'email' => 'Email ',
            'service_login' => 'Логин смс-сервиса',
            'service_parol' => 'Пароль смс-сервиса',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
