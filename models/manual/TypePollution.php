<?php

namespace app\models\manual;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\Orders;

/**
 * This is the model class for table "type_pollution".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 */
class TypePollution extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_pollution';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            //[['name'], 'unique'],
            [['name'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'company_id' => 'Company ID',
        ];
    }

    public function beforeDelete()
    {
        $orders = Orders::find()->where(['pollution_id' => $this->id])->all();
        foreach ($orders as $order) 
        {
            $order->pollution_id = null;
            $order->save();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['pollution_id' => 'id']);
    }
}
