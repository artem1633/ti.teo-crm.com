<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\manual\Advertising;
use app\models\manual\Articles;
use app\models\manual\Cashbox;
use app\models\manual\ClaimStatuses;
use app\models\manual\ClientsGroup;
use app\models\manual\Goods;
use app\models\manual\ListServices;
use app\models\manual\Marking;
use app\models\manual\OrderStatus;
use app\models\manual\Product;
use app\models\manual\ProductStatus;
use app\models\manual\QuickOrder;
use app\models\manual\Spot;
use app\models\manual\Suppliers;
use app\models\manual\TypeClothes;
use app\models\manual\TypePollution;
use app\models\manual\AboutCompany;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $company_name;
    public $login;
    public $telephone;
    public $password;
    public $rate_id;

    /**
     * @var Users содержит созраненную модель users для возможности доступа к ней
     * через обработчик событий
     * @see \app\event_handlers\RegisteredFormEventHandler
     */
    public $_user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class' => \app\event_handlers\RegisteredFormEventHandler::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'rate_id', 'password', 'company_name', 'telephone'], 'required'],
            [['telephone'], 'string'],
            [['password'], 'string', 'max' => 255, 'min' => 6],
            [['rate_id'], 'integer'],
            [['login'], 'email'],
            [['login'], 'unique', 'targetClass' => '\app\models\User'],
            [['company_name'], 'unique', 'targetClass' => '\app\models\Companies'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_name' => 'Названия компании',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'telephone' => 'Телефон',
            'password' => 'Пароль',
            'rate_id' => 'Тариф',
        ];
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($type = Users::USER_ROLE_ADMIN)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new Users();
        //$user->attributes = $this->attributes;
        $user->name = $this->fio;
        $user->login = $this->login;
        $user->telephone = $this->telephone;
        $user->password = $this->password;
        $dateNow = time();
        $rate = Rates::findOne($this->rate_id);
        if($rate != null){
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ {$rate->time} seconds", $dateNow));
        } else {
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ 14 days", $dateNow));
        }
        $user->role_id = $type;

        if($user->save())
        {
            $this->_user = $user;
            $company = new Companies([
                'company_name' => $this->company_name,
                'admin_id' => $user->id,
                'access_end_datetime' => $accessEndDateTime,
                'rate_id' => $this->rate_id,
                'created_date' => date('Y-m-d'),
            ]);

            $company->save();

            //$user->link('companies', $company);

            $this->trigger(self::EVENT_REGISTERED);

            $template = EmailTemplates::findByKey('success_registration');

            if($template != null){
                $body = $template->applyTags($user);

                try {
                    Yii::$app->mailer->compose()
                        ->setFrom('zvonki.crm@mail.ru')
                        ->setTo($user->login)
                        ->setSubject('Успешная регистрация')
                        ->setHtmlBody($body)
                        ->send();
                } catch(\Exception $e){

                }
            }

            $user->company_id = $company->id;
            $user->save();
            $this->setDefaultValues($company->id,$user->id);
            return $user;
        }

        return null;
    }

    public function setDefaultValues($company_id,$user_id)
    {
        $aboutCompany = new AboutCompany();
        $aboutCompany->name = 'Компания "Цифровые Технологии"';
        $aboutCompany->director = 'Боровиков Андрей Васильевич';
        $aboutCompany->address = 'г. Глазов, ул. Молодой гвардии, д. 9';
        $aboutCompany->bank = 'Ижкомбанк';
        $aboutCompany->email = 'tv-glazov@mail.ru';
        $aboutCompany->telephone = '(34141) 66-001, 8-912-022-82-22';
        $aboutCompany->site = 'shop-crm.ru';
        $aboutCompany->company_id = $company_id;
        $aboutCompany->save();

        /*$articles = new Articles();
        $articles->name = 'Без названия';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Выдача ЗП';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Продажа товара';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Оказание услуг';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Химчистка';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Продажа товаров/носки';
        $articles->company_id = $company_id;
        $articles->save();

        $articles = new Articles();
        $articles->name = 'Продажа товаров/ швейная фурнитура';
        $articles->company_id = $company_id;
        $articles->save();*/


        /*$claim = new ClaimStatuses();
        $claim->name = 'Cоздано';
        $claim->color = '#ff0000';
        $claim->company_id = $company_id;
        $claim->save();

        $claim = new ClaimStatuses();
        $claim->name = 'Принята';
        $claim->color = '#339933';
        $claim->company_id = $company_id;
        $claim->save();

        $claim = new ClaimStatuses();
        $claim->name = 'Товар отправлен';
        $claim->color = '#ffff80';
        $claim->company_id = $company_id;
        $claim->save();

        $claim = new ClaimStatuses();
        $claim->name = 'Товар принят';
        $claim->color = '#80ffff';
        $claim->company_id = $company_id;
        $claim->save();*/

        $good = new Goods();
        $good->name = 'Без названия';
        $good->company_id = $company_id;
        $good->save();

        $good = new Goods();
        $good->name = 'Носки';
        $good->company_id = $company_id;
        $good->save();

        $good = new Goods();
        $good->name = 'Сопутствующие товары';
        $good->company_id = $company_id;
        $good->save();

        $good = new Goods();
        $good->name = 'Швейная фурнитура';
        $good->company_id = $company_id;
        $good->save();


        $marking = new Marking();
        $marking->name = 'Без типа маркировки';
        $marking->company_id = $company_id;
        $marking->save();

        $marking = new Marking();
        $marking->name = 'Имеется';
        $marking->company_id = $company_id;
        $marking->save();

        $marking = new Marking();
        $marking->name = 'Отсутствует';
        $marking->company_id = $company_id;
        $marking->save();

        $marking = new Marking();
        $marking->name = 'Не полная';
        $marking->company_id = $company_id;
        $marking->save();

        $marking = new Marking();
        $marking->name = 'Противоречит текстовой информации по уходу';
        $marking->company_id = $company_id;
        $marking->save();

        $marking = new Marking();
        $marking->name = 'Запрещена химчистка';
        $marking->company_id = $company_id;
        $marking->save();

        $orderStatus = new OrderStatus();
        $orderStatus->name = 'Принят';
        $orderStatus->company_id = $company_id;
        $orderStatus->save();

        $orderStatus = new OrderStatus();
        $orderStatus->name = 'В работе';
        $orderStatus->company_id = $company_id;
        $orderStatus->save();

        $orderStatus = new OrderStatus();
        $orderStatus->name = 'Готов';
        $orderStatus->company_id = $company_id;
        $orderStatus->save();

        /*$productStatus = new ProductStatus();
        $productStatus->name = 'На складе';
        $productStatus->company_id = $company_id;
        $productStatus->save();

        $productStatus = new ProductStatus();
        $productStatus->name = 'Поступили на склад';
        $productStatus->company_id = $company_id;
        $productStatus->save();

        $productStatus = new ProductStatus();
        $productStatus->name = 'Необходимо заказать';
        $productStatus->company_id = $company_id;
        $productStatus->save();

        $productStatus = new ProductStatus();
        $productStatus->name = 'Ожидание';
        $productStatus->company_id = $company_id;
        $productStatus->save();*/

        $quick = new QuickOrder();
        $quick->name = '25%';
        $quick->company_id = $company_id;
        $quick->save();

        $spot = new Spot();
        $spot->name = 'Кровь';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Краска';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Растительные масла';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Лекарства';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Духи/дезодоранты';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Вино';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Жир';
        $spot->company_id = $company_id;
        $spot->save();

        $spot = new Spot();
        $spot->name = 'Застаревшие пятна';
        $spot->company_id = $company_id;
        $spot->save();

        $atelier = new Atelier();
        $atelier->name = 'Ателье';
        $atelier->company_id = $company_id;
        $atelier->save();

        $storage = new Storage();
        $storage->name = 'Главный склад';
        $storage->user_id = $user_id;
        $storage->atelier_id = $atelier->id;
        $storage->is_main = 1;
        $storage->company_id = $company_id;
        $storage->save();

        $pollution = new TypePollution();
        $pollution->name = 'Засаленность';
        $pollution->description = '';
        $pollution->company_id = $company_id;
        $pollution->save();

        $pollution = new TypePollution();
        $pollution->name = 'Выгорание/побурение';
        $pollution->description = '';
        $pollution->company_id = $company_id;
        $pollution->save();

        $pollution = new TypePollution();
        $pollution->name = 'Повреждения';
        $pollution->description = '';
        $pollution->company_id = $company_id;
        $pollution->save();
    }

}