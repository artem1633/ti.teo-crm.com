<?php

namespace app\models;

use app\models\manual\OrderStatus;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "status_message".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property integer $status_id
 * @property string $files
 * @property integer $is_active
 * @property string $created_at
 *
 * @property OrderStatus $status
 */
class StatusMessage extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $uploadFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'status_id'], 'required'],
            [['content', 'files'], 'string'],
            [['status_id', 'is_active'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['uploadFiles'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'content' => 'Текст',
            'status_id' => 'Статус',
            'files' => 'Файлы',
            'is_active' => 'Активный',
            'created_at' => 'Created At',
            'uploadFiles' => 'Файлы',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->created_at = date('Y-m-d H:i:s');
        }

        if($this->uploadFiles != null)
        {
            $files = explode(',', $this->files);

            $newFiles = [];

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            if(is_dir('uploads/data') == false){
                mkdir('uploads/data');
            }

            /** @var UploadedFile $file */
            foreach ($this->uploadFiles as $file){

                $path = "uploads/data/".Yii::$app->security->generateRandomString().".{$file->extension}";
                $file->saveAs($path);
                $newFiles[] = $path;

            }

            if(count($files) > 0){
                $this->files = ArrayHelper::merge($files, $newFiles);
            } else {
                $this->files = $newFiles;
            }

            $this->files = implode(',', $this->files);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }
}
