<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $factDateStart;
    public $factDateEnd;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_type', 'status_id', 'user_id', 'clothes_type', 'marketing_id', 'quick', 'pollution_id', 'spot_id', 'marks_id', 'wear', 'lost_product', 'hidden_defect', 'acceptor_id', 'creator_id'], 'integer'],
            [['issue_date', 'telephone', 'comment', 'color', 'fabric_structure', 'material', 'symbol', 'product_warranty', 'complekt', 'defect', 'damage', 'spots_location', 'fact_date', 'client_id', 'factDateStart', 'factDateEnd'], 'safe'],
            [['prepay', 'sale', 'cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['user', 'client', 'company.admin as admins', 'jobLists.job', 'jobLists.status as job_list_status', 'jobLists.master as job_list_master', 'marks', 'spot']);

        $query->andFilterWhere([
            'orders.id' => $this->client_id,
            'fact_date' => $this->fact_date,
            'cost' => $this->cost,
            'issue_date' => $this->issue_date,
            'order_type' => $this->order_type,
//            'client_id' => $this->client_id,
//            'user_id' => $this->user_id,
            'clothes_type' => $this->clothes_type,
            'prepay' => $this->prepay,
            'sale' => $this->sale,
            'marketing_id' => $this->marketing_id,
            'quick' => $this->quick,
            'pollution_id' => $this->pollution_id,
            'spot_id' => $this->spot_id,
            'marks_id' => $this->marks_id,
            'wear' => $this->wear,
            'lost_product' => $this->lost_product,
            'hidden_defect' => $this->hidden_defect,
            'creator_id' => $this->creator_id,
            'acceptor_id' => $this->acceptor_id,
        ]);


        if($this->factDateStart != null && $this->factDateEnd){
            $query->andWhere(['between', 'fact_date', $this->factDateStart.' 00:00:00', $this->factDateEnd.' 23:59:59']);
        }


        $query->orFilterWhere(['like', 'orders.telephone', $this->client_id])
            ->orFilterWhere(['like', 'comment', $this->client_id])
            ->orFilterWhere(['like', 'admins.name', $this->client_id])
            ->orFilterWhere(['like', 'clients.fio', $this->client_id])
            ->orFilterWhere(['like', 'list_services.name', $this->client_id])
            ->orFilterWhere(['like', 'job_list_status.name', $this->client_id])
            ->orFilterWhere(['like', 'job_list_master.name', $this->client_id])
            ->orFilterWhere(['like', 'marking.name', $this->client_id])
            ->orFilterWhere(['like', 'spot.name', $this->client_id])
            ->orFilterWhere(['like', 'number', $this->client_id]);


        if($this->status_id == 3){
            $query->andWhere(['orders.status_id' => [3, 4]]);

            $orders = Orders::find()->all();
            $ordersPks = [];

            foreach ($orders as $order)
            {
                $jobList = JobList::find()->where(['order_id' => $order->id])->andWhere(['!=', 'status_id', 4])->one();
                if($jobList == null){
                    $jobList = JobList::find()->where(['order_id' => $order->id, 'status_id' => 4])->one();
                    if($jobList){
                        $ordersPks[] = $order->id;
                    }
                }
            }

            $query->orWhere(['orders.id' => $ordersPks]);
        } else {
            $query->andFilterWhere(['orders.status_id' => $this->status_id]);
        }

        return $dataProvider;
    }
}
