<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SellingsGood;
use app\models\Product;

/**
 * SellingsGoodSearch represents the model behind the search form about `app\models\SellingsGood`.
 */
class SellingsGoodSearch extends SellingsGood
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'selling_id', 'product_id', 'count'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SellingsGood::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'selling_id' => $this->selling_id,
            'product_id' => $this->product_id,
            'price' => $this->price,
            'count' => $this->count,
        ]);

        return $dataProvider;
    }

    public function searchBySelling($params,$id)
    {
        $query = SellingsGood::find()->where(['selling_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'selling_id' => $this->selling_id,
            'product_id' => $this->product_id,
            'price' => $this->price,
            'count' => $this->count,
        ]);

        return $dataProvider;
    }
}
