<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;

/**
 * This is the model class for table "available".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $creator_id
 * @property integer $storage_id
 * @property integer $atelier_id
 * @property integer $status_id
 * @property integer $count
 * @property double $price
 * @property double $price_shop
 * @property integer $good_id
 * @property string $comment
 * @property integer $manufacturer_id
 *
 * @property Atelier $atelier
 * @property Users $creator
 * @property Manufacturer $manufacturer
 * @property Product $product
 * @property ProductStatus $status
 * @property Storage $storage
 */
class Available extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $move_count;
    public $tovar;
    public $storages;
    public $active_window;
    public $storage_name;
    public static function tableName()
    {
        return 'available';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    /*public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'storage_id', 'status_id', 'count', 'good_id', 'move_count', 'number', 'active_window', 'received', 'company_id', 'atelier_id'], 'integer'],
            [['price', 'price_shop'], 'number'],
            [['comment'], 'string'],
            [['date_cr'], 'safe'],
            [['product_id', 'count', 'status_id', 'price_shop', 'postavshik_id', 'storage_id' ], 'required'],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manufacturer::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\ProductStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            ['product_id', 'validateProduct'],
            ['postavshik_id', 'validatePostavshik'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Название товара',
            'creator_id' => 'Создатель',
            'storage_id' => 'Склад',
            'storage_name' => 'Склад',
            'status_id' => 'Статус',
            'count' => 'Объем',
            'price' => 'Цена',
            'price_shop' => 'Цена закупа',
            'good_id' => 'Категория товар',
            'comment' => 'Примечание',
            'move_count' => 'Объем',
            'tovar' => 'Товар',
            'storages' => 'Склады',
            'postavshik_id' => 'Поставщик',
            'number' => 'Номер документа',
            'date_cr' => 'Дата создания',
            'received' => 'Поступило',
            'company_id' => 'Company ID',
            'atelier_id' => 'Филиал',
            'manufacturer_id' => 'Производитель',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->creator_id = Yii::$app->user->identity->id;
            $this->date_cr = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }

    //Новый товар
    public function validateProduct($attribute, $params)
    {
        $product = manual\Product::find()->where(['id' => $this->product_id])->one();
        if (!isset($product))
        {
            $product = new manual\Product();
            $product->name = $this->product_id;
            $product->cost = $this->price;
            $error = $parts->errors;
            
            if ($product->save())
            {
                $this->product_id = $product->id;
            }
            else
            {
                $this->addError($attribute,"Не создан новый товар");
            }
        }
    }

    //Новый поставщик
    public function validatePostavshik($attribute, $params)
    {
        $postavshik = manual\Suppliers::find()->where(['id' => $this->postavshik_id])->one();
        if (!isset($postavshik))
        {
            $postavshik = new manual\Suppliers();
            $postavshik->name = $this->postavshik_id;
            $error = $postavshik->errors;
            
            if ($postavshik->save())
            {
                $this->postavshik_id = $postavshik->id;
            }
            else
            {
                $this->addError($attribute, "Не создан новый поставщик");
            }
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(manual\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(manual\ProductStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(manual\Goods::className(), ['id' => 'good_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['available_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostavshik()
    {
        return $this->hasOne(manual\Suppliers::className(), ['id' => 'postavshik_id']);
    }

    public function getProductList()
    {
        $product = manual\Product::find()->all();
        return ArrayHelper::map($product, 'id', 'name');
    }

    public function getStorageList()
    {
        $role = Yii::$app->user->identity->role_id;
        if($role == Users::USER_ROLE_ADMIN) $storage = Storage::find()->all();
        else $storage = Storage::find()->where(['id' => Yii::$app->user->identity->atelier_id])->all();

        return ArrayHelper::map($storage, 'id', 'name');
    }

    public function getStatusList()
    {
        $product_status = manual\ProductStatus::find()->all();
        return ArrayHelper::map($product_status, 'id', 'name');
    }

    public function getGoodList()
    {
        $goods = manual\Goods::find()->where([ '!=', 'name', 'Без названия'])->all();
        return ArrayHelper::map($goods, 'id', 'name');
    }

    public function getSuppliersList()
    {
        $supliers = manual\Suppliers::find()->all();
        return ArrayHelper::map($supliers, 'id', 'name');
    }

    public function getOtherStorageList()
    {
        $result = [];
        $storages = Storage::find()->all();
        foreach ($storages as $storage) {
            
            if($this->storage_id != $storage->id){
                if($storage->is_main == 1)
                    $result [] = [
                        'id' => $storage->id,
                        'name' => 'Пользователь : Администратор / ' .$storage->user->name . $storage->name,
                    ];
                else $result [] = [
                    'id' => $storage->id,
                    'name' => 'Пользователь :' .$storage->user->name  . ' / '. $storage->name,
                ];
            }
        }
        return ArrayHelper::map($result,'id', 'name');
    }
}
