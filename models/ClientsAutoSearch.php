<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientsAuto;

/**
 * ClientsAutoSearch represents the model behind the search form about `app\models\ClientsAuto`.
 */
class ClientsAutoSearch extends ClientsAuto
{
    /** @var string */
    public $all;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'type_id', 'marks_id', 'spot_id'], 'integer'],
            [['number', 'passport', 'color', 'year', 'all'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientsAuto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['marks', 'spot', 'type', 'client']);

        $query->andFilterWhere([
            'id' => $this->id,
//            'client_id' => $this->client_id,
//            'type_id' => $this->type_id,
//            'marks_id' => $this->marks_id,
//            'spot_id' => $this->spot_id,
        ]);

        $query->orFilterWhere(['like', 'number', $this->all])
            ->orFilterWhere(['like', 'passport', $this->all])
            ->orFilterWhere(['like', 'color', $this->all])
            ->orFilterWhere(['like', 'year', $this->all])
            ->orFilterWhere(['like', 'marking.name', $this->all])
            ->orFilterWhere(['like', 'spot.name', $this->all])
            ->orFilterWhere(['like', 'type_clothes.name', $this->all])
            ->orFilterWhere(['like', 'clients.fio', $this->all]);



        return $dataProvider;
    }
}
