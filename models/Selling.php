<?php

namespace app\models;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Product;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
/**
 * This is the model class for table "selling".
 * 
 * @property int $id
 * @property int $storage_id
 * @property int $client_id
 * @property double $summa
 * @property string $comment
 * @property int $creater_id
 * @property int $changed_id
 * @property string $date_cr
 * @property string $date_up
 *
 * @property SellingsGood[] $sellingsGoods
 */
class Selling extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'selling';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['storage_id', 'client_id', 'creater_id', 'changed_id', 'company_id'], 'integer'],
            [['summa'], 'number'],
            [['comment'], 'string'],
            [['date_cr', 'date_up'], 'safe'],
            [['storage_id','client_id'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /** 
     * {@inheritdoc} 
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storage_id' => 'Склад',
            'client_id' => 'Клиент',
            'summa' => 'Сумма',
            'comment' => 'Комментарий',
            'creater_id' => 'Создатель',
            'changed_id' => 'Изменял',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'company_id' => 'Компания, к которой принадлежит пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->creater_id = Yii::$app->user->identity->id;
            $this->date_cr = date('Y-m-d H:i:s');
        }
            $this->changed_id = Yii::$app->user->identity->id;
            $this->date_up = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $sellingsgoods = SellingsGood::find()->where(['selling_id' => $this->id])->all();
        foreach ($sellingsgoods as $sellingsgood) {
           $sellingsgood->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
    
    public function getSellingsGoods()
    {
        return $this->hasMany(SellingsGood::className(), ['selling_id' => 'id']);
    }
    public function getChanged()
    {
        return $this->hasOne(Users::className(), ['id' => 'changed_id']);
    }
    public function getChange()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'name');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
    public function getClients()
    {
        $client = Clients::find()->all();
        return ArrayHelper::map($client, 'id', 'fio');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreater()
    {
        return $this->hasOne(Users::className(), ['id' => 'creater_id']);
    }
    public function getCreaters()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'name');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }
    public function getStorages()
    {
        $storage = Storage::find()->all();
        return ArrayHelper::map($storage, 'id', 'name');
    }
    public function getSellingsGood()
    {
        $query = SellingsGood::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $query->andFilterWhere(['selling_id' => $this->id]);
        return $dataProvider;
    }
}
