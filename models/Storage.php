<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;

/**
 * This is the model class for table "storage".
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property integer $atelier_id
 *
 * @property Atelier $atelier
 * @property Users $user
 */
class Storage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else {
                        if($this->company_id != null) return $this->company_id;
                        else return null;
                    }
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
                }
            }
        }
    }  

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'atelier_id', 'is_main', 'company_id'], 'integer'],
            [['name', 'user_id', 'atelier_id'], 'required'],
            //[['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
            [['atelier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Atelier::className(), 'targetAttribute' => ['atelier_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'user_id' => 'Пользователь',
            'atelier_id' => 'Магазин',
            'is_main' => 'is_main',
            'company_id' => 'Company ID',
        ];
    }

    public function beforeSave($insert)
    {
        //$this->atelier_id = $this->user->atelier_id;
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            $available = Available::find()->where(['storage_id' => $this->id])->all();
            foreach ($available as $value) {
               $value->delete();
            }

            $resource = Resource::find()->where(['storage_id' => $this->id])->all();
            foreach ($resource as $value) {
               $value->delete();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtelier()
    {
        return $this->hasOne(Atelier::className(), ['id' => 'atelier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['storage_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['storage_id' => 'id']);
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'name');
    }

    public function getAtelierList()
    {
        $atelier = Atelier::find()->all();
        return ArrayHelper::map($atelier, 'id', 'name');
    }
}
