<?php

namespace app\bootstrap;

use kartik\grid\ActionColumn;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/**
 * Class AppBootstrap
 * @package app\bootstrap
 */
class AppBootstrap implements BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        Yii::$container->set(ActionColumn::class, [
            'buttons' => [
                'view' => function($url, $model){
                    return Html::a('<i class="feather icon-eye"></i>', $url, [
                        'role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                    ]);
                },
                'update' => function($url, $model){
                    return Html::a('<i class="feather icon-edit"></i>', $url, [
                        'role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip', 'style' => 'font-size: 16px;',
                    ]);
                },
                'delete' => function($url, $model){
                    return Html::a('<i class="feather icon-trash-2"></i>', $url, [
                        'role'=>'modal-remote','title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                        'style' => 'font-size: 16px;',
                    ]);
                },
            ],
        ]);

        Yii::$container->set(LinkPager::class, [
            'options' => [
                'tag' => 'ul',
                'class' => 'pagination',
                'style' => 'margin-bottom: 20px; float: right;',
            ],
            'linkContainerOptions' => [
                'class' => 'paginate_button page-item'
            ],
            'linkOptions' => ['class' => 'page-link'],
            'nextPageLabel' => 'Далее',
            'prevPageLabel' => 'Назад',
            'disabledListItemSubTagOptions' => [
                'style' => 'display: none;'
            ],
        ]);
    }
}